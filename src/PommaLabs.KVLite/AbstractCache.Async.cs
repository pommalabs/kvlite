﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

public abstract partial class AbstractCache<TCache, TSettings>
{
    #region Abstract and virtual members

    /// <summary>
    ///   Adds given value with the specified expiry time and refresh internal.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="utcExpiry">The UTC expiry time.</param>
    /// <param name="interval">The refresh interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    protected virtual Task AddInternalAsync<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, TimeSpan interval, IList<CacheKey> parentKeys, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<object>(cancellationToken);
        }
        AddInternal(partition, key, value, utcExpiry, interval, parentKeys);
        return Task.FromResult(0);
    }

    /// <summary>
    ///   Clears this instance or a partition, if specified.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries that have been removed.</returns>
    protected virtual Task<long> ClearInternalAsync(CacheKey? partition, CacheReadMode cacheReadMode, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<long>(cancellationToken);
        }
        var result = ClearInternal(partition, cacheReadMode);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Determines whether cache contains the specified partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>Whether cache contains the specified partition and key.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    protected virtual Task<bool> ContainsInternalAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<bool>(cancellationToken);
        }
        var result = ContainsInternal(partition, key);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   The number of entries in the cache or in a partition, if specified.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    protected virtual Task<long> CountInternalAsync(CacheKey? partition, CacheReadMode cacheReadMode, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<long>(cancellationToken);
        }
        var result = CountInternal(partition, cacheReadMode);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Computes cache size in bytes. This value might be an estimate of real cache size and,
    ///   therefore, it does not need to be extremely accurate.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>An estimate of cache size in bytes.</returns>
    protected virtual Task<long> GetCacheSizeInBytesInternalAsync(CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<long>(cancellationToken);
        }
        return Task.FromResult(GetCacheSizeInBytesInternal());
    }

    /// <summary>
    ///   Gets all cache entries or the ones in a partition, if specified. If an entry is a
    ///   "sliding" value, its lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries.</returns>
    protected virtual Task<IList<ICacheEntry<TVal>>> GetEntriesInternalAsync<TVal>(CacheKey? partition, CacheQueryOptions queryOptions, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<IList<ICacheEntry<TVal>>>(cancellationToken);
        }
        var result = GetEntriesInternal<TVal>(partition, queryOptions);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Gets the cache entry with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The cache entry with specified partition and key.</returns>
    protected virtual Task<CacheResult<ICacheEntry<TVal>>> GetEntryInternalAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<CacheResult<ICacheEntry<TVal>>>(cancellationToken);
        }
        var result = GetEntryInternal<TVal>(partition, key);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Gets the value with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by the corresponding interval.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The value with specified partition and key.</returns>
    protected virtual Task<CacheResult<TVal>> GetInternalAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<CacheResult<TVal>>(cancellationToken);
        }
        var result = GetInternal<TVal>(partition, key);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Gets the all values in the cache or in the specified partition, without updating
    ///   expiry dates.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All values, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected virtual Task<IList<ICacheEntry<TVal>>> PeekEntriesInternalAsync<TVal>(CacheKey? partition, CacheQueryOptions queryOptions, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<IList<ICacheEntry<TVal>>>(cancellationToken);
        }
        var result = PeekEntriesInternal<TVal>(partition, queryOptions);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected virtual Task<CacheResult<ICacheEntry<TVal>>> PeekEntryInternalAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<CacheResult<ICacheEntry<TVal>>>(cancellationToken);
        }
        var result = PeekEntryInternal<TVal>(partition, key);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected virtual Task<CacheResult<TVal>> PeekInternalAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<CacheResult<TVal>>(cancellationToken);
        }
        var result = PeekInternal<TVal>(partition, key);
        return Task.FromResult(result);
    }

    /// <summary>
    ///   Removes the value with given partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    protected virtual Task RemoveInternalAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken)
    {
        if (cancellationToken.IsCancellationRequested)
        {
            return CanceledTaskAsync<object>(cancellationToken);
        }
        RemoveInternal(partition, key);
        return Task.FromResult(0);
    }

    #endregion Abstract and virtual members

    #region ICache members

    /// <inheritdoc/>
    public Task AddSlidingAsync<TVal>(CacheKey partition, CacheKey key, TVal value, TimeSpan interval, IList<CacheKey> parentKeys = default, CancellationToken cancellationToken = default)
    {
        return AddAsync(partition, key, value, Clock.UtcNow + interval, interval, parentKeys, cancellationToken);
    }

    /// <inheritdoc/>
    public Task AddTimedAsync<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, IList<CacheKey> parentKeys = default, CancellationToken cancellationToken = default)
    {
        return AddAsync(partition, key, value, utcExpiry, TimeSpan.Zero, parentKeys, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<long> ClearAsync(CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheCleanupActivity(default))
        {
            try
            {
                var result = await ClearInternalAsync(default, CacheReadMode.IgnoreExpiryDate, cancellationToken).ConfigureAwait(false);

                // Postconditions
                Debug.Assert(result >= 0L);
                Debug.Assert(await CountAsync(cancellationToken).ConfigureAwait(false) == 0);
                Debug.Assert(await LongCountAsync(cancellationToken).ConfigureAwait(false) == 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearAll, Settings.CacheName);
                return 0L;
            }
        }
    }

    /// <inheritdoc/>
    public async Task<long> ClearAsync(CacheKey partition, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheCleanupActivity(partition))
        {
            try
            {
                var result = await ClearInternalAsync(partition, CacheReadMode.IgnoreExpiryDate, cancellationToken).ConfigureAwait(false);

                // Postconditions
                Debug.Assert(result >= 0L);
                Debug.Assert(await CountAsync(partition, cancellationToken).ConfigureAwait(false) == 0);
                Debug.Assert(await LongCountAsync(partition, cancellationToken).ConfigureAwait(false) == 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearPartition, Settings.CacheName, partition);
                return 0L;
            }
        }
    }

    /// <summary>
    ///   Clears the cache using the specified cache read mode.
    /// </summary>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries that have been removed.</returns>
    public Task<long> ClearAsync(CacheReadMode cacheReadMode, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        return AsyncMethod(cacheReadMode, cancellationToken);

        async Task<long> AsyncMethod(CacheReadMode cacheReadMode, CancellationToken cancellationToken)
        {
            using (StartCacheCleanupActivity(default))
            {
                try
                {
                    var result = await ClearInternalAsync(default, cacheReadMode, cancellationToken).ConfigureAwait(false);

                    // Postconditions
                    Debug.Assert(result >= 0L);
                    return result;
                }
                catch (Exception ex)
                {
                    LastError = ex;
                    Logger.LogError(ex, ErrorMessages.InternalErrorOnClearAll, Settings.CacheName);
                    return 0L;
                }
            }
        }
    }

    /// <summary>
    ///   Clears the specified partition using the specified cache read mode.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries that have been removed.</returns>
    public Task<long> ClearAsync(CacheKey partition, CacheReadMode cacheReadMode, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        return AsyncMethod(partition, cacheReadMode, cancellationToken);

        async Task<long> AsyncMethod(CacheKey partition, CacheReadMode cacheReadMode, CancellationToken cancellationToken)
        {
            using (StartCacheCleanupActivity(partition))
            {
                try
                {
                    var result = await ClearInternalAsync(partition, cacheReadMode, cancellationToken).ConfigureAwait(false);

                    // Postconditions
                    Debug.Assert(result >= 0L);
                    return result;
                }
                catch (Exception ex)
                {
                    LastError = ex;
                    Logger.LogError(ex, ErrorMessages.InternalErrorOnClearPartition, Settings.CacheName, partition);
                    return 0L;
                }
            }
        }
    }

    /// <inheritdoc/>
    public async Task<bool> ContainsAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            return await ContainsInternalAsync(partition, key, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
            return false;
        }
    }

    /// <inheritdoc/>
    public async Task<int> CountAsync(CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = Convert.ToInt32(await CountInternalAsync(default, CacheReadMode.ConsiderExpiryDate, cancellationToken).ConfigureAwait(false));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0;
        }
    }

    /// <inheritdoc/>
    public async Task<int> CountAsync(CacheKey partition, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = Convert.ToInt32(await CountInternalAsync(partition, CacheReadMode.ConsiderExpiryDate, cancellationToken).ConfigureAwait(false));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0;
        }
    }

    /// <inheritdoc/>
    public async Task<CacheResult<TVal>> GetAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                var result = await GetInternalAsync<TVal>(partition, key, cancellationToken).ConfigureAwait(false);

                if (Logger.IsEnabled(LogLevel.Trace))
                {
                    Logger.LogTrace(TraceMessages.GetEntry, partition, key, Settings.CacheName, stopwatch?.ElapsedMilliseconds);
                }

                // Postconditions
                Debug.Assert((await ContainsAsync(partition, key, cancellationToken).ConfigureAwait(false)) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public async Task<long> GetCacheSizeInBytesAsync(CancellationToken cancellationToken = default)
    {
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = await GetCacheSizeInBytesInternalAsync(cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public async Task<IList<ICacheEntry<TVal>>> GetEntriesAsync<TVal>(CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = await GetEntriesInternalAsync<TVal>(null, queryOptions, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= await CountAsync(cancellationToken).ConfigureAwait(false));
            Debug.Assert(result.Count <= await LongCountAsync(cancellationToken).ConfigureAwait(false));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public async Task<IList<ICacheEntry<TVal>>> GetEntriesAsync<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = await GetEntriesInternalAsync<TVal>(partition, queryOptions, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= await CountAsync(partition, cancellationToken).ConfigureAwait(false));
            Debug.Assert(result.Count <= await LongCountAsync(partition, cancellationToken).ConfigureAwait(false));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadPartition, Settings.CacheName, partition);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public async Task<CacheResult<ICacheEntry<TVal>>> GetEntryAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                var result = await GetEntryInternalAsync<TVal>(partition, key, cancellationToken).ConfigureAwait(false);

                if (Logger.IsEnabled(LogLevel.Trace))
                {
                    Logger.LogTrace(TraceMessages.GetEntry, partition, key, Settings.CacheName, stopwatch?.ElapsedMilliseconds);
                }

                // Postconditions
                Debug.Assert((await ContainsAsync(partition, key, cancellationToken).ConfigureAwait(false)) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public Task<TVal> GetOrAddSlidingAsync<TVal>(
        CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, TimeSpan interval,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default,
        CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (valueGetter == null) throw new ArgumentNullException(nameof(valueGetter), ErrorMessages.NullValueGetter);
        if (parentKeys != null && parentKeys.Count > MaxParentKeyCountPerEntry) throw new NotSupportedException(ErrorMessages.TooManyParentKeys);

        return AsyncMethod(partition, key, valueGetter, interval, parentKeys, valueFilterer, cancellationToken);

        async Task<TVal> AsyncMethod(
            CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, TimeSpan interval,
            IList<CacheKey> parentKeys, Predicate<TVal> valueFilterer, CancellationToken cancellationToken)
        {
            var (hasValue, value) = await TryGetAsync(partition, key, valueGetter, valueFilterer, cancellationToken).ConfigureAwait(false);
            if (!hasValue)
            {
                await AddSlidingAsync(partition, key, value, interval, parentKeys, cancellationToken).ConfigureAwait(false);
            }
            return value;
        }
    }

    /// <inheritdoc/>
    public Task<TVal> GetOrAddTimedAsync<TVal>(
        CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, DateTimeOffset utcExpiry,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default,
        CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (valueGetter == null) throw new ArgumentNullException(nameof(valueGetter), ErrorMessages.NullValueGetter);
        if (parentKeys != null && parentKeys.Count > MaxParentKeyCountPerEntry) throw new NotSupportedException(ErrorMessages.TooManyParentKeys);

        return AsyncMethod(partition, key, valueGetter, utcExpiry, parentKeys, valueFilterer, cancellationToken);

        async Task<TVal> AsyncMethod(
            CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, DateTimeOffset utcExpiry,
            IList<CacheKey> parentKeys, Predicate<TVal> valueFilterer, CancellationToken cancellationToken)
        {
            var (hasValue, value) = await TryGetAsync(partition, key, valueGetter, valueFilterer, cancellationToken).ConfigureAwait(false);
            if (!hasValue)
            {
                await AddTimedAsync(partition, key, value, utcExpiry, parentKeys, cancellationToken).ConfigureAwait(false);
            }
            return value;
        }
    }

    /// <inheritdoc/>
    public async Task<long> LongCountAsync(CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = await CountInternalAsync(default, CacheReadMode.ConsiderExpiryDate, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public async Task<long> LongCountAsync(CacheKey partition, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = await CountInternalAsync(partition, CacheReadMode.ConsiderExpiryDate, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public async Task<CacheResult<TVal>> PeekAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var result = await PeekInternalAsync<TVal>(partition, key, cancellationToken).ConfigureAwait(false);

                // Postconditions
                Debug.Assert((await ContainsAsync(partition, key, cancellationToken).ConfigureAwait(false)) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public async Task<IList<ICacheEntry<TVal>>> PeekEntriesAsync<TVal>(CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = await PeekEntriesInternalAsync<TVal>(null, queryOptions, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= await CountAsync(cancellationToken).ConfigureAwait(false));
            Debug.Assert(result.Count <= await LongCountAsync(cancellationToken).ConfigureAwait(false));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public async Task<IList<ICacheEntry<TVal>>> PeekEntriesAsync<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = await PeekEntriesInternalAsync<TVal>(partition, queryOptions, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= await CountAsync(partition, cancellationToken).ConfigureAwait(false));
            Debug.Assert(result.Count <= await LongCountAsync(partition, cancellationToken).ConfigureAwait(false));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadPartition, Settings.CacheName, partition);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public async Task<CacheResult<ICacheEntry<TVal>>> PeekEntryAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var result = await PeekEntryInternalAsync<TVal>(partition, key, cancellationToken).ConfigureAwait(false);

                // Postconditions
                Debug.Assert((await ContainsAsync(partition, key, cancellationToken).ConfigureAwait(false)) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public async Task RemoveAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            await RemoveInternalAsync(partition, key, cancellationToken).ConfigureAwait(false);

            // Postconditions
            Debug.Assert(!await ContainsAsync(partition, key, cancellationToken).ConfigureAwait(false));
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnWrite, partition, key, Settings.CacheName);
        }
    }

    #endregion ICache members

    #region Helpers

    /// <summary>
    ///   Gets a task that has been canceled.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token used to cancel the task.</param>
    /// <returns>A task that has been canceled.</returns>
    private static Task<TResult> CanceledTaskAsync<TResult>(CancellationToken cancellationToken = default)
    {
        var tcs = new TaskCompletionSource<TResult>(cancellationToken);
        tcs.TrySetCanceled(CancellationToken.None);
        return tcs.Task;
    }

    private async Task AddAsync<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, TimeSpan interval, IList<CacheKey> parentKeys = default, CancellationToken cancellationToken = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (parentKeys != null && parentKeys.Count > MaxParentKeyCountPerEntry) throw new NotSupportedException(ErrorMessages.TooManyParentKeys);

        using (StartCacheEntryInsertionActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                await AddInternalAsync(partition, key, value, utcExpiry, interval, parentKeys, cancellationToken).ConfigureAwait(false);
                TraceNewEntry<TVal>(partition, key, utcExpiry, interval, stopwatch);
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnWrite, partition, key, Settings.CacheName);
            }
        }
    }

    private async Task<(bool hasValue, TVal value)> TryGetAsync<TVal>(
        CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter,
        Predicate<TVal> valueFilterer, CancellationToken cancellationToken)
    {
        var originalPartition = partition;
        var originalKey = key;
        Interceptor.EntryGetting(ref partition, ref key);

        var cacheResult = await GetAsync<TVal>(partition, key, cancellationToken).ConfigureAwait(false);
        if (cacheResult.HasValue)
        {
            return (true, cacheResult.Value);
        }

        // This line is reached when the cache does not contain the entry or an error has occurred.
        var value = await valueGetter().ConfigureAwait(false);

        // If value filterer has been specified, then it should be invoked in order to
        // understand whether retrieved value will be filtered.
        if (valueFilterer?.Invoke(value) ?? false)
        {
            return (true, value);
        }

        partition = originalPartition;
        key = originalKey;
        Interceptor.EntryAdding(ref partition, ref key);

        return (false, value);
    }

    #endregion Helpers
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Microsoft.Extensions.Caching.Distributed;
using PommaLabs.KVLite;
using PommaLabs.KVLite.Memory;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for in-memory KVLite services.
/// </summary>
public static class MemoryKVLiteServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="MemoryCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteMemoryCache(this IServiceCollection services) => services.AddKVLiteMemoryCache(null);

    /// <summary>
    ///   Registers <see cref="MemoryCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeSettings">Can be used to customize settings.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteMemoryCache(this IServiceCollection services, Action<MemoryCacheSettings> changeSettings)
    {
        var settings = new MemoryCacheSettings();
        changeSettings?.Invoke(settings);

        return services.AddKVLiteCache<MemoryCache, MemoryCacheSettings>(
            (serializer, clock, logger, interceptor, random) =>
            new MemoryCache(settings, serializer, clock, logger, interceptor, random));
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.Serialization;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Settings used by <see cref="VolatileCache"/>.
/// </summary>
[Serializable, DataContract]
public sealed class VolatileCacheSettings : SQLiteCacheSettings<VolatileCacheSettings>;

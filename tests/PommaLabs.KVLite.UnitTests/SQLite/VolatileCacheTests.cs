﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Ninject;
using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Resources;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.SQLite;

[TestFixture]
[Category(nameof(SQLite))]
[NonParallelizable]
internal sealed class VolatileCacheTests : AbstractTests
{
    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        VolatileCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance

    #region Cache creation and disposal

    [Test]
    public void Dispose_ObjectDisposedExceptionAfterDispose()
    {
        var cache = new VolatileCache(new VolatileCacheSettings());
        cache.Dispose();
        Assert.Throws<ObjectDisposedException>(() => { cache.Count(); });
    }

    [TestCase("")]
    [TestCase((string)null)]
    [TestCase("   ")]
    public void NewCache_BlankName(string name)
    {
        try
        {
            using var cache = new VolatileCache(new VolatileCacheSettings { CacheName = name }, clock: Kernel.Get<IClock>());
        }
        catch (Exception ex)
        {
            Assert.That(ex, Is.InstanceOf<ArgumentException>());
            Assert.That(ex.Message, Does.Contain(ErrorMessages.NullOrEmptyCacheName));
        }
    }

    [TestCase("a")]
    [TestCase("a1")]
    [TestCase("a_")]
    [TestCase("a.")]
    [TestCase("_")]
    [TestCase("1")]
    [TestCase("1_a")]
    [TestCase("1.a")]
    [TestCase("a.b")]
    [TestCase("a...b")]
    public void NewCache_GoodName(string name)
    {
        var cache = new VolatileCache(new VolatileCacheSettings { CacheName = name }, clock: Kernel.Get<IClock>());
        Assert.That(cache, Is.Not.Null);
        cache.Dispose();
        Assert.That(cache.Disposed, Is.True);
    }

    [TestCase("$$$")]
    [TestCase("a£")]
    [TestCase("1,2")]
    [TestCase("1+aaa")]
    [TestCase("_3?")]
    public void NewCache_WrongName(string name)
    {
        try
        {
            using var cache = new VolatileCache(new VolatileCacheSettings { CacheName = name }, clock: Kernel.Get<IClock>());
        }
        catch (Exception ex)
        {
            Assert.That(ex, Is.InstanceOf<ArgumentException>());
            Assert.That(ex.Message, Does.Contain(ErrorMessages.InvalidCacheName));
        }
    }

    #endregion Cache creation and disposal

    #region SQLite-specific Clean

    [Test]
    public void Clean_InvalidValues()
    {
        var cache = Kernel.Get<VolatileCache>();

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear();
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear(CacheReadMode.ConsiderExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear(CacheReadMode.IgnoreExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));
    }

    [Test]
    public void Clean_ValidValues()
    {
        var cache = Kernel.Get<VolatileCache>();

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow + TenMinutes);
        }

        cache.Clear();
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow + TenMinutes);
        }

        cache.Clear(CacheReadMode.ConsiderExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(StringValues.Count));

        cache.Clear(CacheReadMode.IgnoreExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));
    }

    #endregion SQLite-specific Clean

    #region Multiple Caches

    [Test, Repeat(50)]
    public void AddTimed_TwoCaches_NoMix()
    {
        const string Partition = "partition";
        const string Key = "key";

        var cache = Kernel.Get<VolatileCache>();
        using var another = new VolatileCache(new VolatileCacheSettings { CacheName = "another" }, clock: Kernel.Get<IClock>());

        try
        {
            cache.AddTimed(Partition, Key, 1, TenMinutes);
            another.AddTimed(Partition, Key, 2, TenMinutes);
            Assert.That(cache.Contains(Partition, Key), Is.True);
            Assert.That(another.Contains(Partition, Key), Is.True);
            Assert.That(cache[Partition, Key].Value, Is.EqualTo(1));
            Assert.That(another[Partition, Key].Value, Is.EqualTo(2));

            another.AddTimed(Partition, Key + Key, 3, TenMinutes);
            Assert.That(cache.Contains(Partition, Key + Key), Is.False);
            Assert.That(another.Contains(Partition, Key + Key), Is.True);
            Assert.That(another[Partition, Key + Key].Value, Is.EqualTo(3));
        }
        catch (Exception ex)
        {
            Console.Error.WriteLine($"{ex.Message} - {ex.GetType().Name} - {ex.StackTrace}");
            Console.Error.WriteLine(cache.LastError?.Message ?? "First cache has no errors");
            Console.Error.WriteLine(another.LastError?.Message ?? "Second cache has no errors");
            throw;
        }
    }

    #endregion Multiple Caches
}

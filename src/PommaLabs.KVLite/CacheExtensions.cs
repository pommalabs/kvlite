﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

/// <summary>
///   Extensions methods for <see cref="ICache"/>.
/// </summary>
public static partial class CacheExtensions
{
    #region Add

    /// <summary>
    ///   Adds a "timed" value with given partition and key. Value will last for the specified
    ///   lifetime and, if accessed before expiry, its lifetime will _not_ be extended.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="cache">The cache.</param>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="lifetime">The desired lifetime.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="ICache.MaxParentKeyCountPerEntry"/> to understand how many parent keys each
    ///   entry may have.
    /// </exception>
    public static void AddTimed<TVal>(this ICache cache, CacheKey partition, CacheKey key, TVal value, TimeSpan lifetime, IList<CacheKey> parentKeys = default)
    {
        // Preconditions
        if (cache == null) throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);

        cache.AddTimed(partition, key, value, cache.Clock.UtcNow + lifetime, parentKeys);
    }

    #endregion Add

    #region GetOrAdd

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "sliding" value with given partition and
    ///     key. Value will last as much as specified in given interval and, if accessed before
    ///     expiry, its lifetime will be extended by the interval itself.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="cache">The cache.</param>
    /// <param name="partition">The partition.</param>
    /// <param name="valueExpression">
    ///   The expression which will be evaluated in order to get key parts and the value.
    /// </param>
    /// <param name="interval">The interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by compiled
    ///   <paramref name="valueExpression"/>, in case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Either specified expression type is not supported or too many parent keys have been
    ///   specified for this entry. Please have a look at the
    ///   <see cref="ICache.MaxParentKeyCountPerEntry"/> to understand how many parent keys each
    ///   entry may have.
    /// </exception>
    public static TVal GetOrAddSliding<TVal>(
        this ICache cache, CacheKey partition, Expression<Func<TVal>> valueExpression, TimeSpan interval,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        // Preconditions
        if (cache == null) throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);
        ValidateExpression(valueExpression);

        return cache.GetOrAddSliding(
            partition, new CacheKey(valueExpression), valueExpression.Compile(), interval,
            parentKeys, valueFilterer);
    }

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "timed" value with given partition and
    ///     key. Value will last for the specified lifetime and, if accessed before expiry, its
    ///     lifetime will _not_ be extended.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="cache">The cache.</param>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="valueGetter">
    ///   The function that is called in order to get the value when it was not found inside the cache.
    /// </param>
    /// <param name="lifetime">The desired lifetime.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by <paramref name="valueGetter"/>, in
    ///   case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="valueGetter"/> is null.</exception>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="ICache.MaxParentKeyCountPerEntry"/> to understand how many parent keys each
    ///   entry may have.
    /// </exception>
    public static TVal GetOrAddTimed<TVal>(
        this ICache cache, CacheKey partition, CacheKey key, Func<TVal> valueGetter, TimeSpan lifetime,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        // Preconditions
        if (cache == null) throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);

        return cache.GetOrAddTimed(
            partition, key, valueGetter, cache.Clock.UtcNow + lifetime,
            parentKeys, valueFilterer);
    }

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "timed" value with given partition and
    ///     key. Value will last until the specified time and, if accessed before expiry, its
    ///     lifetime will _not_ be extended.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="cache">The cache.</param>
    /// <param name="partition">The partition.</param>
    /// <param name="valueExpression">
    ///   The expression which will be evaluated in order to get key parts and the value.
    /// </param>
    /// <param name="utcExpiry">The UTC expiry.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by compiled
    ///   <paramref name="valueExpression"/>, in case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Either specified expression type is not supported or too many parent keys have been
    ///   specified for this entry. Please have a look at the
    ///   <see cref="ICache.MaxParentKeyCountPerEntry"/> to understand how many parent keys each
    ///   entry may have.
    /// </exception>
    public static TVal GetOrAddTimed<TVal>(
        this ICache cache, CacheKey partition, Expression<Func<TVal>> valueExpression, DateTimeOffset utcExpiry,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        // Preconditions
        if (cache == null) throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);
        ValidateExpression(valueExpression);

        return cache.GetOrAddTimed(
            partition, new CacheKey(valueExpression), valueExpression.Compile(), utcExpiry,
            parentKeys, valueFilterer);
    }

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "timed" value with given partition and
    ///     key. Value will last for the specified lifetime and, if accessed before expiry, its
    ///     lifetime will _not_ be extended.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="cache">The cache.</param>
    /// <param name="partition">The partition.</param>
    /// <param name="valueExpression">
    ///   The expression which will be evaluated in order to get key parts and the value.
    /// </param>
    /// <param name="lifetime">The desired lifetime.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by compiled
    ///   <paramref name="valueExpression"/>, in case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Either specified expression type is not supported or too many parent keys have been
    ///   specified for this entry. Please have a look at the
    ///   <see cref="ICache.MaxParentKeyCountPerEntry"/> to understand how many parent keys each
    ///   entry may have.
    /// </exception>
    public static TVal GetOrAddTimed<TVal>(
        this ICache cache, CacheKey partition, Expression<Func<TVal>> valueExpression, TimeSpan lifetime,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        // Preconditions
        if (cache == null) throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);

        return cache.GetOrAddTimed(
            partition, valueExpression, cache.Clock.UtcNow + lifetime,
            parentKeys, valueFilterer);
    }

    #endregion GetOrAdd

    #region Helpers

    private static void ValidateExpression(LambdaExpression valueExpression)
    {
        if (valueExpression == null || valueExpression.Body is not MethodCallExpression)
        {
            throw new NotSupportedException(ErrorMessages.InvalidExpression);
        }
    }

    #endregion Helpers
}

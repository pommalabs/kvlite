﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using Polly;
using PommaLabs.KVLite.Polly;
using PommaLabs.KVLite.SQLite;

namespace PommaLabs.KVLite.Examples;

internal static class Polly
{
    public static void Run()
    {
        // Every KVLite cache can be interfaced with Polly: Memory, MySQL, PostgreSQL, ...
        var options = new KVLiteCacheProviderOptions();
        var cacheProvider = new KVLiteSyncCacheProvider<string>(PersistentCache.DefaultInstance, options);
        var cachePolicy = Policy.Cache(cacheProvider, TimeSpan.FromMinutes(10));

        var myGuid1 = cachePolicy.Execute(ctx => Guid.NewGuid().ToString(), new Context("MyGuid"));
        var myGuid2 = cachePolicy.Execute(ctx => Guid.NewGuid().ToString(), new Context("MyGuid"));

        // Two GUIDs are equal because they share the same key.
        Debug.Assert(myGuid1 == myGuid2);

        myGuid1 = cachePolicy.Execute(ctx => Guid.NewGuid().ToString(), new KVLiteContext("My", "Complex", "Key", 1));
        myGuid2 = cachePolicy.Execute(ctx => Guid.NewGuid().ToString(), new KVLiteContext("My", "Complex", "Key", 2));

        // Two GUIDs are not equal because they do not share the same key.
        Debug.Assert(myGuid1 != myGuid2);
    }
}

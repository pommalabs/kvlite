﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Globalization;
using Ninject;
using Serilog;
using Serilog.Events;

namespace PommaLabs.KVLite.UnitTests.Infrastructure;

internal abstract class AbstractTests
{
    static AbstractTests()
    {
        // Configure Serilog and LibLog logging.
        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .MinimumLevel.Is(LogEventLevel.Warning)
            .CreateLogger();
    }

    protected static IKernel Kernel { get; } = new StandardKernel(new NinjectConfig());

    protected static TimeSpan TenMinutes { get; } = TimeSpan.FromMinutes(10);

    #region Constants

    protected const int LargeEntryCount = 256;
    protected const int MediumEntryCount = 64;
    protected const int SmallEntryCount = 16;

    protected static readonly List<CacheKey> CacheKeys = Enumerable
        .Range(MinEntry, LargeEntryCount)
        .Select(x => new CacheKey(x.ToString(CultureInfo.InvariantCulture)))
        .ToList();

    protected static readonly List<StringValue> StringValues = Enumerable
        .Range(MinEntry, LargeEntryCount)
        .Select(x => new StringValue(x.ToString(CultureInfo.InvariantCulture)))
        .ToList();

    protected const int MinEntry = 10000;

    #endregion Constants

    protected readonly struct StringValue : IEquatable<StringValue>
    {
        public StringValue(string value)
        {
            Value = value;
        }

        public StringValue(StringValue other)
        {
            this = other;
        }

        public string Value { get; }

        public static bool operator !=(StringValue value1, StringValue value2) => !(value1 == value2);

        public static bool operator ==(StringValue value1, StringValue value2) => value1.Equals(value2);

        public override bool Equals(object obj) => obj is StringValue value && Equals(value);

        public bool Equals(StringValue other) => Value == other.Value;

        public override int GetHashCode() => Value.GetHashCode();
    }
}

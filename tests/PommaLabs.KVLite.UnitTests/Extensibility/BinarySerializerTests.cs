﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using System.Runtime.Serialization;
using System.Security.Claims;
using NUnit.Framework;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.Extensibility;

[TestFixture, Parallelizable, Category(nameof(Extensibility))]
internal sealed class BinarySerializerTests
{
    private static readonly BinarySerializer s_serializer = BinarySerializer.Instance;

    [Test]
    public void ShouldSerializeAndDeserializeClaimWithoutSpecifyingObjectType()
    {
        var x = new Claim("type", "value", "valueType", "issuer", "originalIssuer");
        Claim y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (Claim)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.Type.ShouldBe(x.Type);
        y.Value.ShouldBe(x.Value);
        y.ValueType.ShouldBe(x.ValueType);
        y.Issuer.ShouldBe(x.Issuer);
        y.OriginalIssuer.ShouldBe(x.OriginalIssuer);
    }

    [Test]
    public void ShouldSerializeAndDeserializeDataContractClassWithoutSpecifyingObjectType()
    {
        var x = new DataContractClass { Pino = "Gino" };
        DataContractClass y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (DataContractClass)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.Pino.ShouldBe(x.Pino);
    }

    [Test]
    public void ShouldSerializeAndDeserializeDataTableWithoutSpecifyingObjectType()
    {
        var x = new RandomDataTableGenerator().GenerateDataTable(10);
        DataTable y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (DataTable)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.TableName.ShouldBe(x.TableName);
        y.Rows.Count.ShouldBe(x.Rows.Count);
    }

    [Test]
    public void ShouldSerializeAndDeserializeDateTimeWithoutSpecifyingObjectType()
    {
        var x = DateTime.UtcNow;
        DateTime y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (DateTime)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(x);
    }

    [Test]
    public void ShouldSerializeAndDeserializeDictionaryWithoutSpecifyingObjectType()
    {
        var x = new Dictionary<string, object>
        {
            ["in"] = 123,
            ["do"] = 123.45,
            ["da"] = DateTime.UtcNow
        };
        Dictionary<string, object> y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (Dictionary<string, object>)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(x);
    }

    [Test]
    public void ShouldSerializeAndDeserializeDoubleWithoutSpecifyingObjectType()
    {
        const double X = 123.45;
        double y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, X);
            ms.Position = 0L;
            y = (double)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(X);
    }

    [Test]
    public void ShouldSerializeAndDeserializeInt32WithoutSpecifyingObjectType()
    {
        const int X = 123;
        int y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            // Serialize with Int32 type.
            var compressed = s_serializer.Serialize(ms, X);
            ms.Position = 0L;
            y = (int)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(X);

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            // Serialize with Object type.
            var compressed = s_serializer.Serialize<object>(ms, X);
            ms.Position = 0L;
            y = (int)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(X);
    }

    [Test]
    public void ShouldSerializeAndDeserializeNewTupleWithoutSpecifyingObjectType()
    {
        var x = (123, 123.45, DateTime.UtcNow);
        (int, double, DateTime) y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize(ms, x);
            ms.Position = 0L;
            y = ((int, double, DateTime))s_serializer.Deserialize<object>(ms, compressed);
        }

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = ((int, double, DateTime))s_serializer.Deserialize<object>(ms, compressed);
        }

        y.ShouldBe(x);
    }

    [Test]
    public void ShouldSerializeAndDeserializeNullWithoutSpecifyingObjectType()
    {
        using var ms = MemoryStreamManager.GetMemoryStream();
        var compressed = s_serializer.Serialize<object>(ms, null);
        ms.Position = 0L;
        s_serializer.Deserialize<object>(ms, compressed).ShouldBeNull();
    }

    [Test]
    public void ShouldSerializeAndDeserializeSimpleClassWithoutSpecifyingObjectType()
    {
        var x = new TestClass { A = 123 };
        TestClass y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (TestClass)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.A.ShouldBe(x.A);
    }

    [Test]
    public void ShouldSerializeAndDeserializeStringWithoutSpecifyingObjectType()
    {
        const string X = "123";
        string y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, X);
            ms.Position = 0L;
            y = (string)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(X);
    }

    [Test]
    public void ShouldSerializeAndDeserializeTupleWithoutSpecifyingObjectType()
    {
        var x = Tuple.Create(123, 123.45, DateTime.UtcNow);
        Tuple<int, double, DateTime> y;

        using (var ms = MemoryStreamManager.GetMemoryStream())
        {
            var compressed = s_serializer.Serialize<object>(ms, x);
            ms.Position = 0L;
            y = (Tuple<int, double, DateTime>)s_serializer.Deserialize<object>(ms, compressed);
        }
        y.ShouldBe(x);
    }

    [DataContract]
    private sealed class DataContractClass
    {
        [DataMember]
        public string Pino { get; set; }
    }

    private sealed class TestClass
    {
        public int A { get; set; }
    }
}

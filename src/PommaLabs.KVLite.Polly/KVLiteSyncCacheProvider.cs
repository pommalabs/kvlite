﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Polly.Caching;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.Polly;

/// <summary>
///   KVLite adapter for <see cref="ISyncCacheProvider"/>.
/// </summary>
public sealed class KVLiteSyncCacheProvider : ISyncCacheProvider
{
    /// <summary>
    ///   Initializes cache provider.
    /// </summary>
    /// <param name="backingCache">Backing KVLite cache.</param>
    /// <param name="options">KVLite cache options.</param>
    public KVLiteSyncCacheProvider(ICache backingCache, KVLiteCacheProviderOptions options)
    {
        BackingCache = backingCache ?? throw new ArgumentNullException(nameof(backingCache), ErrorMessages.NullCache);
        Options = options ?? throw new ArgumentNullException(nameof(options), ErrorMessages.NullSettings);
    }

    /// <summary>
    ///   Backing KVLite cache.
    /// </summary>
    public ICache BackingCache { get; }

    /// <summary>
    ///   KVLite cache provider options.
    /// </summary>
    public KVLiteCacheProviderOptions Options { get; }

    /// <summary>
    ///   Puts the specified value in the cache.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="value">The value to put into the cache.</param>
    /// <param name="ttl">The time-to-live for the cache entry.</param>
    public void Put(string key, object value, Ttl ttl)
    {
        if (ttl.SlidingExpiration)
        {
            BackingCache.AddSliding(Options.Partition, key, value, ttl.Timespan);
        }
        else
        {
            BackingCache.AddTimed(Options.Partition, key, value, ttl.Timespan);
        }
    }

    /// <summary>
    ///   Tries to get a value from the cache.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <returns>The value from cache, or null, if none was found.</returns>
    public (bool, object) TryGet(string key)
    {
        var cacheResult = BackingCache.Get<object>(Options.Partition, key);
        return (cacheResult.HasValue, cacheResult.ValueOrDefault());
    }
}

/// <summary>
///   KVLite adapter for <see cref="ISyncCacheProvider{TResult}"/>.
/// </summary>
/// <typeparam name="TResult">The result type.</typeparam>
public sealed class KVLiteSyncCacheProvider<TResult> : ISyncCacheProvider<TResult>
{
    /// <summary>
    ///   Initializes cache provider.
    /// </summary>
    /// <param name="backingCache">Backing KVLite cache.</param>
    /// <param name="options">KVLite cache options.</param>
    public KVLiteSyncCacheProvider(ICache backingCache, KVLiteCacheProviderOptions options)
    {
        BackingCache = backingCache ?? throw new ArgumentNullException(nameof(backingCache), ErrorMessages.NullCache);
        Options = options ?? throw new ArgumentNullException(nameof(options), ErrorMessages.NullSettings);
    }

    /// <summary>
    ///   Backing KVLite cache.
    /// </summary>
    public ICache BackingCache { get; }

    /// <summary>
    ///   KVLite cache provider options.
    /// </summary>
    public KVLiteCacheProviderOptions Options { get; }

    /// <summary>
    ///   Puts the specified value in the cache.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="value">The value to put into the cache.</param>
    /// <param name="ttl">The time-to-live for the cache entry.</param>
    public void Put(string key, TResult value, Ttl ttl)
    {
        if (ttl.SlidingExpiration)
        {
            BackingCache.AddSliding(Options.Partition, key, value, ttl.Timespan);
        }
        else
        {
            BackingCache.AddTimed(Options.Partition, key, value, ttl.Timespan);
        }
    }

    /// <summary>
    ///   Tries to get a value from the cache.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <returns>The value from cache, or null, if none was found.</returns>
    public (bool, TResult) TryGet(string key)
    {
        var cacheResult = BackingCache.Get<TResult>(Options.Partition, key);
        return (cacheResult.HasValue, cacheResult.ValueOrDefault());
    }
}

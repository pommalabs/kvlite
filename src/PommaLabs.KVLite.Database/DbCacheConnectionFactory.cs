﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.Database;

/// <summary>
///   Base class for cache connection factories.
/// </summary>
/// <typeparam name="TSettings">The type of the cache settings.</typeparam>
/// <typeparam name="TConnection">The type of the cache connection.</typeparam>
/// <typeparam name="TTransaction">The type of the SQL transaction.</typeparam>
/// <typeparam name="TCommand">The type of the SQL command.</typeparam>
public abstract class DbCacheConnectionFactory<TSettings, TConnection, TTransaction, TCommand>
    where TSettings : DbCacheSettings<TSettings>
    where TConnection : DbConnection
    where TTransaction : DbTransaction
    where TCommand : DbCommand
{
    /// <summary>
    ///   Initializes the cache connection factory.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    protected DbCacheConnectionFactory(TSettings settings, ILogger logger)
    {
        Settings = settings ?? throw new ArgumentNullException(nameof(settings), ErrorMessages.NullSettings);
        Logger = logger ?? NullLogger.Instance;

        Settings.PropertyChanged += OnSettingsPropertyChanged;
    }

    #region Configuration

    /// <summary>
    ///   The connection string, customized to improve cache management.
    /// </summary>
    protected string ConnectionString { get; set; }

    /// <summary>
    ///   Cache settings.
    /// </summary>
    protected TSettings Settings { get; }

    /// <summary>
    ///   Updates commands and queries if schema name or entries table name have been changed.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    protected void OnSettingsPropertyChanged(object sender, PropertyChangedEventArgs args)
    {
        if (sender == null || args == null)
        {
            // Update everything and exit.
            UpdateCommandsAndQueries();
            UpdateConnectionString();
            return;
        }

        switch (args.PropertyName)
        {
            case nameof(DbCacheSettings<TSettings>.CacheSchemaName):
            case nameof(DbCacheSettings<TSettings>.CacheEntriesTableName):
                UpdateCommandsAndQueries();
                break;

            case nameof(DbCacheSettings<TSettings>.ConnectionString):
                UpdateConnectionString();
                break;
        }
    }

    #endregion Configuration

    #region Helpers

    /// <summary>
    ///   Gets the log used by the cache.
    /// </summary>
    protected ILogger Logger { get; }

    #endregion Helpers

    #region Commands

    /// <summary>
    ///   Deletes a set of cache entries.
    /// </summary>
    public string DeleteCacheEntriesCommand { get; protected set; }

    /// <summary>
    ///   Deletes a cache entry using specified hash.
    /// </summary>
    public string DeleteCacheEntryCommand { get; protected set; }

    /// <summary>
    ///   Inserts or updates a cache entry.
    /// </summary>
    public string InsertOrUpdateCacheEntryCommand { get; protected set; }

    /// <summary>
    ///   Updates the expiry time of one cache entry.
    /// </summary>
    public string UpdateCacheEntryExpiryCommand { get; protected set; }

    /// <summary>
    ///   Creates cache entries table and required indexes, if they do not already exist.
    /// </summary>
    protected string CreateCacheSchemaCommand { get; set; }

    #endregion Commands

    #region Queries

    /// <summary>
    ///   Checks whether a valid entry with given hash exists.
    /// </summary>
    public string ContainsCacheEntryQuery { get; protected set; }

    /// <summary>
    ///   Counts valid or invalid cache entries.
    /// </summary>
    public string CountCacheEntriesQuery { get; protected set; }

    /// <summary>
    ///   Returns current cache size in bytes.
    /// </summary>
    public string GetCacheSizeInBytesQuery { get; protected set; }

    /// <summary>
    ///   Returns all valid cache entries without updating their expiry time.
    /// </summary>
    public string PeekCacheEntriesQuery { get; protected set; }

    /// <summary>
    ///   Returns a valid cache entry with given hash.
    /// </summary>
    public string PeekCacheEntryQuery { get; protected set; }

    /// <summary>
    ///   Returns a valid cache value with given hash.
    /// </summary>
    public string PeekCacheValueQuery { get; protected set; }

    #endregion Queries

    #region DB Connection

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public DbParameter AddParameter(TCommand command, string name, string value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);

        var parameter = command.CreateParameter();
        parameter.DbType = DbType.String;
        parameter.ParameterName = name;
        parameter.Value = value ?? (object)DBNull.Value;
        command.Parameters.Add(parameter);
        return parameter;
    }

    /// <summary>
    ///   Adds a struct database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public DbParameter AddParameter(TCommand command, string name, long value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);

        var parameter = command.CreateParameter();
        parameter.DbType = DbType.Int64;
        parameter.ParameterName = name;
        parameter.Value = value;
        command.Parameters.Add(parameter);
        return parameter;
    }

    /// <summary>
    ///   Adds a struct database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public DbParameter AddParameter(TCommand command, string name, long? value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);

        var parameter = command.CreateParameter();
        parameter.DbType = DbType.Int64;
        parameter.ParameterName = name;
        parameter.Value = value ?? (object)DBNull.Value;
        command.Parameters.Add(parameter);
        return parameter;
    }

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public abstract DbParameter AddParameter(TCommand command, string name, MemoryStream value);

    /// <summary>
    ///   Starts a database transaction.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <returns>An object representing the new transaction.</returns>
    public abstract TTransaction BeginTransaction(TConnection connection);

    /// <summary>
    ///   Creates a database command.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <param name="transaction">A started transaction.</param>
    /// <param name="text">Command text.</param>
    /// <returns>An object representing a database command.</returns>
    public abstract TCommand CreateCommand(TConnection connection, TTransaction transaction, string text);

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <returns>An open connection.</returns>
    public abstract TConnection OpenConnection();

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <param name="cancellationToken">The cancellation instruction.</param>
    /// <returns>An open connection.</returns>
    public abstract Task<TConnection> OpenConnectionAsync(CancellationToken cancellationToken);

    #endregion DB Connection

    #region Query optimization

    /// <summary>
    ///   The maximum length a key can have. Longer keys will be truncated. Default value is
    ///   2000, but each SQL connection factory might change it.
    /// </summary>
    public int MaxKeyNameLength { get; protected set; } = 2000;

    /// <summary>
    ///   The maximum length a partition can have. Longer partitions will be truncated. Default
    ///   value is 2000, but each SQL connection factory might change it.
    /// </summary>
    public int MaxPartitionNameLength { get; protected set; } = 2000;

    /// <summary>
    ///   The symbol used to enclose an identifier (left side).
    /// </summary>
    protected virtual string LeftIdentifierEncloser { get; } = string.Empty;

    /// <summary>
    ///   Function used to estimate cache size.
    /// </summary>
    protected virtual string LengthSqlFunction { get; } = "length";

    /// <summary>
    ///   Prefix used to identify parameters inside a SQL query or command.
    /// </summary>
    protected virtual string ParameterPrefix { get; } = "@";

    /// <summary>
    ///   The symbol used to enclose an identifier (right side).
    /// </summary>
    protected virtual string RightIdentifierEncloser { get; } = string.Empty;

    /// <summary>
    ///   SQL schema including DOT character. If schema was empty, then this property will be empty.
    /// </summary>
    protected virtual string SqlSchemaWithDot => string.IsNullOrEmpty(Settings.CacheSchemaName) ? string.Empty : $"{Settings.CacheSchemaName}.";

    /// <summary>
    ///   Minifies specified query.
    /// </summary>
    /// <param name="query">The query.</param>
    /// <returns>A query without comments and unnecessary blank characters.</returns>
    protected static string MinifyQuery(string query)
    {
        // Removes all SQL comments. Multiline excludes '/n' from '.' matches.
        query = Regex.Replace(query, "--.*", string.Empty, RegexOptions.Multiline, Constants.RegexMatchTimeout);

        // Removes all multiple blanks.
        query = Regex.Replace(query, @"\s+", " ", default, Constants.RegexMatchTimeout);

        // Removes initial and ending blanks.
        return query.Trim();
    }

    /// <summary>
    ///   This method is called when either the cache schema name or the cache entries table
    ///   name have been changed by the user.
    /// </summary>
    protected virtual void UpdateCommandsAndQueries()
    {
        const uint T = uint.MaxValue;

        var p = ParameterPrefix;
        var l = LeftIdentifierEncloser;
        var r = RightIdentifierEncloser;
        var s = SqlSchemaWithDot;

        #region Commands

        DeleteCacheEntryCommand = MinifyQuery($@"
                delete from {s}{l}{Settings.CacheEntriesTableName}{r}
                 where {Settings.HashColumnName} = {p}{nameof(DbCacheEntrySingleQuery.Hash)}
            ");

        DeleteCacheEntriesCommand = MinifyQuery($@"
                delete from {s}{l}{Settings.CacheEntriesTableName}{r}
                 where ({p}{nameof(DbCacheEntryGroupQuery.Hash)} is null or ({Settings.HashColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.Hash)} and {Settings.HashColumnName} <= {p}{nameof(DbCacheEntryGroupQuery.Hash)} + {T}))
                   and ({p}{nameof(DbCacheEntryGroupQuery.IgnoreExpiryDate)} = 1 or {Settings.UtcExpiryColumnName} < {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)})
            ");

        UpdateCacheEntryExpiryCommand = MinifyQuery($@"
                update {s}{l}{Settings.CacheEntriesTableName}{r}
                   set {Settings.UtcExpiryColumnName} = {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}
                 where {Settings.HashColumnName} = {p}{nameof(DbCacheEntrySingleQuery.Hash)}
            ");

        #endregion Commands

        #region Queries

        ContainsCacheEntryQuery = MinifyQuery($@"
                select count(*)
                  from {s}{l}{Settings.CacheEntriesTableName}{r} x
                 where x.{Settings.HashColumnName} = {p}{nameof(DbCacheEntrySingleQuery.Hash)}
                   and x.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}
                   and (x.{Settings.ParentHash0ColumnName} is null
                     or x.{Settings.ParentHash0ColumnName} in (select p0.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p0
                                                                where p0.{Settings.HashColumnName} = x.{Settings.ParentHash0ColumnName}
                                                                  and p0.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash1ColumnName} is null
                     or x.{Settings.ParentHash1ColumnName} in (select p1.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p1
                                                                where p1.{Settings.HashColumnName} = x.{Settings.ParentHash1ColumnName}
                                                                  and p1.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash2ColumnName} is null
                     or x.{Settings.ParentHash2ColumnName} in (select p2.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p2
                                                                where p2.{Settings.HashColumnName} = x.{Settings.ParentHash2ColumnName}
                                                                  and p2.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
            ");

        CountCacheEntriesQuery = MinifyQuery($@"
                select count(*)
                  from {s}{l}{Settings.CacheEntriesTableName}{r} x
                 where ({p}{nameof(DbCacheEntryGroupQuery.Hash)} is null
                     or (x.{Settings.HashColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.Hash)} and x.{Settings.HashColumnName} <= {p}{nameof(DbCacheEntryGroupQuery.Hash)} + {T}))
                   and ({p}{nameof(DbCacheEntryGroupQuery.IgnoreExpiryDate)} = 1
                     or x.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)})
                   and (x.{Settings.ParentHash0ColumnName} is null
                     or x.{Settings.ParentHash0ColumnName} in (select p0.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p0
                                                                where p0.{Settings.HashColumnName} = x.{Settings.ParentHash0ColumnName}
                                                                  and p0.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash1ColumnName} is null
                     or x.{Settings.ParentHash1ColumnName} in (select p1.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p1
                                                                where p1.{Settings.HashColumnName} = x.{Settings.ParentHash1ColumnName}
                                                                  and p1.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash2ColumnName} is null
                     or x.{Settings.ParentHash2ColumnName} in (select p2.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p2
                                                                where p2.{Settings.HashColumnName} = x.{Settings.ParentHash2ColumnName}
                                                                  and p2.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
            ");

        PeekCacheEntriesQuery = MinifyQuery($@"
                select x.{Settings.HashColumnName}        {l}{nameof(DbCacheValue.Hash)}{r},
                       x.{Settings.UtcExpiryColumnName}   {l}{nameof(DbCacheValue.UtcExpiry)}{r},
                       x.{Settings.IntervalColumnName}    {l}{nameof(DbCacheValue.Interval)}{r},
                       x.{Settings.ValueColumnName}       {l}{nameof(DbCacheValue.Value)}{r},
                       x.{Settings.CompressedColumnName}  {l}{nameof(DbCacheValue.Compressed)}{r},
                       x.{Settings.PartitionColumnName}   {l}{nameof(DbCacheEntry.Partition)}{r},
                       x.{Settings.KeyColumnName}         {l}{nameof(DbCacheEntry.Key)}{r},
                       x.{Settings.UtcCreationColumnName} {l}{nameof(DbCacheEntry.UtcCreation)}{r},
                       x.{Settings.ParentKey0ColumnName}  {l}{nameof(DbCacheEntry.ParentKey0)}{r},
                       x.{Settings.ParentKey1ColumnName}  {l}{nameof(DbCacheEntry.ParentKey1)}{r},
                       x.{Settings.ParentKey2ColumnName}  {l}{nameof(DbCacheEntry.ParentKey2)}{r}
                  from {s}{l}{Settings.CacheEntriesTableName}{r} x
                 where ({p}{nameof(DbCacheEntryGroupQuery.Hash)} is null
                     or (x.{Settings.HashColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.Hash)} and x.{Settings.HashColumnName} <= {p}{nameof(DbCacheEntryGroupQuery.Hash)} + {T}))
                   and ({p}{nameof(DbCacheEntryGroupQuery.IgnoreExpiryDate)} = 1
                     or x.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)})
                   and (x.{Settings.ParentHash0ColumnName} is null
                     or x.{Settings.ParentHash0ColumnName} in (select p0.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p0
                                                                where p0.{Settings.HashColumnName} = x.{Settings.ParentHash0ColumnName}
                                                                  and p0.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash1ColumnName} is null
                     or x.{Settings.ParentHash1ColumnName} in (select p1.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p1
                                                                where p1.{Settings.HashColumnName} = x.{Settings.ParentHash1ColumnName}
                                                                  and p1.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash2ColumnName} is null
                     or x.{Settings.ParentHash2ColumnName} in (select p2.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p2
                                                                where p2.{Settings.HashColumnName} = x.{Settings.ParentHash2ColumnName}
                                                                  and p2.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntryGroupQuery.UtcExpiry)}))
                 order by x.{Settings.PartitionColumnName}, x.{Settings.KeyColumnName}
            ");

        // Partition and key are not "select-ed" because the caller already knows them.
        PeekCacheEntryQuery = MinifyQuery($@"
                select x.{Settings.HashColumnName}        {l}{nameof(DbCacheValue.Hash)}{r},
                       x.{Settings.UtcExpiryColumnName}   {l}{nameof(DbCacheValue.UtcExpiry)}{r},
                       x.{Settings.IntervalColumnName}    {l}{nameof(DbCacheValue.Interval)}{r},
                       x.{Settings.ValueColumnName}       {l}{nameof(DbCacheValue.Value)}{r},
                       x.{Settings.CompressedColumnName}  {l}{nameof(DbCacheValue.Compressed)}{r},
                       x.{Settings.PartitionColumnName}   {l}{nameof(DbCacheEntry.Partition)}{r},
                       x.{Settings.KeyColumnName}         {l}{nameof(DbCacheEntry.Key)}{r},
                       x.{Settings.UtcCreationColumnName} {l}{nameof(DbCacheEntry.UtcCreation)}{r},
                       x.{Settings.ParentKey0ColumnName}  {l}{nameof(DbCacheEntry.ParentKey0)}{r},
                       x.{Settings.ParentKey1ColumnName}  {l}{nameof(DbCacheEntry.ParentKey1)}{r},
                       x.{Settings.ParentKey2ColumnName}  {l}{nameof(DbCacheEntry.ParentKey2)}{r}
                  from {s}{l}{Settings.CacheEntriesTableName}{r} x
                 where x.{Settings.HashColumnName} = {p}{nameof(DbCacheEntrySingleQuery.Hash)}
                   and ({p}{nameof(DbCacheEntrySingleQuery.IgnoreExpiryDate)} = 1
                     or x.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)})
                   and (x.{Settings.ParentHash0ColumnName} is null
                     or x.{Settings.ParentHash0ColumnName} in (select p0.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p0
                                                                where p0.{Settings.HashColumnName} = x.{Settings.ParentHash0ColumnName}
                                                                  and p0.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash1ColumnName} is null
                     or x.{Settings.ParentHash1ColumnName} in (select p1.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p1
                                                                where p1.{Settings.HashColumnName} = x.{Settings.ParentHash1ColumnName}
                                                                  and p1.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash2ColumnName} is null
                     or x.{Settings.ParentHash2ColumnName} in (select p2.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p2
                                                                where p2.{Settings.HashColumnName} = x.{Settings.ParentHash2ColumnName}
                                                                  and p2.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
            ");

        PeekCacheValueQuery = MinifyQuery($@"
                select x.{Settings.HashColumnName}        {l}{nameof(DbCacheValue.Hash)}{r},
                       x.{Settings.UtcExpiryColumnName}   {l}{nameof(DbCacheValue.UtcExpiry)}{r},
                       x.{Settings.IntervalColumnName}    {l}{nameof(DbCacheValue.Interval)}{r},
                       x.{Settings.ValueColumnName}       {l}{nameof(DbCacheValue.Value)}{r},
                       x.{Settings.CompressedColumnName}  {l}{nameof(DbCacheValue.Compressed)}{r}
                  from {s}{l}{Settings.CacheEntriesTableName}{r} x
                 where x.{Settings.HashColumnName} = {p}{nameof(DbCacheEntrySingleQuery.Hash)}
                   and ({p}{nameof(DbCacheEntrySingleQuery.IgnoreExpiryDate)} = 1
                     or x.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)})
                   and (x.{Settings.ParentHash0ColumnName} is null
                     or x.{Settings.ParentHash0ColumnName} in (select p0.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p0
                                                                where p0.{Settings.HashColumnName} = x.{Settings.ParentHash0ColumnName}
                                                                  and p0.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash1ColumnName} is null
                     or x.{Settings.ParentHash1ColumnName} in (select p1.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p1
                                                                where p1.{Settings.HashColumnName} = x.{Settings.ParentHash1ColumnName}
                                                                  and p1.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
                   and (x.{Settings.ParentHash2ColumnName} is null
                     or x.{Settings.ParentHash2ColumnName} in (select p2.{Settings.HashColumnName} from {s}{l}{Settings.CacheEntriesTableName}{r} p2
                                                                where p2.{Settings.HashColumnName} = x.{Settings.ParentHash2ColumnName}
                                                                  and p2.{Settings.UtcExpiryColumnName} >= {p}{nameof(DbCacheEntrySingleQuery.UtcExpiry)}))
            ");

        GetCacheSizeInBytesQuery = MinifyQuery($@"
                select sum({LengthSqlFunction}({Settings.ValueColumnName}))
                     + sum({LengthSqlFunction}({Settings.PartitionColumnName}))
                     + sum({LengthSqlFunction}({Settings.KeyColumnName}))
                     + count(*) * (4*8) -- Four fields of 8 bytes: hash, expiry, interval, creation
                  from {s}{l}{Settings.CacheEntriesTableName}{r}
            ");

        #endregion Queries
    }

    #endregion Query optimization

    #region Connection and Schema

    /// <summary>
    ///   Ensures cache schema is ready.
    /// </summary>
    protected virtual void EnsureSchemaIsReady()
    {
        if (string.IsNullOrWhiteSpace(ConnectionString) || string.IsNullOrWhiteSpace(CreateCacheSchemaCommand))
        {
            return;
        }

        using var db = OpenConnection();
        using var cm = db.CreateCommand();

        cm.CommandText = CreateCacheSchemaCommand;
        cm.ExecuteNonQuery();
    }

    /// <summary>
    ///   Updates the connection string used to operate the SQL cache.
    /// </summary>
    protected virtual void UpdateConnectionString()
    {
        ConnectionString = Settings.ConnectionString;

        if (!Settings.AutoCreateCacheSchema)
        {
            Logger.LogTrace(TraceMessages.AutoSchemaCreationDisabled);
            return;
        }

        try
        {
            EnsureSchemaIsReady();
        }
        catch (Exception ex)
        {
            Logger.LogWarning(ex, TraceMessages.InternalErrorOnCreateSchema, CreateCacheSchemaCommand);
        }
    }

    #endregion Connection and Schema
}

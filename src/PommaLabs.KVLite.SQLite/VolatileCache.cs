﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data.SQLite;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Database;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   An SQLite-based in-memory cache.
/// </summary>
public sealed class VolatileCache : DbCache<VolatileCache, VolatileCacheSettings, SQLiteCacheConnectionFactory<VolatileCacheSettings>, SQLiteConnection, SQLiteTransaction, SQLiteCommand>
{
    #region Default instance

    /// <summary>
    ///   Gets the default instance for this cache kind. Default instance is configured using
    ///   default cache settings.
    /// </summary>
    public static VolatileCache DefaultInstance { get; } = new(new VolatileCacheSettings());

    #endregion Default instance

    /// <summary>
    ///   Initializes a new instance of the <see cref="VolatileCache"/> class with given settings.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger,</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public VolatileCache(
        VolatileCacheSettings settings, ISerializer serializer = null, IClock clock = null,
        ILogger<VolatileCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : base(settings, new VolatileCacheConnectionFactory(settings, logger), serializer, clock, logger, interceptor, random)
    {
    }
}

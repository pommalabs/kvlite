﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

internal partial class CacheTests
{
    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Get_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Get<object>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Get_EmptyCache_Parallel(int entryCount)
    {
        var tasks = new List<Task<CacheResult<StringValue>>>();
        for (var i = 0; i < entryCount; ++i)
        {
            var l = i;
            var task = Task.Run(() => _cache.Get<StringValue>(CacheKeys[l], CacheKeys[l]));
            tasks.Add(task);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(tasks[i].Result.HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Get_FullCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            var entry = _cache.Get<StringValue>(CacheKeys[i], CacheKeys[i]);
            Assert.That(entry.Value, Is.EqualTo(StringValues[i]));
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Get_FullCache_Outdated(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Subtract(TenMinutes));
        }
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Get<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [Test]
    public void Get_LargeDataTable()
    {
        var dt = new RandomDataTableGenerator("A123", "Test", "Pi", "<3", "Pu").GenerateDataTable(LargeEntryCount);
        _cache.AddTimed(CacheKeys[0], dt.TableName, dt, TenMinutes);
        var storedDt = _cache.Get<DataTable>(CacheKeys[0], dt.TableName).Value;
        Assert.That(storedDt.Rows, Has.Count.EqualTo(dt.Rows.Count));
        for (var i = 0; i < dt.Rows.Count; ++i)
        {
            Assert.That(storedDt.Rows[i].ItemArray, Has.Length.EqualTo(dt.Rows[i].ItemArray.Length));
            for (var j = 0; j < dt.Rows[i].ItemArray.Length; ++j)
            {
                Assert.That(storedDt.Rows[i].ItemArray[j], Is.EqualTo(dt.Rows[i].ItemArray[j]));
            }
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Get_Typed_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Get<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetEntries_Pagination_NoPartition(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = _cache.GetEntries<StringValue>(new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = _cache.GetEntries<StringValue>(new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetEntries_Pagination_WithPartition(int entryCount)
    {
        var p = CacheKeys[0];

        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(p, CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = _cache.GetEntries<StringValue>(p, new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = _cache.GetEntries<StringValue>(p, new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task GetEntriesAsync_Pagination_NoPartition(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = await _cache.GetEntriesAsync<StringValue>(new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = await _cache.GetEntriesAsync<StringValue>(new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task GetEntriesAsync_Pagination_WithPartition(int entryCount)
    {
        var p = CacheKeys[0];

        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(p, CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = await _cache.GetEntriesAsync<StringValue>(p, new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = await _cache.GetEntriesAsync<StringValue>(p, new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetEntry_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.GetEntry<object>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetEntry_FullSlidingCache_TimeIncreased(int entryCount)
    {
        var interval = TenMinutes;
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddSliding(CacheKeys[i], CacheKeys[i], StringValues[i], interval);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            var entry = _cache.GetEntry<StringValue>(CacheKeys[i], CacheKeys[i]).Value;
            Assert.That(entry, Is.Not.Null);
            entry.UtcExpiry.ShouldBe(_cache.Clock.UtcNow + interval);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetEntry_Typed_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.GetEntry<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetMany_RightEntries_AfterAddSliding_InvalidTime(int entryCount)
    {
        AddSliding(_cache, entryCount, TimeSpan.FromSeconds(1));
        (_cache.Clock as FakeClock).Advance(TimeSpan.FromSeconds(2));
        var entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(i => i.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]), Is.False);
        }
        entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(x => x.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]), Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetMany_RightEntries_AfterAddSliding_ValidTime(int entryCount)
    {
        AddSliding(_cache, entryCount, TimeSpan.FromHours(1));
        var entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(i => i.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]));
        }
        entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(x => x.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]));
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetMany_RightEntries_AfterAddTimed_InvalidTime(int entryCount)
    {
        AddTimed(_cache, entryCount, _cache.Clock.UtcNow.Subtract(TenMinutes));
        var entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(i => i.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]), Is.False);
        }
        entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(x => x.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]), Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetMany_RightEntries_AfterAddTimed_ValidTime(int entryCount)
    {
        AddTimed(_cache, entryCount, _cache.Clock.UtcNow + TenMinutes);
        var entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(i => i.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]));
        }
        entries = new HashSet<StringValue>(_cache.GetEntries<StringValue>().Select(x => x.Value));
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(entries.Contains(StringValues[i]));
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void GetManyEntries_FullSlidingCache_TimeIncreased(int entryCount)
    {
        var interval = TenMinutes;
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddSliding(CacheKeys[i], CacheKeys[i], StringValues[i], interval);
        }
        (_cache.Clock as FakeClock).Advance(TimeSpan.FromMinutes(1));
        var entries = _cache.GetEntries<StringValue>().ToList();
        for (var i = 0; i < entryCount; ++i)
        {
            var entry = entries[i];
            Assert.That(entry, Is.Not.Null);
            entry.UtcExpiry.ShouldBe(_cache.Clock.UtcNow + interval);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Indexer_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache[CacheKeys[i], CacheKeys[i]].HasValue, Is.False);
        }
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading;
using System.Threading.Tasks;

namespace PommaLabs.KVLite;

/// <summary>
///   Internal interface used for unit tests.
/// </summary>
internal interface ICacheInternal : ICache
{
    long Clear(CacheReadMode cacheReadMode);

    long Clear(CacheKey partition, CacheReadMode cacheReadMode);

    Task<long> ClearAsync(CacheReadMode cacheReadMode, CancellationToken cancellationToken = default);

    Task<long> ClearAsync(CacheKey partition, CacheReadMode cacheReadMode, CancellationToken cancellationToken = default);

    int Count(CacheReadMode cacheReadMode);

    int Count(CacheKey partition, CacheReadMode cacheReadMode);

    long LongCount(CacheReadMode cacheReadMode);

    long LongCount(CacheKey partition, CacheReadMode cacheReadMode);
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Cache connection factory specialized for SQLite.
/// </summary>
public abstract class SQLiteCacheConnectionFactory<TSettings> : DbCacheConnectionFactory<TSettings, SQLiteConnection, SQLiteTransaction, SQLiteCommand>
    where TSettings : SQLiteCacheSettings<TSettings>
{
    private readonly string _dataSourcePropertyName;
    private readonly SQLiteJournalModeEnum _journalMode;
    private readonly string _storageMode;
    private string _getCacheEntriesSchemaQuery;
    private string _vacuumCommand;

    /// <summary>
    ///   Cache connection factory specialized for SQLite.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="storageMode">Storage mode.</param>
    /// <param name="journalMode">Journal mode.</param>
    /// <param name="dataSourcePropertyName">Data source property name.</param>
    protected SQLiteCacheConnectionFactory(TSettings settings, ILogger logger, string storageMode, SQLiteJournalModeEnum journalMode, string dataSourcePropertyName)
        : base(settings, logger)
    {
        _storageMode = storageMode;
        _journalMode = journalMode;
        _dataSourcePropertyName = dataSourcePropertyName;

        Settings.PropertyChanged += (sender, args) =>
        {
            if (args.PropertyName == _dataSourcePropertyName)
            {
                UpdateConnectionString();
            }
        };
    }

    /// <summary>
    ///   Runs VACUUM on the underlying SQLite database.
    /// </summary>
    public void Vacuum()
    {
        using var db = OpenConnection();
        using var cm = new SQLiteCommand(_vacuumCommand, db);

        // Vacuum cannot be run within a transaction.
        cm.ExecuteNonQuery();
    }

    /// <summary>
    ///   Ensures cache schema is ready.
    /// </summary>
    protected override void EnsureSchemaIsReady()
    {
        using var db = OpenConnection();
        using var cm = db.CreateCommand();

        var isSchemaReady = true;

        cm.CommandText = _getCacheEntriesSchemaQuery;
        using (var dataReader = cm.ExecuteReader())
        {
            isSchemaReady = IsCacheEntriesTableReady(dataReader);
        }

        if (!isSchemaReady)
        {
            // Creates cache entries table and required indexes.
            cm.CommandText = CreateCacheSchemaCommand;
            cm.ExecuteNonQuery();
        }
    }

    /// <summary>
    ///   Init SQLite connection string.
    /// </summary>
    /// <param name="dataSource">Data source.</param>
    /// <returns>SQLite connection string.</returns>
    protected string InitConnectionString(string dataSource)
    {
        return new SQLiteConnectionStringBuilder
        {
            FullUri = $"file:{dataSource}?mode={_storageMode}&cache=shared",
            JournalMode = _journalMode,
            SyncMode = SynchronizationModes.Off
        }.ConnectionString;
    }

    /// <summary>
    ///   This method is called when either the cache schema name or the cache entries table
    ///   name have been changed by the user.
    /// </summary>
    protected sealed override void UpdateCommandsAndQueries()
    {
        base.UpdateCommandsAndQueries();

        var p = ParameterPrefix;
        var s = SqlSchemaWithDot;

        #region Commands

        InsertOrUpdateCacheEntryCommand = MinifyQuery($@"
                insert into {s}{Settings.CacheEntriesTableName} (
                    {Settings.HashColumnName},
                    {Settings.UtcExpiryColumnName},
                    {Settings.IntervalColumnName},
                    {Settings.ValueColumnName},
                    {Settings.CompressedColumnName},
                    {Settings.PartitionColumnName},
                    {Settings.KeyColumnName},
                    {Settings.UtcCreationColumnName},
                    {Settings.ParentHash0ColumnName},
                    {Settings.ParentKey0ColumnName},
                    {Settings.ParentHash1ColumnName},
                    {Settings.ParentKey1ColumnName},
                    {Settings.ParentHash2ColumnName},
                    {Settings.ParentKey2ColumnName}
                )
                values (
                    {p}{nameof(DbCacheValue.Hash)},
                    {p}{nameof(DbCacheValue.UtcExpiry)},
                    {p}{nameof(DbCacheValue.Interval)},
                    {p}{nameof(DbCacheValue.Value)},
                    {p}{nameof(DbCacheValue.Compressed)},
                    {p}{nameof(DbCacheEntry.Partition)},
                    {p}{nameof(DbCacheEntry.Key)},
                    {p}{nameof(DbCacheEntry.UtcCreation)},
                    {p}{nameof(DbCacheEntry.ParentHash0)},
                    {p}{nameof(DbCacheEntry.ParentKey0)},
                    {p}{nameof(DbCacheEntry.ParentHash1)},
                    {p}{nameof(DbCacheEntry.ParentKey1)},
                    {p}{nameof(DbCacheEntry.ParentHash2)},
                    {p}{nameof(DbCacheEntry.ParentKey2)}
                )
                on conflict ({Settings.HashColumnName}) do update set
                    {Settings.UtcExpiryColumnName} = {p}{nameof(DbCacheValue.UtcExpiry)},
                    {Settings.IntervalColumnName} = {p}{nameof(DbCacheValue.Interval)},
                    {Settings.ValueColumnName} = {p}{nameof(DbCacheValue.Value)},
                    {Settings.CompressedColumnName} = {p}{nameof(DbCacheValue.Compressed)},
                    {Settings.UtcCreationColumnName} = {p}{nameof(DbCacheEntry.UtcCreation)},
                    {Settings.ParentHash0ColumnName} = {p}{nameof(DbCacheEntry.ParentHash0)},
                    {Settings.ParentKey0ColumnName} = {p}{nameof(DbCacheEntry.ParentKey0)},
                    {Settings.ParentHash1ColumnName} = {p}{nameof(DbCacheEntry.ParentHash1)},
                    {Settings.ParentKey1ColumnName} = {p}{nameof(DbCacheEntry.ParentKey1)},
                    {Settings.ParentHash2ColumnName} = {p}{nameof(DbCacheEntry.ParentHash2)},
                    {Settings.ParentKey2ColumnName} = {p}{nameof(DbCacheEntry.ParentKey2)};
            ");

        CreateCacheSchemaCommand = MinifyQuery($@"
                DROP TABLE IF EXISTS {s}{Settings.CacheEntriesTableName};
                CREATE TABLE {s}{Settings.CacheEntriesTableName} (
                    {Settings.IdColumnName} INTEGER PRIMARY KEY,
                    {Settings.HashColumnName} BIGINT NOT NULL,
                    {Settings.UtcExpiryColumnName} BIGINT NOT NULL,
                    {Settings.IntervalColumnName} BIGINT NOT NULL,
                    {Settings.ValueColumnName} BLOB NOT NULL,
                    {Settings.CompressedColumnName} BOOLEAN NOT NULL,
                    {Settings.PartitionColumnName} TEXT NOT NULL,
                    {Settings.KeyColumnName} TEXT NOT NULL,
                    {Settings.UtcCreationColumnName} BIGINT NOT NULL,
                    {Settings.ParentHash0ColumnName} BIGINT,
                    {Settings.ParentKey0ColumnName} TEXT,
                    {Settings.ParentHash1ColumnName} BIGINT,
                    {Settings.ParentKey1ColumnName} TEXT,
                    {Settings.ParentHash2ColumnName} BIGINT,
                    {Settings.ParentKey2ColumnName} TEXT,
                    CONSTRAINT uk_kvl_cache_entries UNIQUE ({Settings.HashColumnName})
                );
            ");

        #endregion Commands

        #region Queries

        PeekCacheEntriesQuery = MinifyQuery($@"
                {PeekCacheEntriesQuery}
                limit {p}{nameof(DbCacheEntryGroupQuery.Skip)}, {p}{nameof(DbCacheEntryGroupQuery.Take)}
            ");

        #endregion Queries

        #region Specific queries and commands

        _vacuumCommand = MinifyQuery(@"
                vacuum; -- Clears free list and makes DB file smaller
            ");

        _getCacheEntriesSchemaQuery = MinifyQuery($@"
                PRAGMA table_info({s}{Settings.CacheEntriesTableName})
            ");

        #endregion Specific queries and commands
    }

    #region DB Connection

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public override DbParameter AddParameter(SQLiteCommand command, string name, MemoryStream value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);
        ArgumentNullException.ThrowIfNull(value);

        var parameter = new SQLiteParameter(name, value.ToArray()) { DbType = DbType.Binary };
        command.Parameters.Add(parameter);
        return parameter;
    }

    /// <summary>
    ///   Starts a database transaction.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <returns>An object representing the new transaction.</returns>
    public override SQLiteTransaction BeginTransaction(SQLiteConnection connection)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(connection);

        return connection.BeginTransaction(IsolationLevel.ReadCommitted);
    }

    /// <summary>
    ///   Creates a database command.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <param name="transaction">A started transaction.</param>
    /// <param name="text">Command text.</param>
    /// <returns>An object representing a database command.</returns>
    public override SQLiteCommand CreateCommand(SQLiteConnection connection, SQLiteTransaction transaction, string text)
    {
        return new SQLiteCommand(text, connection, transaction) { CommandTimeout = (int)Settings.CommandTimeout.TotalSeconds };
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <returns>An open connection.</returns>
    public override SQLiteConnection OpenConnection()
    {
        var connection = new SQLiteConnection(ConnectionString);
        connection.Open();
        return connection;
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <param name="cancellationToken">The cancellation instruction.</param>
    /// <returns>An open connection.</returns>
    public override async Task<SQLiteConnection> OpenConnectionAsync(CancellationToken cancellationToken)
    {
        var connection = new SQLiteConnection(ConnectionString);
        await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
        return connection;
    }

    #endregion DB Connection

    private bool IsCacheEntriesTableReady(SQLiteDataReader dataReader)
    {
        var columns = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        while (dataReader.Read())
        {
            columns.Add(dataReader.GetValue(dataReader.GetOrdinal("name")) as string);
        }

        return columns.Count == 15
            && columns.Contains(Settings.IdColumnName)
            && columns.Contains(Settings.HashColumnName)
            && columns.Contains(Settings.UtcExpiryColumnName)
            && columns.Contains(Settings.IntervalColumnName)
            && columns.Contains(Settings.ValueColumnName)
            && columns.Contains(Settings.CompressedColumnName)
            && columns.Contains(Settings.PartitionColumnName)
            && columns.Contains(Settings.KeyColumnName)
            && columns.Contains(Settings.UtcCreationColumnName)
            && columns.Contains(Settings.ParentHash0ColumnName)
            && columns.Contains(Settings.ParentKey0ColumnName)
            && columns.Contains(Settings.ParentHash1ColumnName)
            && columns.Contains(Settings.ParentKey1ColumnName)
            && columns.Contains(Settings.ParentHash2ColumnName)
            && columns.Contains(Settings.ParentKey2ColumnName);
    }
}

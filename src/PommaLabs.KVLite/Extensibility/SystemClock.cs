﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   System clock implementation.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
public sealed class SystemClock : IClock
{
    /// <summary>
    ///   System clock singleton.
    /// </summary>
    public static SystemClock Instance { get; } = new();

    /// <summary>
    ///   Retrieves the current system time in UTC.
    /// </summary>
    public DateTimeOffset UtcNow => DateTimeOffset.UtcNow;
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.PostgreSql;

[TestFixture]
[Category(nameof(PostgreSql))]
[NonParallelizable]
internal sealed class PostgreSqlCacheTests : AbstractTests
{
    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        PostgreSqlCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance
}

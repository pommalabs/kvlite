﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data.SQLite;
using Microsoft.Extensions.Logging;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Cache connection factory specialized for <see cref="VolatileCache"/>.
/// </summary>
public sealed class VolatileCacheConnectionFactory : SQLiteCacheConnectionFactory<VolatileCacheSettings>
{
    /// <summary>
    ///   Since in-memory SQLite instances are deleted as soon as the connection is closed, then
    ///   we keep one dangling connection open, so that the store does not disappear.
    /// </summary>
    private SQLiteConnection _keepAliveConnection;

    /// <summary>
    ///   Cache connection factory specialized for <see cref="VolatileCache"/>.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    public VolatileCacheConnectionFactory(VolatileCacheSettings settings, ILogger<VolatileCache> logger)
        : base(settings, logger, "memory", SQLiteJournalModeEnum.Memory, nameof(settings.CacheName))
    {
        OnSettingsPropertyChanged(null, null);
    }

    /// <summary>
    ///   Updates the connection string used to operate the SQL cache.
    /// </summary>
    protected override void UpdateConnectionString()
    {
        ConnectionString = InitConnectionString(Settings.CacheName);

        _keepAliveConnection?.Dispose();
        _keepAliveConnection = OpenConnection();

        EnsureSchemaIsReady();
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.MySql;

[TestFixture]
[Category(nameof(MySql))]
[NonParallelizable]
internal sealed class MySqlCacheTests : AbstractTests
{
    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        MySqlCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance
}

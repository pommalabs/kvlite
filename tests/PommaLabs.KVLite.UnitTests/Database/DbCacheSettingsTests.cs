﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Runtime.Serialization;
using NUnit.Framework;
using PommaLabs.KVLite.Database;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.Database;

[TestFixture]
[Category(nameof(Database))]
[Parallelizable]
internal sealed class DbCacheSettingsTests
{
    private static readonly string[] s_invalidSchemaNames = [null, "!", "$sql_name", "#SQL_NAME#"];
    private static readonly string[] s_invalidSqlNames = [null, string.Empty, "!", "$sql_name", "#SQL_NAME#"];
    private static readonly string[] s_validSchemaNames = [string.Empty, "SqlName", "sql_name", "SQL_NAME"];
    private static readonly string[] s_validSqlNames = ["SqlName", "sql_name", "SQL_NAME"];

    [TestCase(false), TestCase(true)]
    public void AutoCreateCacheSchema_Set_ValueShouldBeSet(bool value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.AutoCreateCacheSchema = value;

        // Assert
        settings.AutoCreateCacheSchema.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void CacheEntriesTableName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.CacheEntriesTableName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void CacheEntriesTableName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.CacheEntriesTableName = value;

        // Assert
        settings.CacheEntriesTableName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSchemaNames))]
    public void CacheSchemaName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.CacheSchemaName = value);
    }

    [TestCaseSource(nameof(s_validSchemaNames))]
    public void CacheSchemaName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.CacheSchemaName = value;

        // Assert
        settings.CacheSchemaName.ShouldBe(value);
    }

    [TestCase(1.0), TestCase(10.0), TestCase(100.0)]
    public void CommandTimeout_Set_ValueShouldBeSet(double value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.CommandTimeout = TimeSpan.FromSeconds(value);

        // Assert
        settings.CommandTimeout.TotalSeconds.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void CompressedColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.CompressedColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void CompressedColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.CompressedColumnName = value;

        // Assert
        settings.CompressedColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void HashColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.HashColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void HashColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.HashColumnName = value;

        // Assert
        settings.HashColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void IdColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.IdColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void IdColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.IdColumnName = value;

        // Assert
        settings.IdColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void IntervalColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.IntervalColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void IntervalColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.IntervalColumnName = value;

        // Assert
        settings.IntervalColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void KeyColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.KeyColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void KeyColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.KeyColumnName = value;

        // Assert
        settings.KeyColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void PartitionColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.PartitionColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void PartitionColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.PartitionColumnName = value;

        // Assert
        settings.PartitionColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void UtcCreationColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.UtcCreationColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void UtcCreationColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.UtcCreationColumnName = value;

        // Assert
        settings.UtcCreationColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void UtcExpiryColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.UtcExpiryColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void UtcExpiryColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.UtcExpiryColumnName = value;

        // Assert
        settings.UtcExpiryColumnName.ShouldBe(value);
    }

    [TestCaseSource(nameof(s_invalidSqlNames))]
    public void ValueColumnName_SetWithInvalidSqlName_ArgumentExceptionShouldBeThrown(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act & Assert
        Should.Throw<ArgumentException>(() => settings.ValueColumnName = value);
    }

    [TestCaseSource(nameof(s_validSqlNames))]
    public void ValueColumnName_SetWithValidSqlName_ValueShouldBeSet(string value)
    {
        // Arrange
        var settings = TestCacheSettings.Create();

        // Act
        settings.ValueColumnName = value;

        // Assert
        settings.ValueColumnName.ShouldBe(value);
    }

    [Serializable, DataContract]
    private sealed class TestCacheSettings : DbCacheSettings<TestCacheSettings>
    {
        public static TestCacheSettings Create() => new();
    }
}

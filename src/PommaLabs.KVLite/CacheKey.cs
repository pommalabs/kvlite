﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading;
using PommaLabs.KVLite.Core;

namespace PommaLabs.KVLite;

/// <summary>
///   Represents a cache key, used to identify an entry inside a partition.
/// </summary>
[Serializable, DebuggerDisplay("{_value}")]
public readonly struct CacheKey : IEquatable<CacheKey>, IEquatable<string>, IComparable<CacheKey>, IComparable<string>
{
    /// <summary>
    ///   Used when receiving null, empty or blank keys.
    /// </summary>
    public const string NoValuePart = "_";

    /// <summary>
    ///   How many characters of a string can be taken in order to build a value part.
    /// </summary>
    private const int MaxStringLength = 32;

    /// <summary>
    ///   Character used to separate cache key parts.
    /// </summary>
    private const string PartSeparator = ":";

    /// <summary>
    ///   Cache key, represented as a string.
    /// </summary>
    private readonly string _value;

    /// <summary>
    ///   Builds a cache key using specified value.
    /// </summary>
    /// <param name="value">Value.</param>
    public CacheKey(string value)
    {
        _value = FormatValuePart(value);
    }

    /// <summary>
    ///   Builds a cache key using a lambda expression.
    /// </summary>
    /// <param name="lambdaExpression">Lambda expression.</param>
    public CacheKey(LambdaExpression lambdaExpression)
    {
        switch (lambdaExpression?.Body)
        {
            case MethodCallExpression mce:
                const int FixedPartCount = 2;
                var args = mce.Arguments;

                var valueParts = new string[FixedPartCount + args.Count];

                valueParts[0] = FormatValuePart(mce.Method.DeclaringType.FullName);
                valueParts[1] = FormatValuePart(mce.Method.Name);

                for (var i = 0; i < args.Count; ++i)
                {
                    var lambda = Expression.Lambda(args[i]).Compile();
                    valueParts[FixedPartCount + i] = FormatValuePart(lambda.DynamicInvoke());
                }

                _value = string.Join(PartSeparator, valueParts);
                break;

            default:
                _value = NoValuePart;
                break;
        }
    }

    /// <summary>
    ///   Builds a cache key using specified value parts.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public CacheKey(params string[] valueParts)
    {
        _value = string.Join(PartSeparator, valueParts.Select(vp => FormatValuePart(vp)));
    }

    /// <summary>
    ///   Builds a cache key using specified value parts.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public CacheKey(params CacheKey[] valueParts)
    {
        _value = string.Join(PartSeparator, valueParts.Select(vp => vp.Value));
    }

    /// <summary>
    ///   Builds a cache key using specified value parts.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public CacheKey(params object[] valueParts)
    {
        _value = string.Join(PartSeparator, valueParts.Select(vp => FormatValuePart(vp)));
    }

    /// <summary>
    ///   Builds a cache key using specified value.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="unsafe">Unsafe!</param>
    internal CacheKey(string value, bool @unsafe)
    {
        Debug.Assert(@unsafe);
        _value = value;
    }

    /// <summary>
    ///   True if original string was null, false otherwise.
    /// </summary>
    public bool IsNull => _value == null || _value == NoValuePart;

    /// <summary>
    ///   Cache key, represented as a string.
    /// </summary>
    public string Value => _value ?? NoValuePart;

    /// <summary>
    ///   Concatenates left part with specified right part.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>Left part concatenated with specified right part.</returns>
    public static CacheKey Add(CacheKey left, CacheKey right) => left.Concatenate(right);

    /// <summary>
    ///   Conversion from object parts to cache key.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public static CacheKey FromObjectArray(params object[] valueParts) => new(valueParts);

    /// <summary>
    ///   Conversion from string to cache key.
    /// </summary>
    /// <param name="value">Value.</param>
    public static CacheKey FromString(string value) => new(value);

    /// <summary>
    ///   Conversion from string parts to cache key.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public static CacheKey FromStringArray(string[] valueParts) => new(valueParts);

    /// <summary>
    ///   Implicit conversion from string to cache key.
    /// </summary>
    /// <param name="value">Value.</param>
    public static implicit operator CacheKey(string value) => new(value);

    /// <summary>
    ///   Implicit conversion from string parts to cache key.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public static implicit operator CacheKey(string[] valueParts) => new(valueParts);

    /// <summary>
    ///   Implicit conversion from object parts to cache key.
    /// </summary>
    /// <param name="valueParts">Value parts.</param>
    public static implicit operator CacheKey(object[] valueParts) => new(valueParts);

    /// <summary>
    ///   Implicit conversion from cache key to string.
    /// </summary>
    /// <param name="cacheKey">Cache key.</param>
    public static implicit operator string(CacheKey cacheKey) => cacheKey.Value;

    /// <summary>
    ///   Concatenates left part with specified right part.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>Left part concatenated with specified right part.</returns>
    public static CacheKey operator +(CacheKey left, CacheKey right) => left.Concatenate(right);

    /// <summary>
    ///   Concatenates this cache key (left part) with specified right part.
    /// </summary>
    /// <param name="right">Right.</param>
    /// <returns>This cache key (left part) concatenated with specified right part.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public CacheKey Concatenate(CacheKey right) => new(this, right);

    /// <summary>
    ///   Returns the cache key, represented as a string.
    /// </summary>
    /// <returns>The cache key, represented as a string.</returns>
    public override string ToString() => Value;

    /// <summary>
    ///   Truncates given string if its length is greater than specified <paramref name="maxLength"/>.
    /// </summary>
    /// <param name="maxLength">The length at which cache key should be truncated.</param>
    /// <returns>
    ///   A new cache key with the first <paramref name="maxLength"/> characters of this cache key.
    /// </returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public CacheKey Truncate(int maxLength)
    {
        var value = Value;
        maxLength = Math.Max(0, maxLength);
        if (value.Length <= maxLength)
        {
            return new CacheKey(value, @unsafe: true);
        }
        if (value[maxLength - 1] != PartSeparator[0])
        {
            return new CacheKey(value[0..maxLength], @unsafe: true);
        }
        return new CacheKey(value[0..(maxLength - 1)], @unsafe: true);
    }

    /// <summary>
    ///   Formats given value part according to internal rules.
    /// </summary>
    /// <param name="vp">Value part.</param>
    /// <returns>Formatted value part.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static string FormatValuePart(object vp) => vp switch
    {
        bool x => x ? "1" : "0",
        byte x => x.ToString(CultureInfo.InvariantCulture),
        byte[] x => Hashing.XXHash64.Calculate(x).ToString(CultureInfo.InvariantCulture),
        CacheKey x => x.Value,
        CancellationToken _ => NoValuePart,
        decimal x => x.ToString(CultureInfo.InvariantCulture),
        double x => x.ToString(CultureInfo.InvariantCulture),
        Enum x => x.ToString("D"),
        float x => x.ToString(CultureInfo.InvariantCulture),
        Guid x => x.ToString("N"),
        IObjectWithCacheKey x => x.GetCacheKey().Value,
        int x => x.ToString(CultureInfo.InvariantCulture),
        long x => x.ToString(CultureInfo.InvariantCulture),
        MemoryStream x => (x.CanRead && x.Position == 0L)
            ? Hashing.XXHash64.Calculate(x.ToArray()).ToString(CultureInfo.InvariantCulture)
            : Hashing.HashObject(x).ToString(CultureInfo.InvariantCulture),
        null => NoValuePart,
        sbyte x => x.ToString(CultureInfo.InvariantCulture),
        short x => x.ToString(CultureInfo.InvariantCulture),
        string x => FormatValuePart(x),
        uint x => x.ToString(CultureInfo.InvariantCulture),
        ulong x => x.ToString(CultureInfo.InvariantCulture),
        Uri x => FormatValuePart(x.ToString()),
        ushort x => x.ToString(CultureInfo.InvariantCulture),
        _ => Hashing.HashObject(vp).ToString(CultureInfo.InvariantCulture)
    };

    /// <summary>
    ///   Formats given value part so that:
    ///   - A null, empty or blank string is mapped to a placeholder.
    ///   - A string shorter than, or equal to, <see cref="MaxStringLength"/> is returned as
    ///   lower case.
    ///   - A string longer than <see cref="MaxStringLength"/> is cut, taking last
    ///     <see cref="MaxStringLength"/> characters and making them lower case. Then, the hash of
    ///   whole lower case string is appended.
    /// </summary>
    /// <param name="vp">Value part.</param>
    /// <returns>Formatted value part.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static string FormatValuePart(string vp)
    {
        if (string.IsNullOrWhiteSpace(vp)) return NoValuePart;

        vp = vp.ToUpperInvariant();
        if (vp.Length <= MaxStringLength) return vp;

        var hash = Hashing.XXHash32.Calculate(vp);
        vp = vp.Substring(vp.Length - MaxStringLength, MaxStringLength);

        return $"{vp}~{hash}";
    }

    #region Equality and comparison members

    /// <summary>
    ///   Compares two objects.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if they are not equal, false otherwise.</returns>
    public static bool operator !=(CacheKey left, CacheKey right) => !left.Equals(right);

    /// <summary>
    ///   Less operator overload.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if <paramref name="left"/> is less than <paramref name="right"/>.</returns>
    public static bool operator <(CacheKey left, CacheKey right)
    {
        return left.CompareTo(right) < 0;
    }

    /// <summary>
    ///   Less or equal operator overload.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if <paramref name="left"/> is less than or equal to <paramref name="right"/>.</returns>
    public static bool operator <=(CacheKey left, CacheKey right)
    {
        return left.CompareTo(right) <= 0;
    }

    /// <summary>
    ///   Compares two objects.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if they are equal, false otherwise.</returns>
    public static bool operator ==(CacheKey left, CacheKey right) => left.Equals(right);

    /// <summary>
    ///   Greater operator overload.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if <paramref name="left"/> is greater than <paramref name="right"/>.</returns>
    public static bool operator >(CacheKey left, CacheKey right)
    {
        return left.CompareTo(right) > 0;
    }

    /// <summary>
    ///   Greater or equal operator overload.
    /// </summary>
    /// <param name="left">Left.</param>
    /// <param name="right">Right.</param>
    /// <returns>True if <paramref name="left"/> is greater than or equal to <paramref name="right"/>.</returns>
    public static bool operator >=(CacheKey left, CacheKey right)
    {
        return left.CompareTo(right) >= 0;
    }

    /// <summary>
    ///   Compares this instance with other instance.
    /// </summary>
    /// <param name="other">Other instance.</param>
    /// <returns>Comparison result.</returns>
    public int CompareTo(CacheKey other) => string.CompareOrdinal(Value, other.Value);

    /// <summary>
    ///   Compares this instance with other instance.
    /// </summary>
    /// <param name="other">Other instance.</param>
    /// <returns>Comparison result.</returns>
    public int CompareTo(string other) => string.CompareOrdinal(Value, other);

    /// <summary>
    ///   Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>
    ///   true if the current object is equal to the <paramref name="other">other</paramref>
    ///   parameter; otherwise, false.
    /// </returns>
    public bool Equals(CacheKey other) => Value == other.Value;

    /// <summary>
    ///   Indicates whether the current object is equal to another object of
    ///   <see cref="string"/> type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>
    ///   true if the current object is equal to the <paramref name="other">other</paramref>
    ///   parameter; otherwise, false.
    /// </returns>
    public bool Equals(string other) => Value == FormatValuePart(other);

    /// <summary>
    ///   Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">The object to compare with the current instance.</param>
    /// <returns>
    ///   true if <paramref name="obj">obj</paramref> and this instance are the same type and
    ///   represent the same value; otherwise, false.
    /// </returns>
    public override bool Equals(object obj) => obj is CacheKey other && Equals(other);

    /// <summary>
    ///   Returns the hash code for this instance.
    /// </summary>
    /// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
    public override int GetHashCode() => unchecked((int)Hashing.XXHash32.CalculateRaw(Value));

    #endregion Equality and comparison members
}

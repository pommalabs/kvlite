﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;
using System.Reflection;

namespace PommaLabs.KVLite.Core;

internal static class KVLiteActivitySource
{
    private static readonly AssemblyName s_assemblyName = typeof(KVLiteActivitySource).Assembly.GetName();

    internal static ActivitySource Instance { get; } = new(s_assemblyName.Name, s_assemblyName.Version.ToString());
}

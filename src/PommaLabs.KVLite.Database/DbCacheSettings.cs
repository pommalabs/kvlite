﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.Database;

/// <summary>
///   Base class for cache settings. Contains settings shared among different caches.
/// </summary>
/// <typeparam name="TSettings">The type of the cache settings.</typeparam>
[Serializable, DataContract]
public class DbCacheSettings<TSettings> : AbstractCacheSettings<TSettings>
    where TSettings : DbCacheSettings<TSettings>
{
    /// <summary>
    ///   Backing field for <see cref="CacheEntriesTableName"/>.
    /// </summary>
    [SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "This field needs to be updated by child classes")]
    protected string _cacheEntriesTableName = "kvl_cache_entries";

    /// <summary>
    ///   Backing field for <see cref="CacheSchemaName"/>.
    /// </summary>
    [SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "This field needs to be updated by child classes")]
    protected string _cacheSchemaName = string.Empty;

    /// <summary>
    ///   Backing field for <see cref="AutoCreateCacheSchema"/>.
    /// </summary>
    private bool _autoCreateCacheSchema = true;

    /// <summary>
    ///   Backing field for <see cref="CommandTimeout"/>.
    /// </summary>
    private TimeSpan _commandTimeout = TimeSpan.FromSeconds(30);

    /// <summary>
    ///   Backing field for <see cref="CompressedColumnName"/>.
    /// </summary>
    private string _compressedColumnName = "kvle_compressed";

    /// <summary>
    ///   Backing field for <see cref="ConnectionString"/>.
    /// </summary>
    private string _connectionString;

    /// <summary>
    ///   Backing field for <see cref="HashColumnName"/>.
    /// </summary>
    private string _hashColumnName = "kvle_hash";

    /// <summary>
    ///   Backing field for <see cref="IdColumnName"/>.
    /// </summary>
    private string _idColumnName = "kvle_id";

    /// <summary>
    ///   Backing field for <see cref="IntervalColumnName"/>.
    /// </summary>
    private string _intervalColumnName = "kvle_interval";

    /// <summary>
    ///   Backing field for <see cref="KeyColumnName"/>.
    /// </summary>
    private string _keyColumnName = "kvle_key";

    /// <summary>
    ///   Backing field for <see cref="ParentHash0ColumnName"/>.
    /// </summary>
    private string _parentHash0ColumnName = "kvle_parent_hash0";

    /// <summary>
    ///   Backing field for <see cref="ParentHash1ColumnName"/>.
    /// </summary>
    private string _parentHash1ColumnName = "kvle_parent_hash1";

    /// <summary>
    ///   Backing field for <see cref="ParentHash2ColumnName"/>.
    /// </summary>
    private string _parentHash2ColumnName = "kvle_parent_hash2";

    /// <summary>
    ///   Backing field for <see cref="ParentKey0ColumnName"/>.
    /// </summary>
    private string _parentKey0ColumnName = "kvle_parent_key0";

    /// <summary>
    ///   Backing field for <see cref="ParentKey1ColumnName"/>.
    /// </summary>
    private string _parentKey1ColumnName = "kvle_parent_key1";

    /// <summary>
    ///   Backing field for <see cref="ParentKey2ColumnName"/>.
    /// </summary>
    private string _parentKey2ColumnName = "kvle_parent_key2";

    /// <summary>
    ///   Backing field for <see cref="PartitionColumnName"/>.
    /// </summary>
    private string _partitionColumnName = "kvle_partition";

    /// <summary>
    ///   Backing field for <see cref="UtcCreationColumnName"/>.
    /// </summary>
    private string _utcCreationColumnName = "kvle_creation";

    /// <summary>
    ///   Backing field for <see cref="UtcExpiryColumnName"/>.
    /// </summary>
    private string _utcExpiryColumnName = "kvle_expiry";

    /// <summary>
    ///   Backing field for <see cref="ValueColumnName"/>.
    /// </summary>
    private string _valueColumnName = "kvle_value";

    /// <summary>
    ///   Determines whether the library will try to automatically create cache schema (if
    ///   necessary) or not. Schema creation requires that the cache SQL user has DDL rights.
    ///   Defaults to true.
    /// </summary>
    [DataMember]
    public bool AutoCreateCacheSchema
    {
        get
        {
            return _autoCreateCacheSchema;
        }
        set
        {
            _autoCreateCacheSchema = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   The name of the table which holds cache entries.
    /// </summary>
    [DataMember]
    public string CacheEntriesTableName
    {
        get
        {
            var result = _cacheEntriesTableName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidCacheEntriesTableName, nameof(CacheEntriesTableName));

            _cacheEntriesTableName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   The schema which holds cache entries table.
    /// </summary>
    [DataMember]
    public string CacheSchemaName
    {
        get
        {
            var result = _cacheSchemaName;

            // Postconditions
            Debug.Assert(result?.Length == 0 || DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (value?.Length != 0 && !DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidCacheSchemaName, nameof(CacheSchemaName));

            _cacheSchemaName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   The timeout applied to SQL commands. Defaults to 30 seconds.
    /// </summary>
    [DataMember]
    public TimeSpan CommandTimeout
    {
        get
        {
            return _commandTimeout;
        }
        set
        {
            _commandTimeout = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheValue.Compressed"/>.
    /// </summary>
    [DataMember]
    public string CompressedColumnName
    {
        get
        {
            var result = _compressedColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(CompressedColumnName));

            _compressedColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   The connection string used to connect to the cache data provider.
    /// </summary>
    /// <exception cref="ArgumentException">
    ///   <paramref name="value"/> is null, empty or blank.
    /// </exception>
    [DataMember]
    public string ConnectionString
    {
        get
        {
            return _connectionString ?? string.Empty;
        }
        set
        {
            // Preconditions
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentException(ErrorMessages.NullOrEmptyConnectionString, nameof(ConnectionString));

            _connectionString = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheValue.Hash"/>.
    /// </summary>
    [DataMember]
    public string HashColumnName
    {
        get
        {
            var result = _hashColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(HashColumnName));

            _hashColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of automatically generated ID.
    /// </summary>
    [DataMember]
    public string IdColumnName
    {
        get
        {
            var result = _idColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(IdColumnName));

            _idColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheValue.Interval"/>.
    /// </summary>
    [DataMember]
    public string IntervalColumnName
    {
        get
        {
            var result = _intervalColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(IntervalColumnName));

            _intervalColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.Key"/>.
    /// </summary>
    [DataMember]
    public string KeyColumnName
    {
        get
        {
            var result = _keyColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(KeyColumnName));

            _keyColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentHash0"/>.
    /// </summary>
    [DataMember]
    public string ParentHash0ColumnName
    {
        get
        {
            var result = _parentHash0ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentHash0ColumnName));

            _parentHash0ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentHash1"/>.
    /// </summary>
    [DataMember]
    public string ParentHash1ColumnName
    {
        get
        {
            var result = _parentHash1ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentHash1ColumnName));

            _parentHash1ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentHash2"/>.
    /// </summary>
    [DataMember]
    public string ParentHash2ColumnName
    {
        get
        {
            var result = _parentHash2ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentHash2ColumnName));

            _parentHash2ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentKey0"/>.
    /// </summary>
    [DataMember]
    public string ParentKey0ColumnName
    {
        get
        {
            var result = _parentKey0ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentKey0ColumnName));

            _parentKey0ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentKey1"/>.
    /// </summary>
    [DataMember]
    public string ParentKey1ColumnName
    {
        get
        {
            var result = _parentKey1ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentKey1ColumnName));

            _parentKey1ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.ParentKey2"/>.
    /// </summary>
    [DataMember]
    public string ParentKey2ColumnName
    {
        get
        {
            var result = _parentKey2ColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ParentKey2ColumnName));

            _parentKey2ColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.Partition"/>.
    /// </summary>
    [DataMember]
    public string PartitionColumnName
    {
        get
        {
            var result = _partitionColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(PartitionColumnName));

            _partitionColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheEntry.UtcCreation"/>.
    /// </summary>
    [DataMember]
    public string UtcCreationColumnName
    {
        get
        {
            var result = _utcCreationColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(UtcCreationColumnName));

            _utcCreationColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheValue.UtcExpiry"/>.
    /// </summary>
    [DataMember]
    public string UtcExpiryColumnName
    {
        get
        {
            var result = _utcExpiryColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(UtcExpiryColumnName));

            _utcExpiryColumnName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   SQL column name of <see cref="DbCacheValue.Value"/>.
    /// </summary>
    [DataMember]
    public string ValueColumnName
    {
        get
        {
            var result = _valueColumnName;

            // Postconditions
            Debug.Assert(DbCacheSettings.IsValidSqlNameRegex.IsMatch(result));
            return result;
        }
        set
        {
            // Preconditions
            if (!DbCacheSettings.IsValidSqlNameRegex.IsMatch(value)) throw new ArgumentException(ErrorMessages.InvalidColumnName, nameof(ValueColumnName));

            _valueColumnName = value;
            OnPropertyChanged();
        }
    }
}

internal static class DbCacheSettings
{
    /// <summary>
    ///   Used to validate SQL names.
    /// </summary>
    public static Regex IsValidSqlNameRegex { get; } = new("^[a-z0-9_]+$", RegexOptions.IgnoreCase, Constants.RegexMatchTimeout);
}

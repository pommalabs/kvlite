﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Session;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging.Abstractions;
using Ninject;
using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.SqlServer;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.AspNetCore;

[TestFixture]
[Category(nameof(AspNetCore))]
[TestFixture(typeof(MemoryCache))]
[TestFixture(typeof(MySqlCache))]
[TestFixture(typeof(PersistentCache))]
[TestFixture(typeof(PostgreSqlCache))]
[TestFixture(typeof(SqlServerCache))]
[TestFixture(typeof(VolatileCache))]
[NonParallelizable]
internal sealed class KVLiteSessionExtensionsTests(Type cacheType) : AbstractTests
{
    [Test]
    public async Task ShouldGetTheSameObjectAfterSetWithCustomSerializer()
    {
        var x = Math.Round(Math.Pow(Math.PI, 2.0), 7);
        var y = new string('a', 21);
        var z = DateTime.Now.Date;
        var t = (x, y, z);

        var sessionKey = GetSessionKey();

        var session = CreateDistributedSession(sessionKey);
        session.SetObject(BinarySerializer.Instance, nameof(t), t);
        await session.CommitAsync();

        session = CreateDistributedSession(sessionKey);
        var fromSession = session.GetObject<(double x, string y, DateTime z)>(BinarySerializer.Instance, nameof(t));
        session.Clear();
        await session.CommitAsync();

        fromSession.HasValue.ShouldBeTrue();
        fromSession.Value.x.ShouldBe(x);
        fromSession.Value.y.ShouldBe(y);
        fromSession.Value.z.ShouldBe(z);
    }

    [Test]
    public async Task ShouldGetTheSameObjectAfterSetWithDefaultSerializer()
    {
        var x = Math.Round(Math.Pow(Math.PI, 2.0), 7);
        var y = new string('a', 21);
        var z = DateTime.Now;
        var t = (x, y, z);

        var sessionKey = GetSessionKey();

        var session = CreateDistributedSession(sessionKey);
        session.SetObject(nameof(t), t);
        await session.CommitAsync();

        session = CreateDistributedSession(sessionKey);
        var fromSession = session.GetObject<(double x, string y, DateTime z)>(nameof(t));
        session.Clear();
        await session.CommitAsync();

        fromSession.HasValue.ShouldBeTrue();
        fromSession.Value.x.ShouldBe(x);
        fromSession.Value.y.ShouldBe(y);
        fromSession.Value.z.ShouldBe(z);
    }

    private static string GetSessionKey()
    {
        return Guid.NewGuid().ToString();
    }

    private DistributedSession CreateDistributedSession(string sessionKey)
    {
        var cache = Kernel.Get(cacheType) as IDistributedCache;
        var genericTimeout = TimeSpan.FromMinutes(1);
        return new DistributedSession(
            cache, sessionKey, genericTimeout, genericTimeout,
            () => true, NullLoggerFactory.Instance, isNewSessionKey: true);
    }
}

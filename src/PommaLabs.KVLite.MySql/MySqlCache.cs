﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using MySqlConnector;
using PommaLabs.KVLite.Database;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.MySql;

/// <summary>
///   Cache backed by MySQL.
/// </summary>
public sealed class MySqlCache : DbCache<MySqlCache, MySqlCacheSettings, MySqlCacheConnectionFactory, MySqlConnection, MySqlTransaction, MySqlCommand>
{
    #region Default instance

    /// <summary>
    ///   Gets the default instance for this cache kind. Default instance is configured using
    ///   default cache settings.
    /// </summary>
    public static MySqlCache DefaultInstance { get; } = new(new MySqlCacheSettings());

    #endregion Default instance

    /// <summary>
    ///   Initializes a new instance of the <see cref="MySqlCache"/> class with given settings.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public MySqlCache(
        MySqlCacheSettings settings, ISerializer serializer = null, IClock clock = null,
        ILogger<MySqlCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : this(settings, new MySqlCacheConnectionFactory(settings, logger), serializer, clock, logger, interceptor, random)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="MySqlCache"/> class with given settings
    ///   and specified connection factory.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="connectionFactory">Cache connection factory.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public MySqlCache(
        MySqlCacheSettings settings, MySqlCacheConnectionFactory connectionFactory, ISerializer serializer = null,
        IClock clock = null, ILogger<MySqlCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : base(settings, connectionFactory, serializer, clock, logger, interceptor, random)
    {
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Database;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.SqlServer;

/// <summary>
///   Cache backed by SQL Server.
/// </summary>
public sealed class SqlServerCache : DbCache<SqlServerCache, SqlServerCacheSettings, SqlServerCacheConnectionFactory, SqlConnection, SqlTransaction, SqlCommand>
{
    #region Default instance

    /// <summary>
    ///   Gets the default instance for this cache kind. Default instance is configured using
    ///   default cache settings.
    /// </summary>
    public static SqlServerCache DefaultInstance { get; } = new(new SqlServerCacheSettings());

    #endregion Default instance

    /// <summary>
    ///   Initializes a new instance of the <see cref="SqlServerCache"/> class with given settings.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public SqlServerCache(
        SqlServerCacheSettings settings, ISerializer serializer = null, IClock clock = null,
        ILogger<SqlServerCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : this(settings, new SqlServerCacheConnectionFactory(settings, logger), serializer, clock, logger, interceptor, random)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="SqlServerCache"/> class with given
    ///   settings and specified connection factory.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="connectionFactory">Cache connection factory.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public SqlServerCache(
        SqlServerCacheSettings settings, SqlServerCacheConnectionFactory connectionFactory, ISerializer serializer = null,
        IClock clock = null, ILogger<SqlServerCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : base(settings, connectionFactory, serializer, clock, logger, interceptor, random)
    {
    }
}

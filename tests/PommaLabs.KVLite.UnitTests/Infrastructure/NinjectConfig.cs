﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Runtime.Versioning;
using dotenv.net;
using Microsoft.Extensions.Logging;
using Ninject;
using Ninject.Modules;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.SqlServer;
using Serilog;

namespace PommaLabs.KVLite.UnitTests.Infrastructure;

/// <summary>
///   Bindings for KVLite.
/// </summary>
internal sealed class NinjectConfig : NinjectModule
{
    private readonly ILoggerFactory _loggerFactory = LoggerFactory.Create(
        builder => builder.AddSerilog(dispose: true));

    public override void Load()
    {
        DotEnv.Load(new DotEnvOptions(probeForEnv: true, probeLevelsToSearch: 6));

        // Cache entries table name is generated in such a way that tests
        // from different TFMs (e.g. net8.0 and net9.0) can be executed in parallel.
        // .NET 9.0 introduced the capability of running in parallel tests
        // belonging to different DLLs compiled with different TFMs.
        var cacheEntriesTableName = GetCacheEntriesTableName();

        Bind<ISerializer>()
            .ToConstant(BinarySerializer.Instance)
            .InSingletonScope();

        Bind<IClock>()
            .ToMethod(_ => new FakeClock(SystemClock.Instance.UtcNow))
            .InTransientScope();

        Bind(typeof(ILogger<>))
            .ToMethod(ctx => typeof(LoggerFactoryExtensions)
                .GetMethods()
                .Single(x => x.Name == nameof(_loggerFactory.CreateLogger) && x.ContainsGenericParameters)
                .MakeGenericMethod(ctx.Request.ParentRequest!.Service)
                .Invoke(null, [_loggerFactory]))
            .InSingletonScope();

        Bind<IInterceptor>()
            .ToConstant(NoOpInterceptor.Instance)
            .InSingletonScope();

        Bind<IRandom>()
            .To<SystemRandom>()
            .InTransientScope();

        Bind<MemoryCacheSettings>()
            .ToConstant(new MemoryCacheSettings { MaxCacheSizeInMB = 512 })
            .InSingletonScope();

        Bind<MemoryCache>()
            .ToSelf()
            .InSingletonScope();

        Bind<MySqlCacheSettings>()
            .ToMethod(_ =>
            {
                var settings = new MySqlCacheSettings
                {
                    ConnectionString = DatabaseHelper.GetMySqlConnectionString(),
                    CacheEntriesTableName = cacheEntriesTableName,
                };
                DatabaseHelper.EnsureMySqlIsAvailable(settings);
                return settings;
            })
            .InSingletonScope();

        Bind<MySqlCache>()
            .ToSelf()
            .InSingletonScope();

        Bind<PostgreSqlCacheSettings>()
            .ToMethod(_ =>
            {
                var settings = new PostgreSqlCacheSettings
                {
                    ConnectionString = DatabaseHelper.GetPostgreSqlConnectionString(),
                    CacheEntriesTableName = cacheEntriesTableName,
                };
                DatabaseHelper.EnsurePostgreSqlIsAvailable(settings);
                return settings;
            })
            .InSingletonScope();

        Bind<PostgreSqlCache>()
            .ToSelf()
            .InSingletonScope();

        Bind<SqlServerCacheSettings>()
            .ToMethod(_ =>
            {
                var settings = new SqlServerCacheSettings
                {
                    ConnectionString = DatabaseHelper.GetSqlServerConnectionString(),
                    CacheEntriesTableName = cacheEntriesTableName,
                };
                DatabaseHelper.EnsureSqlServerIsAvailable(settings);
                return settings;
            })
            .InSingletonScope();

        Bind<SqlServerCache>()
            .ToSelf()
            .InSingletonScope();

        Bind<VolatileCacheSettings>()
            .ToConstant(new VolatileCacheSettings())
            .InSingletonScope();

        Bind<VolatileCache>()
            .ToSelf()
            .InSingletonScope();
    }

    private string GetCacheEntriesTableName()
    {
        // Example output: .NET 8.0
        var frameworkDisplayName = GetFrameworkDisplayName();
        // Example output: net80
        var escapedFrameworkVersion = frameworkDisplayName
            .Replace(" ", string.Empty)
            .Replace(".", string.Empty)
            .ToLowerInvariant();
        return $"kvl_cache_entries_{escapedFrameworkVersion}";
    }

    private string GetFrameworkDisplayName()
    {
        var assembly = GetType().Assembly;
        var targetFrameworkAttribute = assembly
            .GetCustomAttributes(typeof(TargetFrameworkAttribute), inherit: false)
            .Cast<TargetFrameworkAttribute>()
            .Single();
        return targetFrameworkAttribute.FrameworkDisplayName;
    }
}

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [12.4.0] - 2024-12-11

### Added

- Added DLLs compiled for .NET 9.0.

### Changed

- Refactored some tests by properly using the `TestFixture` attribute.

### Removed

- Removed DLLs compiled for .NET 6.0, since that version is not supported anymore.

## [12.3.0] - 2023-11-15

### Added

- Added DLLs compiled for .NET 8.0.

### Removed

- Removed DLLs compiled for .NET 7.0: projects targeting that version will still be able to use KVLite,
  as they will automatically fall back to DLLs compiled for .NET 6.

## [12.2.1] - 2022-12-18

### Changed

- Health check result is cached for 27 seconds when its status is healthy,
  otherwise it is cached for 9 seconds.

## [12.2.0] - 2022-12-17

### Changed

- Health check result is cached for 30 seconds.

## [12.1.0] - 2022-12-15

### Added

- Added support for .NET 7.0.

### Removed

- Dropped support for .NET Core 3.1.
- Dropped support for IdentityServer4 adapter.

## [12.0.0] - 2022-01-23

### Removed

- `NoOpCache` and related classes have been removed.
- `NoOpSerializer` has been removed.
- Oracle driver has been removed: it is hard to keep it tested automatically.
- .NET Framework 4.6.1 and 4.7.2 are not supported anymore.

## [11.1.0] - 2021-11-28

### Changed

- Microsoft.Data.SqlClient driver has been updated to v4. That releases contains
  [some breaking changes](https://techcommunity.microsoft.com/t5/sql-server/released-general-availability-of-microsoft-data-sqlclient-4-0/ba-p/2983346)
  which should be handled within specified connection string.
- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.6.1 and 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

## [11.0.7] - 2021-09-20

### Fixed

- Fixed a regression in "CacheSchemaName" setting validation.

## [11.0.6] - 2021-09-19

### Changed

- Addressed SonarCloud warnings.

### Fixed

- Validation of some DB cache settings (like table name) was not correctly applied.

## [11.0.5] - 2021-04-25

### Added

- Get and Add operations have been wrapped in .NET activities.

### Changed

- Updated NuGet dependencies.

## [11.0.4] - 2021-03-16

### Changed

- Updated NuGet dependencies.

### Fixed

- Caching enumerable utility was not working at all...

## [11.0.3] - 2021-03-14

### Changed

- Updated NuGet dependencies.

## [11.0.2] - 2021-01-15

### Changed

- Updated NuGet dependencies.
- "IClock" and "IRandom" have been hidden from IntelliSense.

## [11.0.1] - 2020-12-06

### Changed

- Updated NuGet dependencies.

## [11.0.0] - 2020-11-01

### Changed

- Microsoft.IO.RecyclableMemoryStream is the current memory stream manager.
- Library has been updated in order to respect most FxCop rules.
- "continueOnCapturedContext" parameter has been removed from all methods where it appeared.

## [10.1.2] - 2020-10-31

### Changed

- Updated NuGet dependencies.
- Cache key now handles byte arrays and memory streams more efficiently.

## [10.1.1] - 2020-10-26

### Changed

- Updated NuGet dependencies.

## [10.1.0] - 2020-08-12

### Added

- Settings for DB drivers now allow configuring automatic cache schema creation (`AutoCreateCacheSchema` property).

### Changed

- Updated NuGet dependencies.
- SQL Server driver now creates tables with IDENTITY ID.

## [10.0.5] - 2020-06-17

### Changed

- Updated NuGet dependencies.
- Embedded package icon and license.

## [10.0.4] - 2020-06-13

### Changed

- Updated NuGet dependencies.

### Fixed

- Cache key now properly handles URI key parts.

## [10.0.3] - 2020-06-02

### Changed

- Updated NuGet dependencies.

## [10.0.2] - 2020-06-01

### Changed

- Updated NuGet dependencies.
- Added a new setting to control timeout of SQL commands.
- Restored support for .NET Standard 2.0 and .NET Framework 4.6.1.

## [10.0.1] - 2020-05-28

### Changed

- Updated NuGet dependencies.

## [10.0.0] - 2020-03-08

### Changed

- Cache key improvements.
- Embedded "Core" DLL into main one.
- Removed TaskUtils class.
- Hashing module is now internal.
- Moved some methods from ICache interface to extension methods.
- Simplified delegate required for async GetOrAdd.
- The usage of "item" and "entry" terms was confusing. All occurrences of "item" term have been replaced with "entry".

## [9.3.3] - 2020-03-02

### Changed

- Cache key improvements.

## [9.3.2] - 2020-03-01

### Changed

- Updated NuGet dependencies.
- Added "valueFilterer" parameter to GetOrAdd* overloads.
- Added an interceptor interface.

## [9.3.1] - 2020-02-28

### Changed

- MySQL tables are now created with InnoDB engine and "utf8mb4_unicode_ci" encoding.

## [9.3.0] - 2020-02-25

### Changed

- GetOrAdd overloads now understand simple method call expressions.
- Dropped support for Entity Framework adapter.
- Dropped support for IdentityServer3 adapter.
- Dropped support for WebApi adapter.
- Dropped support for WebForms adapter.
- Added support for .NET Standard 2.1.
- Dropped support for .NET Standard 2.0 and .NET Framework 4.6.1.

## [9.2.7] - 2020-02-06

### Changed

- Fixed VolatileCache.

## [9.2.6] - 2020-02-06

### Changed

- `Add*` and `GetOrAdd*` are sync again.

## [9.2.5] - 2020-02-04

### Changed

- Fixed concurrency issue with CacheKey string interning.

## [9.2.4] - 2020-01-03

### Changed

- Revert (again...) SQLite driver to System.Data.SQLite.

## [9.2.3] - 2020-01-02

### Changed

- Use CodeProject.ObjectPool for stream pooling.

## [9.2.2] - 2019-12-31

### Changed

- Revert SQLite driver to Microsoft.Data.Sqlite.

## [9.2.1] - 2019-12-30

### Changed

- Using DI abstractions NuGet package.

## [9.2.0] - 2019-12-28

### Changed

- Package updates.
- Removed LibLog, which is now obsolete.
- Using standard Microsoft.Extensions.Logging classes for internal log.
- Using MySqlConnector as MySQL driver.
- Using System.Data.SQLite as SQLite driver.

## [9.1.2] - 2019-08-31

### Changed

- Package updates.
- Added some basic string interning to CacheKey.

## [9.1.1] - 2019-06-27

### Changed

- Switched from System.Data.SqlClient to Microsoft.Data.SqlClient.
- Preserve table name casing for all drivers.
- Column names can be configured using settings.

## [9.0.6] - 2019-04-25

### Changed

- Preserve table name casing for PostgreSQL.

## [9.0.5] - 2019-02-25

### Changed

- Fixed health check for KVLite.

## [9.0.4] - 2019-02-24

### Changed

- Added health check for KVLite.

## [9.0.3] - 2018-12-26

### Changed

- Optimized CacheKey.Truncate method.

## [9.0.2] - 2018-12-26

### Changed

- Fix pagination with query options.

## [9.0.1] - 2018-12-26

### Changed

- Updated lots of packages.
- Added support for .NET Framework 4.7.2.
- Dropped support for .NET Framework 4.5.2.
- Modified core interfaces.
- Dropped "default" partition concept.

## [8.1.5] - 2018-08-29

### Changed

- Fix public signature on Linux.

## [8.1.3] - 2018-08-17

### Changed

- Updated Oracle driver to latest version for .NET Core.
- Fixed links in NuGet packages.

## [8.1.2] - 2018-08-16

### Changed

- Fixed (again) schema handling for SQL Server automatic table creation.

## [8.1.1] - 2018-08-15

### Changed

- Fixed schema handling for SQL Server automatic table creation.
- SQL Server driver creates a default CacheEntries table in KVLite schema.

## [8.1.0] - 2018-08-14

### Changed

- LibLog is now used in private mode.
- Reverted IClock interface to a custom one.
- Moved to GitLab, which allowed full CI with Oracle.

## [8.0.2] - 2018-06-30

### Changed

- Restored dependency on LibLog.
- Package updates.
- Added timings to tracing.

## [8.0.1] - 2018-06-12

### Changed

- Removed all compressors, which might be embedded into serializers.
- Updated ISerializer interface in order to add compression management.
- FsPickler+Deflate is now the default binary serializer.
- Removed JsonSerializer, now fully replaced by new BinarySerializer.
- Added Oracle driver for .NET Core (still in beta).
- Updated MySQL driver to latest version.
- Only the main DLL will be signed.
- Refreshed the WebForms adapter.
- Removed anti tamper code.
- Removed CanSerialize/CanDeserialize from ISerializer interface.
- Persistent SQLite cache performs an automatic VACUUM during dependency registration.

## [7.2.3] - 2018-04-05

### Changed

- Updated WebAPI2 output cache.
- Updated Microsoft libs for .NET461.

## [7.2.2] - 2018-04-01

### Changed

- Added KVLiteContext with CacheKey for Polly.

## [7.2.1] - 2018-04-01

### Changed

- Added CacheKey builder.

## [7.2.0] - 2018-04-01

### Changed

- Reimplemented MemoryCache.

## [7.1.2] - 2018-03-31

### Changed

- Implemented a [Polly cache provider](https://github.com/App-vNext/Polly/wiki/Cache).
- Using latest ODP.NET Core beta for Oracle driver on .NET Standard 2.0.

## [7.1.1] - 2018-03-24

### Changed

- Implemented a deconstructor for CacheResult (issue #2).
- Updated IdentityServer4 to v2.1.3.

## [7.1.0] - 2018-03-18

### Changed

- Removed dependency on Dapper.Signed (issue #1).
- IdentityServer3/4: Moving type name from cache partitions to keys.

## [7.0.3] - 2018-03-06

### Changed

- Adding type name to IdentityServer3/4 cache partitions.

## [7.0.2] - 2018-03-05

### Changed

- Restoring transactions with isolation level `READ_COMMITTED` for Oracle.

## [7.0.1] - 2018-03-05

### Changed

- Updated JSON.NET to v11.
- Removed NodaTime and updated interfaces accordingly.
- Some integration tests have been setup on AppVeyor.
- Many unit tests can now run in parallel.
- MemoryCache sample driver can now run on .NET Standard 2.0.
- Using MySql.Data as low level driver for MySQL and MariaDB.
- Using transactions with isolation level `READ_UNCOMMITTED` for all operations.

## [6.4.4] - 2017-10-31

### Changed

- Using MySqlConnector as low level driver for MySQL and MariaDB.
- Dynamic JSON deserialization of date/time properties should produce a property with DateTimeOffset type.
- Dynamic JSON deserialization of double/decimal properties should produce a property with Decimal type.

## [6.4.3] - 2017-10-29

### Changed

- Slightly improved documentation.
- New ASP.NET Core session helpers.

## [6.4.2] - 2017-10-28

### Changed

- Updated NodaTime to v2.2.2.
- Updated EF to v6.2.0.

## [6.4.1] - 2017-10-22

### Changed

- Updated NodaTime to v2.2.1.
- Added support for .NET Framework 4.7.1 and 4.5.2.

## [6.4.0] - 2017-08-16

### Changed

- Added support for .NET Standard 2.0 and .NET Framework 4.6.1.
- All previous versions of above toolkits are not supported anymore.
- Completed PostgreSQL driver.

## [6.3.3] - 2017-07-30

### Changed

- Default JSON serializer now has all converters for NodaTime types.
- Added registrations for .NET Core services collections.
- Settings have been moved to cleanup connection factories.

## [6.3.2] - 2017-07-19

### Changed

- Default JSON serializer now has a custom converter for Claim class.
- Updated NodaTime.

## [6.3.1] - 2017-07-09

### Changed

- Renamed all methods involving default partition.
- Updated NodaTime.

## [6.3.0] - 2017-07-09

### Changed

- Changed SQL table structure, now using hashes for faster lookups.
- Removed embedded Thrower and a few unneeded dependencies.
- Reduced the number of allowed parent keys from 5 to 3.

## [6.2.5] - 2017-06-11

### Changed

- Removed dependency on CodeProject.ObjectPool, now using Microsoft.IO.RecyclableMemoryStream.
- Added FakeRandom class for unit tests.
- Embedded LibLog.
- Embedded Thrower.

## [6.2.3] - 2017-05-29

### Changed

- SQL tables no longer need foreign keys to handle parent keys.
- Added MaxParentKeyTreeDepth property to inspect how deep can be the tree generated by parent keys.
- Properly overridden all async methods inside DbCache.

## [6.2.2] - 2017-05-05

### Changed

- Removed dependency on Polly.
- Added support for Microsoft IDistributedCache interface.

## [6.2.1] - 2017-04-17

### Changed

- Updated packages.
- Random cleanup task is now started on a dedicated thread.
- Removed support for .NET 4, since its async polyfill was causing issues with new project format (VS2017).
- Added support for .NET Standard 1.3 in all projects where it was feasible.
- Changed SQLite driver to Microsoft.Data.SQLite, in order to support .NET Standard 1.3.
- Using NodaTime clock and types instead of custom interfaces.

## [6.1.0] - 2017-03-31

### Changed

- Removed integration with Microsoft caching abstractions. It will be added again as a separate package.

## [6.0.9] - 2017-03-30

### Changed

- Updated packages.
- Added missing deps in NuGet packages.

## [6.0.7] - 2017-02-26

### Changed

- Removed everything related to CodeServices.
- Imported everything required from CodeServices.

## [6.0.6] - 2017-02-05

### Changed

- Fixed missing exception logging on failed retries.
- "Add*" methods are now properly async.
- Added optimizations for certain data types (strings and byte arrays).
- Updated many dependencies.

## [6.0.5] - 2016-12-27

### Changed

- Complete library rewrite.
- Added new simple anti-tamper mechanism.
- Added new retry policy for add and clear operations.

## [5.2.6] - 2016-06-26

### Changed

- Updated System.Data.SQLite to version 1.0.102.0.
- Moved interfaces and core classes to PommaLabs.CodeServices.Caching package.

## [5.2.5] - 2016-04-23

### Changed

- Updated System.Data.SQLite to version 1.0.101.0.

## [5.2.4] - 2016-04-10

### Changed

- Updated a few dependencies.
- Minor internal rework to cache SQLite commands.

## [5.2.2] - 2016-03-29

### Changed

- Updated lots of dependencies.
- Fixed a bug in type name handling during serialization and deserialization.
- Reduced the number of parent keys for SQLite caches from 10 to 5.
- Fixed an initialization which was happening too early for WebCaches.
- Now using Deflate as the default compressor. Deflate replaces Snappy, which seemed an "unsafe" solution for the long term.

[12.4.0]: https://gitlab.com/pommalabs/kvlite/-/compare/12.3.0...12.4.0
[12.3.0]: https://gitlab.com/pommalabs/kvlite/-/compare/12.2.1...12.3.0
[12.2.1]: https://gitlab.com/pommalabs/kvlite/-/compare/12.2.0...12.2.1
[12.2.0]: https://gitlab.com/pommalabs/kvlite/-/compare/12.1.0...12.2.0
[12.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/12.0.0...12.1.0
[12.0.0]: https://gitlab.com/pommalabs/kvlite/-/compare/11.1.0...12.0.0
[11.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.7...11.1.0
[11.0.7]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.6...11.0.7
[11.0.6]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.5...11.0.6
[11.0.5]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.4...11.0.5
[11.0.4]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.3...11.0.4
[11.0.3]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.2...11.0.3
[11.0.2]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.1...11.0.2
[11.0.1]: https://gitlab.com/pommalabs/kvlite/-/compare/11.0.0...11.0.1
[11.0.0]: https://gitlab.com/pommalabs/kvlite/-/compare/10.1.2...11.0.0
[10.1.2]: https://gitlab.com/pommalabs/kvlite/-/compare/10.1.1...10.1.2
[10.1.1]: https://gitlab.com/pommalabs/kvlite/-/compare/10.1.0...10.1.1
[10.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.5...10.1.0
[10.0.5]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.4...10.0.5
[10.0.4]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.3...10.0.4
[10.0.3]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.2...10.0.3
[10.0.2]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.1...10.0.2
[10.0.1]: https://gitlab.com/pommalabs/kvlite/-/compare/10.0.0...10.0.1
[10.0.0]: https://gitlab.com/pommalabs/kvlite/-/compare/9.3.3...10.0.0
[9.3.3]: https://gitlab.com/pommalabs/kvlite/-/compare/9.3.2...9.3.3
[9.3.2]: https://gitlab.com/pommalabs/kvlite/-/compare/9.3.1...9.3.2
[9.3.1]: https://gitlab.com/pommalabs/kvlite/-/compare/9.3.0...9.3.1
[9.3.0]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.7...9.3.0
[9.2.7]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.6...9.2.7
[9.2.6]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.5...9.2.6
[9.2.5]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.4...9.2.5
[9.2.4]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.3...9.2.4
[9.2.3]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.2...9.2.3
[9.2.2]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.1...9.2.2
[9.2.1]: https://gitlab.com/pommalabs/kvlite/-/compare/9.2.0...9.2.1
[9.2.0]: https://gitlab.com/pommalabs/kvlite/-/compare/9.1.2...9.2.0
[9.1.2]: https://gitlab.com/pommalabs/kvlite/-/compare/9.1.1...9.1.2
[9.1.1]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.6...9.1.1
[9.0.6]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.5...9.0.6
[9.0.5]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.4...9.0.5
[9.0.4]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.3...9.0.4
[9.0.3]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.2...9.0.3
[9.0.2]: https://gitlab.com/pommalabs/kvlite/-/compare/9.0.1...9.0.2
[9.0.1]: https://gitlab.com/pommalabs/kvlite/-/compare/8.1.5...9.0.1
[8.1.5]: https://gitlab.com/pommalabs/kvlite/-/compare/8.1.3...8.1.5
[8.1.3]: https://gitlab.com/pommalabs/kvlite/-/compare/8.1.2...8.1.3
[8.1.2]: https://gitlab.com/pommalabs/kvlite/-/compare/8.1.1...8.1.2
[8.1.1]: https://gitlab.com/pommalabs/kvlite/-/compare/8.1.0...8.1.1
[8.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/8.0.2...8.1.0
[8.0.2]: https://gitlab.com/pommalabs/kvlite/-/compare/8.0.1...8.0.2
[8.0.1]: https://gitlab.com/pommalabs/kvlite/-/compare/7.2.3...8.0.1
[7.2.3]: https://gitlab.com/pommalabs/kvlite/-/compare/7.2.2...7.2.3
[7.2.2]: https://gitlab.com/pommalabs/kvlite/-/compare/7.2.1...7.2.2
[7.2.1]: https://gitlab.com/pommalabs/kvlite/-/compare/7.2.0...7.2.1
[7.2.0]: https://gitlab.com/pommalabs/kvlite/-/compare/7.1.2...7.2.0
[7.1.2]: https://gitlab.com/pommalabs/kvlite/-/compare/7.1.1...7.1.2
[7.1.1]: https://gitlab.com/pommalabs/kvlite/-/compare/7.1.0...7.1.1
[7.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/7.0.3...7.1.0
[7.0.3]: https://gitlab.com/pommalabs/kvlite/-/compare/7.0.2...7.0.3
[7.0.2]: https://gitlab.com/pommalabs/kvlite/-/compare/7.0.1...7.0.2
[7.0.1]: https://gitlab.com/pommalabs/kvlite/-/compare/6.4.4...7.0.1
[6.4.4]: https://gitlab.com/pommalabs/kvlite/-/compare/6.4.3...6.4.4
[6.4.3]: https://gitlab.com/pommalabs/kvlite/-/compare/6.4.2...6.4.3
[6.4.2]: https://gitlab.com/pommalabs/kvlite/-/compare/6.4.1...6.4.2
[6.4.1]: https://gitlab.com/pommalabs/kvlite/-/compare/6.4.0...6.4.1
[6.4.0]: https://gitlab.com/pommalabs/kvlite/-/compare/6.3.3...6.4.0
[6.3.3]: https://gitlab.com/pommalabs/kvlite/-/compare/6.3.2...6.3.3
[6.3.2]: https://gitlab.com/pommalabs/kvlite/-/compare/6.3.1...6.3.2
[6.3.1]: https://gitlab.com/pommalabs/kvlite/-/compare/6.3.0...6.3.1
[6.3.0]: https://gitlab.com/pommalabs/kvlite/-/compare/6.2.5...6.3.0
[6.2.5]: https://gitlab.com/pommalabs/kvlite/-/compare/6.2.3...6.2.5
[6.2.3]: https://gitlab.com/pommalabs/kvlite/-/compare/6.2.2...6.2.3
[6.2.2]: https://gitlab.com/pommalabs/kvlite/-/compare/6.2.1...6.2.2
[6.2.1]: https://gitlab.com/pommalabs/kvlite/-/compare/6.1.0...6.2.1
[6.1.0]: https://gitlab.com/pommalabs/kvlite/-/compare/6.0.9...6.1.0
[6.0.9]: https://gitlab.com/pommalabs/kvlite/-/compare/6.0.7...6.0.9
[6.0.7]: https://gitlab.com/pommalabs/kvlite/-/compare/6.0.6...6.0.7
[6.0.6]: https://gitlab.com/pommalabs/kvlite/-/compare/6.0.5...6.0.6
[6.0.5]: https://gitlab.com/pommalabs/kvlite/-/compare/5.2.6...6.0.5
[5.2.6]: https://gitlab.com/pommalabs/kvlite/-/compare/5.2.5...5.2.6
[5.2.5]: https://gitlab.com/pommalabs/kvlite/-/compare/5.2.4...5.2.5
[5.2.4]: https://gitlab.com/pommalabs/kvlite/-/compare/5.2.2...5.2.4
[5.2.2]: https://gitlab.com/pommalabs/kvlite/-/compare/4.5.0...5.2.2

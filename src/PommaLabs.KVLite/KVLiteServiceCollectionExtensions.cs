﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using PommaLabs.KVLite;
using PommaLabs.KVLite.Extensibility;

[assembly: InternalsVisibleTo("PommaLabs.KVLite.AspNetCore")]
[assembly: InternalsVisibleTo("PommaLabs.KVLite.Database")]
[assembly: InternalsVisibleTo("PommaLabs.KVLite.Memory")]
[assembly: InternalsVisibleTo("PommaLabs.KVLite.Polly")]
[assembly: InternalsVisibleTo("PommaLabs.KVLite.SQLite")]
[assembly: InternalsVisibleTo("PommaLabs.KVLite.UnitTests")]

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for core KVLite services.
/// </summary>
public static class KVLiteServiceCollectionExtensions
{
    /// <summary>
    ///   Registers specified cache factory as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <typeparam name="TCache">Cache type.</typeparam>
    /// <typeparam name="TSettings">Cache settings type.</typeparam>
    /// <param name="services">Service collection.</param>
    /// <param name="cacheFactory">The cache factory that should be registered.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteCache<TCache, TSettings>(
        this IServiceCollection services,
        Func<ISerializer, IClock, ILogger<TCache>, IInterceptor, IRandom, TCache> cacheFactory)
        where TCache : AbstractCache<TCache, TSettings>
        where TSettings : AbstractCacheSettings<TSettings>
    {
        if (services == null || cacheFactory == null)
        {
            return services;
        }

        TCache ImplementationFactory(IServiceProvider serviceProvider)
        {
            var serializer = serviceProvider.GetService<ISerializer>() ?? BinarySerializer.Instance;
            var clock = serviceProvider.GetService<IClock>() ?? SystemClock.Instance;
            var logger = serviceProvider.GetService<ILogger<TCache>>() ?? NullLogger<TCache>.Instance;
            var interceptor = serviceProvider.GetService<IInterceptor>() ?? NoOpInterceptor.Instance;
            var random = serviceProvider.GetService<IRandom>() ?? new SystemRandom();
            return cacheFactory(serializer, clock, logger, interceptor, random);
        }

        return services
            .AddOrReplaceSingleton<ICache, TCache>(serviceProvider => ImplementationFactory(serviceProvider))
            .AddOrReplaceSingleton<ICache<TSettings>, TCache>(serviceProvider => ImplementationFactory(serviceProvider))
            .AddOrReplaceSingleton<IDistributedCache, TCache>(serviceProvider => ImplementationFactory(serviceProvider));
    }

    internal static IServiceCollection AddOrReplaceSingleton<TService, TImplementation>(
        this IServiceCollection services,
        Func<IServiceProvider, TImplementation> implementationFactory)
        where TService : class
        where TImplementation : class, TService
    {
        var d = services.FirstOrDefault(x => x.ServiceType == typeof(TService));
        if (d != null)
        {
            services.Remove(d);
        }
        return services.AddSingleton<TService>(implementationFactory);
    }
}

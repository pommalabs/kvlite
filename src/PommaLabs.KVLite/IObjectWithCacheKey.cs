﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite;

/// <summary>
///   Represents an object which can generate a cache key with its own key properties.
/// </summary>
public interface IObjectWithCacheKey
{
    /// <summary>
    ///   Generates a cache key with object key properties.
    /// </summary>
    /// <returns>A cache key with object key properties.</returns>
    CacheKey GetCacheKey();
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Microsoft.Extensions.Caching.Distributed;
using PommaLabs.KVLite;
using PommaLabs.KVLite.PostgreSql;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for PostgreSQL KVLite services.
/// </summary>
public static class PostgreSqlKVLiteServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="PostgreSqlCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePostgreSqlCache(this IServiceCollection services) => services.AddKVLitePostgreSqlCache(null);

    /// <summary>
    ///   Registers <see cref="PostgreSqlCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeSettings">Can be used to customize settings.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePostgreSqlCache(this IServiceCollection services, Action<PostgreSqlCacheSettings> changeSettings)
    {
        var settings = new PostgreSqlCacheSettings();
        changeSettings?.Invoke(settings);

        return services.AddKVLiteCache<PostgreSqlCache, PostgreSqlCacheSettings>(
            (serializer, clock, logger, interceptor, random) =>
            new PostgreSqlCache(settings, serializer, clock, logger, interceptor, random));
    }
}

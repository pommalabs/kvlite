﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite;

/// <summary>
///   Options which can be used to control how cache is queried.
/// </summary>
public sealed class CacheQueryOptions
{
    private const int MaxTakeValue = 1000;

    /// <summary>
    ///   How many entries should be skipped.
    /// </summary>
    public int Skip { get; set; }

    /// <summary>
    ///   How many entries should be taken.
    /// </summary>
    public int Take { get; set; } = MaxTakeValue;

    /// <summary>
    ///   Validates given query options and returns validated options.
    /// </summary>
    /// <param name="queryOptions">Query options.</param>
    /// <returns>Validated options.</returns>
    public static CacheQueryOptions Validate(CacheQueryOptions queryOptions)
    {
        if (queryOptions == null) return new CacheQueryOptions();
        if (queryOptions.Skip < 0) queryOptions.Skip = 0;
        if (queryOptions.Take < 0) queryOptions.Take = 0;
        if (queryOptions.Take > MaxTakeValue) queryOptions.Take = MaxTakeValue;
        return queryOptions;
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using BenchmarkDotNet.Attributes;
using PommaLabs.KVLite.Benchmarks.Models;

namespace PommaLabs.KVLite.Benchmarks;

[MemoryDiagnoser]
public class GetOrAddLogMessages
{
    [Params(10, 100)]
    public int Count { get; set; }

    [GlobalSetup]
    public static void ClearCaches() => Caches.Clear();

    [Benchmark]
    public void MemoryCache() => GetOrAddLogMsg(Caches.Memory);

    [Benchmark(Baseline = true)]
    public void MySqlCache() => GetOrAddLogMsg(Caches.MySql);

    [Benchmark]
    public void PostgreSqlCache() => GetOrAddLogMsg(Caches.PostgreSql);

    [Benchmark]
    public void SqlServerCache() => GetOrAddLogMsg(Caches.SqlServer);

    private void GetOrAddLogMsg<TCache>(TCache cache)
        where TCache : ICache
    {
        var k = Guid.NewGuid().ToString();
        cache.GetOrAddSliding(k, k, () => LogMessage.GenerateRandomLogMessages(Count), TimeSpan.FromHours(1));
    }
}

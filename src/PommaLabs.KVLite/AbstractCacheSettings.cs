﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

/// <summary>
///   Base class for cache settings. Contains settings shared among different caches.
/// </summary>
[Serializable, DataContract]
public abstract partial class AbstractCacheSettings<TSettings> : ICacheSettings
    where TSettings : AbstractCacheSettings<TSettings>
{
    /// <summary>
    ///   Backing field for <see cref="CacheName"/>.
    /// </summary>
    private string _cacheName = typeof(TSettings).Name.Replace("Settings", string.Empty);

    /// <summary>
    ///   Backing field for <see cref="ChancesOfAutoCleanup"/>.
    /// </summary>
    private double _chancesOfAutoCleanup = 0.01;

    /// <summary>
    ///   Backing field for <see cref="DefaultDistributedCacheAbsoluteExpiration"/>.
    /// </summary>
    private TimeSpan _defaultDistributedCacheAbsoluteExpiration = TimeSpan.FromMinutes(20);

    /// <summary>
    ///   The cache name, can be used for logging.
    /// </summary>
    /// <value>The cache name.</value>
    [DataMember]
    public string CacheName
    {
        get
        {
            var result = _cacheName;

            // Postconditions
            Debug.Assert(!string.IsNullOrWhiteSpace(result));
            return result;
        }
        set
        {
            // Preconditions
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentException(ErrorMessages.NullOrEmptyCacheName, nameof(CacheName));
            if (!Regex.IsMatch(value, @"^[a-zA-Z0-9_\-\. ]*$", default, Constants.RegexMatchTimeout)) throw new ArgumentException(ErrorMessages.InvalidCacheName, nameof(CacheName));

            _cacheName = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   Chances of an automatic cleanup happening right after an insert operation. Defaults to 1%.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">
    ///   <paramref name="value"/> is less than zero or greater than one.
    /// </exception>
    /// <remarks>
    ///   Set this property to zero if you want automatic cleanups to never happen. Instead, if
    ///   you want automatic cleanups to happen on every insert operation, you should set this
    ///   value to one.
    /// </remarks>
    [DataMember]
    public double ChancesOfAutoCleanup
    {
        get
        {
            var result = _chancesOfAutoCleanup;

            // Postconditions
            Debug.Assert(result >= 0.0 && result <= 1.0);
            return result;
        }
        set
        {
            // Preconditions
            if (value < 0.0 || value > 1.0) throw new ArgumentOutOfRangeException(nameof(ChancesOfAutoCleanup));

            _chancesOfAutoCleanup = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    ///   Default absolute expiration for distributed cache entries. Initial value is 20 minutes.
    /// </summary>
    public TimeSpan DefaultDistributedCacheAbsoluteExpiration
    {
        get
        {
            var result = _defaultDistributedCacheAbsoluteExpiration;

            // Postconditions
            Debug.Assert(result.Ticks > 0L);
            return result;
        }
        set
        {
            // Preconditions
            if (value.Ticks <= 0L) throw new ArgumentOutOfRangeException(nameof(DefaultDistributedCacheAbsoluteExpiration));

            _defaultDistributedCacheAbsoluteExpiration = value;
            OnPropertyChanged();
        }
    }

    #region Helpers

    /// <inheritdoc/>
    public override string ToString()
    {
        var props = GetType().GetProperties();

        var sb = new StringBuilder();

        // Avoid logging connection string, in order not to expose sensitive information.
        foreach (var prop in props.Where(p => p.Name != "ConnectionString"))
        {
            sb.Append(prop.Name);
            sb.Append(": \"");
            sb.Append(prop.GetValue(this, null) ?? "(null)");
            sb.Append("\", ");
        }

        // Remove last comma and whitespace.
        sb.Remove(sb.Length - 2, 2);

        return sb.ToString();
    }

    #endregion Helpers

    #region INotifyPropertyChanged

    /// <summary>
    ///   Occurs when a property value changes.
    /// </summary>
    public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

    /// <summary>
    ///   Called when a property changed.
    /// </summary>
    /// <param name="propertyName">Name of the property.</param>
    protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion INotifyPropertyChanged
}

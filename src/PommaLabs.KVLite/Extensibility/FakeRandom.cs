﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Builds a fake random which constantly returns specified <see cref="Value"/>.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
public sealed class FakeRandom : IRandom
{
    private double _value;

    /// <summary>
    ///   Sets <see cref="Value"/> to 0.5.
    /// </summary>
    public FakeRandom()
        : this(0.5)
    {
    }

    /// <summary>
    ///   Sets <see cref="Value"/> to <paramref name="value"/>.
    /// </summary>
    /// <param name="value">The constant value.</param>
    public FakeRandom(double value)
    {
        Value = value;
    }

    /// <summary>
    ///   The value which will be returned by <see cref="NextDouble"/>.
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">
    ///   Specified value is less than 0.0 or it is greater than or equal to 1.0.
    /// </exception>
    public double Value
    {
        get { return _value; }
        set
        {
            // Preconditions
            if (value < 0.0 || value >= 1.0) throw new ArgumentOutOfRangeException(nameof(Value));

            _value = value;
        }
    }

    /// <summary>
    ///   Returns a specified floating-point number that is greater than or equal to 0.0, and
    ///   less than 1.0.
    /// </summary>
    /// <returns>
    ///   A double-precision floating point number that is greater than or equal to 0.0, and
    ///   less than 1.0.
    /// </returns>
    public double NextDouble() => _value;
}

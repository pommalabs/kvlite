﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.Goodies;

/// <summary>
///   A caching enumerable which stores huge collections in timed cache pages.
/// </summary>
public abstract class CachingEnumerable
{
    /// <summary>
    ///   Default page size of cached collection pages.
    /// </summary>
    protected const int PrivateDefaultPageSize = 100;

    /// <summary>
    ///   Partition reserved for <see cref="CachingEnumerable{T}"/>.
    /// </summary>
    protected static readonly CacheKey PartitionPrefix = "enumerables";

    /// <summary>
    ///   Default page size of cached collection pages.
    /// </summary>
    public static int DefaultPageSize => PrivateDefaultPageSize;
}

/// <summary>
///   A caching enumerable which stores huge collections in timed cache pages.
/// </summary>
/// <typeparam name="T">The type of the items stored in the caching collection.</typeparam>
public sealed class CachingEnumerable<T> : CachingEnumerable, IEnumerable<T>, IDisposable
{
    private readonly ICache _cache;
    private readonly CacheKey _cachePartition;
    private readonly int _pageCount;

    /// <summary>
    ///   Builds a caching enumerable starting from source collection.
    /// </summary>
    /// <param name="cache">The cache.</param>
    /// <param name="source">The source enumerable.</param>
    /// <param name="pageSize">The size of cache collection pages. Defaults to <see cref="CachingEnumerable.DefaultPageSize"/>.</param>
    public CachingEnumerable(ICache cache, IEnumerable<T> source, int pageSize = PrivateDefaultPageSize)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(source);
        if (pageSize <= 0) throw new ArgumentOutOfRangeException(nameof(pageSize));

        _cache = cache ?? throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);

        // Fill cache starting from given enumerable.
        var result = FillCacheFromEnumerable(cache, source, pageSize);

        _cachePartition = result.CachePartition;
        _pageCount = result.PageCount;
        Count = result.ItemCount;
    }

    /// <summary>
    ///   The number of items stored inside the caching enumerable.
    /// </summary>
    public int Count { get; }

    /// <summary>
    ///   Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>An enumerator that can be used to iterate through the collection.</returns>
    public IEnumerator<T> GetEnumerator()
    {
        for (var i = 0; i < _pageCount; ++i)
        {
            var page = _cache.Get<T[]>(_cachePartition, new CacheKey(i)).Value;

            foreach (var item in page)
            {
                yield return item;
            }
        }
    }

    /// <summary>
    ///   Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>
    ///   An <see cref="IEnumerator"/> object that can be used to iterate through the collection.
    /// </returns>
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    #region Private methods

    private static ImportResult FillCacheFromEnumerable(ICache cache, IEnumerable<T> source, int pageSize)
    {
        var cachePartition = new CacheKey(PartitionPrefix, Guid.NewGuid());

        var page = new T[pageSize];
        var pageNumber = 0;
        var pageIndex = 0;
        var itemCount = 0;

        foreach (var item in source)
        {
            itemCount++;
            page[pageIndex++] = item;

            if (pageIndex == pageSize)
            {
                pageIndex = 0;
                cache.AddTimed(cachePartition, new CacheKey(pageNumber++), page, TimeSpan.FromDays(1));
            }
        }

        if (pageIndex != 0)
        {
            Array.Resize(ref page, pageIndex);
            cache.AddTimed(cachePartition, new CacheKey(pageNumber++), page, TimeSpan.FromDays(1));
        }

        return new ImportResult
        {
            CachePartition = cachePartition,
            ItemCount = itemCount,
            PageCount = pageNumber
        };
    }

    private sealed class ImportResult
    {
        public CacheKey CachePartition { get; set; }

        public int ItemCount { get; set; }

        public int PageCount { get; set; }
    }

    #endregion Private methods

    #region IDisposable support

    private bool _disposed; // To detect redundant calls

    /// <summary>
    ///   Performs application-defined tasks associated with freeing, releasing, or resetting
    ///   unmanaged resources.
    /// </summary>
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
    }

    private void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _cache.Clear(_cachePartition);
            }
            _disposed = true;
        }
    }

    #endregion IDisposable support
}

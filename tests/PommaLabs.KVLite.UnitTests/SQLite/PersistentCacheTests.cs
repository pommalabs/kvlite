﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Ninject;
using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Resources;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.SQLite;

[TestFixture]
[Category(nameof(SQLite))]
[NonParallelizable]
internal sealed class PersistentCacheTests : AbstractTests
{
    private const string BlankPath = "   ";

    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        PersistentCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance

    #region Cache creation and disposal

    [Test]
    public void Dispose_ObjectDisposedExceptionAfterDispose()
    {
        var cache = new PersistentCache(new PersistentCacheSettings());
        cache.Dispose();
        Assert.Throws<ObjectDisposedException>(() => { cache.Count(); });
    }

    [Test]
    public void NewCache_BlankPath()
    {
        try
        {
            using var cache = new PersistentCache(new PersistentCacheSettings { CacheFile = BlankPath }, clock: Kernel.Get<IClock>());
        }
        catch (Exception ex)
        {
            Assert.That(ex, Is.InstanceOf<ArgumentException>());
            Assert.That(ex.Message, Does.Contain(ErrorMessages.NullOrEmptyCacheFile));
        }
    }

    [Test]
    public void NewCache_EmptyPath()
    {
        try
        {
            using var cache = new PersistentCache(new PersistentCacheSettings { CacheFile = string.Empty }, clock: Kernel.Get<IClock>());
        }
        catch (Exception ex)
        {
            Assert.That(ex, Is.InstanceOf<ArgumentException>());
            Assert.That(ex.Message, Does.Contain(ErrorMessages.NullOrEmptyCacheFile));
        }
    }

    [Test]
    public void NewCache_NullPath()
    {
        try
        {
            using var cache = new PersistentCache(new PersistentCacheSettings { CacheFile = null }, clock: Kernel.Get<IClock>());
        }
        catch (Exception ex)
        {
            Assert.That(ex, Is.InstanceOf<ArgumentException>());
            Assert.That(ex.Message, Does.Contain(ErrorMessages.NullOrEmptyCacheFile));
        }
    }

    #endregion Cache creation and disposal

    #region SQLite-specific Clean

    [Test]
    public void Clean_InvalidValues()
    {
        var cache = Kernel.Get<PersistentCache>();

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear();
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear(CacheReadMode.ConsiderExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow.Subtract(TenMinutes));
        }

        cache.Clear(CacheReadMode.IgnoreExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));
    }

    [Test]
    public void Clean_ValidValues()
    {
        var cache = Kernel.Get<PersistentCache>();

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow + TenMinutes);
        }

        cache.Clear();
        Assert.That(cache.Count(), Is.EqualTo(0));

        for (var i = 0; i < CacheKeys.Count; ++i)
        {
            cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], cache.Clock.UtcNow + TenMinutes);
        }

        cache.Clear(CacheReadMode.ConsiderExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(StringValues.Count));

        cache.Clear(CacheReadMode.IgnoreExpiryDate);
        Assert.That(cache.Count(), Is.EqualTo(0));
    }

    #endregion SQLite-specific Clean
}

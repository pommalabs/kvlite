﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Threading.Tasks;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SqlServer;

namespace PommaLabs.KVLite.Benchmarks;

public static class Caches
{
    public static MemoryCache Memory { get; } = MemoryCache.DefaultInstance;

    public static MySqlCache MySql { get; } = MySqlCache.DefaultInstance;

    public static PostgreSqlCache PostgreSql { get; } = PostgreSqlCache.DefaultInstance;

    public static SqlServerCache SqlServer { get; } = SqlServerCache.DefaultInstance;

    public static void Clear()
    {
        Memory.Clear();
        MySql.Clear();
        PostgreSql.Clear();
        SqlServer.Clear();
    }

    public static async Task ClearAsync()
    {
        await Memory.ClearAsync();
        await MySql.ClearAsync();
        await PostgreSql.ClearAsync();
        await SqlServer.ClearAsync();
    }
}

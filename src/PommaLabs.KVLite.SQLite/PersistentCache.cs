﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data.SQLite;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Database;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   An SQLite-based persistent cache.
/// </summary>
public sealed class PersistentCache : DbCache<PersistentCache, PersistentCacheSettings, SQLiteCacheConnectionFactory<PersistentCacheSettings>, SQLiteConnection, SQLiteTransaction, SQLiteCommand>
{
    #region Default instance

    /// <summary>
    ///   Gets the default instance for this cache kind. Default instance is configured using
    ///   default cache settings.
    /// </summary>
    public static PersistentCache DefaultInstance { get; } = new(new PersistentCacheSettings());

    #endregion Default instance

    /// <summary>
    ///   Initializes a new instance of the <see cref="PersistentCache"/> class with given settings.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public PersistentCache(
        PersistentCacheSettings settings, ISerializer serializer = null, IClock clock = null,
        ILogger<PersistentCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : base(settings, new PersistentCacheConnectionFactory(settings, logger), serializer, clock, logger, interceptor, random)
    {
    }

    /// <summary>
    ///   Runs VACUUM on the underlying SQLite database.
    /// </summary>
    public void Vacuum()
    {
        try
        {
            Logger.LogInformation("Vacuuming SQLite DB '{CacheFile}'", Settings.CacheFile);
            ConnectionFactory.Vacuum();
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnVacuum);
        }
    }
}

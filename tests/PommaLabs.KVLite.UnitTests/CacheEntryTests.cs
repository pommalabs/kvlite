﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

[TestFixture, Parallelizable, Category(nameof(Core))]
internal sealed class CacheEntryTests
{
    [Test]
    public void Equals_DifferentObjectsWithDifferentPartitionAndKey_ReturnsFalse()
    {
        // Arrange
        var cacheEntry = new CacheEntry<string> { Partition = "P", Key = "K" };
        var cacheEntry2 = new CacheEntry<string> { Partition = "P2", Key = "K2" };

        // Act & Assert
        cacheEntry.Equals(cacheEntry2).ShouldBeFalse();
        cacheEntry.Equals(cacheEntry2 as ICacheEntry<string>).ShouldBeFalse();
        cacheEntry.Equals(cacheEntry2 as object).ShouldBeFalse();
    }

    [Test]
    public void Equals_DifferentObjectsWithSamePartitionAndKey_ReturnsTrue()
    {
        // Arrange
        var cacheEntry = new CacheEntry<string> { Partition = "P", Key = "K" };
        var cacheEntry2 = new CacheEntry<string> { Partition = "P", Key = "K" };

        // Act & Assert
        cacheEntry.Equals(cacheEntry2).ShouldBeTrue();
        cacheEntry.Equals(cacheEntry2 as ICacheEntry<string>).ShouldBeTrue();
        cacheEntry.Equals(cacheEntry2 as object).ShouldBeTrue();
    }

    [Test]
    public void Equals_NullObject_ReturnsFalse()
    {
        // Arrange
        var cacheEntry = new CacheEntry<string> { Partition = "P", Key = "K" };
        CacheEntry<string> cacheEntry2 = null;

        // Act & Assert
        cacheEntry.Equals(cacheEntry2).ShouldBeFalse();
        cacheEntry.Equals(cacheEntry2 as ICacheEntry<string>).ShouldBeFalse();
        cacheEntry.Equals(cacheEntry2 as object).ShouldBeFalse();
    }

    [Test]
    public void Equals_SameObject_ReturnsTrue()
    {
        // Arrange
        var cacheEntry = new CacheEntry<string> { Partition = "P", Key = "K" };

        // Act & Assert
        cacheEntry.Equals(cacheEntry).ShouldBeTrue();
        cacheEntry.Equals(cacheEntry as ICacheEntry<string>).ShouldBeTrue();
        cacheEntry.Equals(cacheEntry as object).ShouldBeTrue();
    }

    [Test]
    public void ToString_ValidObject_ContainsPartitionAndKey()
    {
        // Arrange
        var cacheEntry = new CacheEntry<string> { Partition = "P123", Key = "K123" };

        // Act & Assert
        cacheEntry.ToString().ShouldContain(cacheEntry.Partition);
        cacheEntry.ToString().ShouldContain(cacheEntry.Key);
    }
}

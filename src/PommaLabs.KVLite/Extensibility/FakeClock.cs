﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Fake clock implementation which can be used for unit testing.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
public sealed class FakeClock : IClock
{
    /// <summary>
    ///   Creates a fake clock with given initial UTC time.
    /// </summary>
    /// <param name="initialUtcNow">Initial UTC time.</param>
    public FakeClock(DateTimeOffset initialUtcNow)
    {
        UtcNow = initialUtcNow;
    }

    /// <summary>
    ///   Retrieves the current system time in UTC.
    /// </summary>
    public DateTimeOffset UtcNow { get; private set; }

    /// <summary>
    ///   Advances the clock by the given time span.
    /// </summary>
    /// <param name="timeSpan">The time span.</param>
    /// <returns>Advanced clock.</returns>
    public DateTimeOffset Advance(TimeSpan timeSpan)
    {
        return UtcNow = UtcNow.Add(timeSpan);
    }
}

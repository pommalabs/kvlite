﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Runtime.CompilerServices;
using PommaLabs.KVLite;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Extensibility;

[assembly: InternalsVisibleTo("PommaLabs.KVLite.UnitTests")]

namespace Microsoft.AspNetCore.Http;

/// <summary>
///   Extension methods for <see cref="ISession"/> interface.
/// </summary>
public static class KVLiteSessionExtensions
{
    /// <summary>
    ///   Retrieves the value of the given key, if present. Value is deserialized using
    ///   <see cref="BinarySerializer"/> default instance.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    /// <param name="session">The session.</param>
    /// <param name="key">The key.</param>
    /// <returns>An optional cache result which contains result value, if any.</returns>
    public static CacheResult<T> GetObject<T>(this ISession session, string key) => GetObject<T>(session, BinarySerializer.Instance, key);

    /// <summary>
    ///   Retrieves the value of the given key, if present. Value is deserialized using given
    ///   custom serializer.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    /// <param name="session">The session.</param>
    /// <param name="serializer">The custom serializer.</param>
    /// <param name="key">The key.</param>
    /// <returns>An optional cache result which contains result value, if any.</returns>
    public static CacheResult<T> GetObject<T>(this ISession session, ISerializer serializer, string key)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(session);
        ArgumentNullException.ThrowIfNull(serializer);

        if (!session.TryGetValue(key, out var serializedBytes))
        {
            return default;
        }

        var compressedIndex = serializedBytes.Length - 1;
        using var serializedStream = new MemoryStream(serializedBytes, 0, compressedIndex);
        var compressed = serializedBytes[compressedIndex] == 1;
        return serializer.Deserialize<T>(serializedStream, compressed);
    }

    /// <summary>
    ///   Sets the given key and value in the current session. Value is serialized using
    ///   <see cref="BinarySerializer"/> default instance.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    /// <param name="session">The session.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    public static void SetObject<T>(this ISession session, string key, T value) => SetObject(session, BinarySerializer.Instance, key, value);

    /// <summary>
    ///   Sets the given key and value in the current session. Value is serialized using given
    ///   custom serializer.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    /// <param name="session">The session.</param>
    /// <param name="serializer">The custom serializer.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    public static void SetObject<T>(this ISession session, ISerializer serializer, string key, T value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(session);
        ArgumentNullException.ThrowIfNull(serializer);

        using var serializedStream = MemoryStreamManager.GetMemoryStream();
        var compressed = serializer.Serialize(serializedStream, value);
        serializedStream.WriteByte((byte)(compressed ? 1 : 0));
        session.Set(key, serializedStream.ToArray());
    }
}

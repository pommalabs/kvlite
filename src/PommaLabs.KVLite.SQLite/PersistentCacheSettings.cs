﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Settings used by <see cref="PersistentCache"/>.
/// </summary>
[Serializable, DataContract]
public sealed class PersistentCacheSettings : SQLiteCacheSettings<PersistentCacheSettings>
{
    /// <summary>
    ///   Backing field for <see cref="CacheFile"/>.
    /// </summary>
    private string _cacheFile = "PersistentCache.sqlite";

    /// <summary>
    /// <para>  Path to the SQLite DB used as backend for the cache.</para>
    /// <para>  Default value is "PersistentCache.sqlite".</para>
    /// </summary>
    [DataMember]
    public string CacheFile
    {
        get
        {
            var result = _cacheFile;

            // Postconditions
            Debug.Assert(!string.IsNullOrWhiteSpace(result));
            return result;
        }
        set
        {
            // Preconditions
            if (string.IsNullOrWhiteSpace(value)) throw new ArgumentException(ErrorMessages.NullOrEmptyCacheFile, nameof(CacheFile));

            _cacheFile = value;
            OnPropertyChanged();
        }
    }
}

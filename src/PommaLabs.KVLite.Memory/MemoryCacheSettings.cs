﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace PommaLabs.KVLite.Memory;

/// <summary>
///   Settings used by <see cref="MemoryCache"/>.
/// </summary>
[Serializable, DataContract]
public sealed class MemoryCacheSettings : AbstractCacheSettings<MemoryCacheSettings>
{
    /// <summary>
    ///   Backing field for <see cref="MaxCacheSizeInMB"/>.
    /// </summary>
    private int _maxCacheSizeInMB = 256;

    /// <summary>
    ///   Max size in megabytes for the cache.
    /// </summary>
    [DataMember]
    public int MaxCacheSizeInMB
    {
        get
        {
            var result = _maxCacheSizeInMB;

            // Postconditions
            Debug.Assert(result > 0);
            return result;
        }
        set
        {
            // Preconditions
            if (value <= 0) throw new ArgumentOutOfRangeException(nameof(MaxCacheSizeInMB));

            _maxCacheSizeInMB = value;
            OnPropertyChanged();
        }
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using System.Globalization;

namespace PommaLabs.KVLite.UnitTests.Infrastructure;

/// <summary>
///   Generates random data tables, starting from a given set of columns.
/// </summary>
public sealed class RandomDataTableGenerator
{
    private readonly string[] _columnNames;
    private readonly Random _random = new();

    /// <summary>
    ///   Initializes the generator with a given set of columns.
    /// </summary>
    /// <param name="columnNames">The columns that the data tables will have.</param>
    public RandomDataTableGenerator(params string[] columnNames)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(columnNames);
        if (columnNames.Any(string.IsNullOrWhiteSpace)) throw new ArgumentException("Each column name cannot be null, empty or blank", nameof(columnNames));

        _columnNames = columnNames.Clone() as string[];
    }

    /// <summary>
    ///   Generates a new random data table, with given row count.
    /// </summary>
    /// <param name="rowCount">The number of rows the data table will have.</param>
    /// <returns>A new random data table, with given row count.</returns>
    public DataTable GenerateDataTable(int rowCount)
    {
        // Preconditions
        if (rowCount < 0) throw new ArgumentOutOfRangeException(nameof(rowCount));

        var dt = new DataTable("RANDOMLY_GENERATED_DATA_TABLE_" + _random.Next());
        foreach (var columnName in _columnNames)
        {
            dt.Columns.Add(columnName);
        }
        for (var i = 0; i < rowCount; ++i)
        {
            var row = new object[_columnNames.Length];
            for (var j = 0; j < row.Length; ++j)
            {
                row[j] = _random.Next(100, 999).ToString(CultureInfo.InvariantCulture);
            }
            dt.Rows.Add(row);
        }
        return dt;
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using PommaLabs.KVLite.Core;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

/// <summary>
///   Abstract class which should make it easier to implement a new kind of cache.
/// </summary>
public abstract partial class AbstractCache
{
    /// <summary>
    ///   Starts an activity related to cache cleanup.
    /// </summary>
    /// <param name="partition">Partition.</param>
    /// <returns>An activity.</returns>
    protected static Activity StartCacheCleanupActivity(CacheKey? partition)
    {
        var activity = KVLiteActivitySource.Instance.StartActivity(ActivityNames.CacheCleanup);
        if (activity != null && partition.HasValue)
        {
            activity.SetTag("cache.partition", partition.Value.Value);
        }
        return activity;
    }

    /// <summary>
    ///   Starts an activity related to cache entry insertion.
    /// </summary>
    /// <param name="partition">Partition.</param>
    /// <param name="key">Key.</param>
    /// <returns>An activity.</returns>
    protected static Activity StartCacheEntryInsertionActivity(CacheKey partition, CacheKey key)
    {
        return StartCacheEntryActivity(ActivityNames.CacheEntryInsertion, partition, key);
    }

    /// <summary>
    ///   Starts an activity related to cache entry retrieval.
    /// </summary>
    /// <param name="partition">Partition.</param>
    /// <param name="key">Key.</param>
    /// <returns>An activity.</returns>
    protected static Activity StartCacheEntryRetrievalActivity(CacheKey partition, CacheKey key)
    {
        return StartCacheEntryActivity(ActivityNames.CacheEntryRetrieval, partition, key);
    }

    private static Activity StartCacheEntryActivity(string activityName, CacheKey partition, CacheKey key)
    {
        var activity = KVLiteActivitySource.Instance.StartActivity(activityName);
        if (activity != null)
        {
            activity.SetTag("cache.partition", partition.Value);
            activity.SetTag("cache.key", key.Value);
        }
        return activity;
    }
}

/// <summary>
///   Abstract class which should make it easier to implement a new kind of cache.
/// </summary>
/// <typeparam name="TCache">The type of the cache.</typeparam>
/// <typeparam name="TSettings">The type of the cache settings.</typeparam>
public abstract partial class AbstractCache<TCache, TSettings> : ICache<TSettings>, ICacheInternal
    where TCache : AbstractCache<TCache, TSettings>
    where TSettings : AbstractCacheSettings<TSettings>
{
    #region Construction and services

    /// <summary>
    ///   Initializes a new instance of this class with given settings and services.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    protected AbstractCache(
        TSettings settings, ISerializer serializer, IClock clock,
        ILogger<TCache> logger, IInterceptor interceptor)
    {
        Settings = settings ?? throw new ArgumentNullException(nameof(settings), ErrorMessages.NullSettings);
        Serializer = serializer ?? BinarySerializer.Instance;
        Clock = clock ?? SystemClock.Instance;
        Logger = logger ?? NullLogger<TCache>.Instance;
        Interceptor = interceptor ?? NoOpInterceptor.Instance;

        Logger.LogInformation("Initializing cache with following settings: {CacheSettings}", Settings);
    }

    /// <inheritdoc/>
    public IClock Clock { get; }

    /// <inheritdoc/>
    public IInterceptor Interceptor { get; }

    /// <inheritdoc/>
    public ISerializer Serializer { get; }

    /// <inheritdoc/>
    public TSettings Settings { get; }

    #endregion Construction and services

    #region Abstract and virtual members

    /// <inheritdoc/>
    public abstract bool CanPeek { get; }

    /// <inheritdoc/>
    public abstract int MaxParentKeyCountPerEntry { get; }

    /// <inheritdoc/>
    public abstract int MaxParentKeyTreeDepth { get; }

    /// <summary>
    ///   Adds given value with the specified expiry time and refresh internal.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="utcExpiry">The UTC expiry time.</param>
    /// <param name="interval">The refresh interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    protected abstract void AddInternal<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, TimeSpan interval, IList<CacheKey> parentKeys);

    /// <summary>
    ///   Clears this instance or a partition, if specified.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <returns>The number of entries that have been removed.</returns>
    protected abstract long ClearInternal(CacheKey? partition, CacheReadMode cacheReadMode = CacheReadMode.IgnoreExpiryDate);

    /// <summary>
    ///   Determines whether cache contains the specified partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>Whether cache contains the specified partition and key.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    protected abstract bool ContainsInternal(CacheKey partition, CacheKey key);

    /// <summary>
    ///   The number of entries in the cache or in a partition, if specified.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <returns>The number of entries in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    protected abstract long CountInternal(CacheKey? partition, CacheReadMode cacheReadMode = CacheReadMode.ConsiderExpiryDate);

    /// <summary>
    ///   Computes cache size in bytes. This value might be an estimate of real cache size and,
    ///   therefore, it does not need to be extremely accurate.
    /// </summary>
    /// <returns>An estimate of cache size in bytes.</returns>
    protected abstract long GetCacheSizeInBytesInternal();

    /// <summary>
    ///   Gets all cache entries or the ones in a partition, if specified. If an entry is a
    ///   "sliding" value, its lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries.</returns>
    protected abstract IList<ICacheEntry<TVal>> GetEntriesInternal<TVal>(CacheKey? partition, CacheQueryOptions queryOptions);

    /// <summary>
    ///   Gets the cache entry with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>The cache entry with specified partition and key.</returns>
    protected abstract CacheResult<ICacheEntry<TVal>> GetEntryInternal<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Gets the value with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by the corresponding interval.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>The value with specified partition and key.</returns>
    protected abstract CacheResult<TVal> GetInternal<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Gets the all values in the cache or in the specified partition, without updating
    ///   expiry dates.
    /// </summary>
    /// <param name="partition">The optional partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All values, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected abstract IList<ICacheEntry<TVal>> PeekEntriesInternal<TVal>(CacheKey? partition, CacheQueryOptions queryOptions);

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected abstract CacheResult<ICacheEntry<TVal>> PeekEntryInternal<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    protected abstract CacheResult<TVal> PeekInternal<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Removes the value with given partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    protected abstract void RemoveInternal(CacheKey partition, CacheKey key);

    #endregion Abstract and virtual members

    #region IDisposable members

    /// <summary>
    ///   Whether this cache has been disposed or not. When a cache has been disposed, no more
    ///   operations are allowed on it.
    /// </summary>
    public bool Disposed { get; private set; }

    /// <inheritdoc/>
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    ///   Performs application-defined tasks associated with freeing, releasing, or resetting
    ///   unmanaged resources.
    /// </summary>
    /// <param name="disposing">True if it is a managed dispose, false otherwise.</param>
    protected virtual void Dispose(bool disposing)
    {
        Disposed = true;
    }

    #endregion IDisposable members

    #region ICache members

    /// <inheritdoc/>
    public Exception LastError { get; set; }

    /// <inheritdoc/>
    ICacheSettings ICache.Settings => Settings;

    /// <inheritdoc/>
    public CacheResult<object> this[CacheKey partition, CacheKey key]
    {
        get
        {
            // Preconditions
            if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

            using (StartCacheEntryRetrievalActivity(partition, key))
            {
                try
                {
                    var result = GetInternal<object>(partition, key);

                    // Postconditions
                    Debug.Assert(Contains(partition, key) == result.HasValue);
                    return result;
                }
                catch (Exception ex)
                {
                    LastError = ex;
                    Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                    return default;
                }
            }
        }
    }

    /// <inheritdoc/>
    public void AddSliding<TVal>(CacheKey partition, CacheKey key, TVal value, TimeSpan interval, IList<CacheKey> parentKeys = default)
    {
        Add(partition, key, value, Clock.UtcNow + interval, interval, parentKeys);
    }

    /// <inheritdoc/>
    public void AddTimed<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, IList<CacheKey> parentKeys = default)
    {
        Add(partition, key, value, utcExpiry, TimeSpan.Zero, parentKeys);
    }

    /// <inheritdoc/>
    public long Clear()
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheCleanupActivity(default))
        {
            try
            {
                var result = ClearInternal(default);

                // Postconditions
                Debug.Assert(result >= 0L);
                Debug.Assert(Count() == 0);
                Debug.Assert(LongCount() == 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearAll, Settings.CacheName);
                return 0L;
            }
        }
    }

    /// <inheritdoc/>
    public long Clear(CacheKey partition)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheCleanupActivity(partition))
        {
            try
            {
                var result = ClearInternal(partition);

                // Postconditions
                Debug.Assert(result >= 0L);
                Debug.Assert(Count(partition) == 0);
                Debug.Assert(LongCount(partition) == 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearPartition, Settings.CacheName, partition);
                return 0L;
            }
        }
    }

    /// <summary>
    ///   Clears the cache using the specified cache read mode.
    /// </summary>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <returns>The number of entries that have been removed.</returns>
    public long Clear(CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        using (StartCacheCleanupActivity(default))
        {
            try
            {
                var result = ClearInternal(default, cacheReadMode);

                // Postconditions
                Debug.Assert(result >= 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearAll, Settings.CacheName);
                return 0L;
            }
        }
    }

    /// <summary>
    ///   Clears the specified partition using the specified cache read mode.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cacheReadMode">The cache read mode.</param>
    /// <returns>The number of entries that have been removed.</returns>
    public long Clear(CacheKey partition, CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        using (StartCacheCleanupActivity(partition))
        {
            try
            {
                var result = ClearInternal(partition, cacheReadMode);

                // Postconditions
                Debug.Assert(result >= 0L);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnClearPartition, Settings.CacheName, partition);
                return 0L;
            }
        }
    }

    /// <inheritdoc/>
    public bool Contains(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            return ContainsInternal(partition, key);
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
            return false;
        }
    }

    /// <inheritdoc/>
    public int Count()
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = Convert.ToInt32(CountInternal(default));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0;
        }
    }

    /// <inheritdoc/>
    public int Count(CacheKey partition)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = Convert.ToInt32(CountInternal(partition));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0;
        }
    }

    /// <summary>
    ///   The number of entries in the cache.
    /// </summary>
    /// <param name="cacheReadMode">Whether invalid entries should be included in the count.</param>
    /// <returns>The number of entries in the cache.</returns>
    public int Count(CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        try
        {
            var result = Convert.ToInt32(CountInternal(default, cacheReadMode));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0;
        }
    }

    /// <summary>
    ///   The number of entries in the cache for given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cacheReadMode">Whether invalid entries should be included in the count.</param>
    /// <returns>The number of entries in the cache.</returns>
    public int Count(CacheKey partition, CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        try
        {
            var result = Convert.ToInt32(CountInternal(partition, cacheReadMode));

            // Postconditions
            Debug.Assert(result >= 0);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0;
        }
    }

    /// <inheritdoc/>
    public CacheResult<TVal> Get<TVal>(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                var result = GetInternal<TVal>(partition, key);

                if (Logger.IsEnabled(LogLevel.Trace))
                {
                    Logger.LogTrace(TraceMessages.GetEntry, partition, key, Settings.CacheName, stopwatch?.ElapsedMilliseconds);
                }

                // Postconditions
                Debug.Assert(Contains(partition, key) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public long GetCacheSizeInBytes()
    {
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = GetCacheSizeInBytesInternal();

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public IList<ICacheEntry<TVal>> GetEntries<TVal>(CacheQueryOptions queryOptions = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = GetEntriesInternal<TVal>(default, queryOptions);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= Count());
            Debug.Assert(result.Count <= LongCount());
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public IList<ICacheEntry<TVal>> GetEntries<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = GetEntriesInternal<TVal>(partition, queryOptions);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= Count(partition));
            Debug.Assert(result.Count <= LongCount(partition));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadPartition, Settings.CacheName, partition);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public CacheResult<ICacheEntry<TVal>> GetEntry<TVal>(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                var result = GetEntryInternal<TVal>(partition, key);

                if (Logger.IsEnabled(LogLevel.Trace))
                {
                    Logger.LogTrace(TraceMessages.GetEntry, partition, key, Settings.CacheName, stopwatch?.ElapsedMilliseconds);
                }

                // Postconditions
                Debug.Assert(Contains(partition, key) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public TVal GetOrAddSliding<TVal>(
        CacheKey partition, CacheKey key, Func<TVal> valueGetter, TimeSpan interval,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        if (!TryGet(partition, key, valueGetter, parentKeys, valueFilterer, out var value))
        {
            AddSliding(partition, key, value, interval, parentKeys);
        }
        return value;
    }

    /// <inheritdoc/>
    public TVal GetOrAddTimed<TVal>(
        CacheKey partition, CacheKey key, Func<TVal> valueGetter, DateTimeOffset utcExpiry,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default)
    {
        if (!TryGet(partition, key, valueGetter, parentKeys, valueFilterer, out var value))
        {
            AddTimed(partition, key, value, utcExpiry, parentKeys);
        }
        return value;
    }

    /// <inheritdoc/>
    public long LongCount()
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = CountInternal(default);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public long LongCount(CacheKey partition)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            var result = CountInternal(partition);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0L;
        }
    }

    /// <summary>
    ///   The number of entries in the cache.
    /// </summary>
    /// <param name="cacheReadMode">Whether invalid entries should be included in the count.</param>
    /// <returns>The number of entries in the cache.</returns>
    public long LongCount(CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        try
        {
            var result = CountInternal(default, cacheReadMode);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountAll, Settings.CacheName);
            return 0L;
        }
    }

    /// <summary>
    ///   The number of entries in the cache for given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cacheReadMode">Whether invalid entries should be included in the count.</param>
    /// <returns>The number of entries in the cache.</returns>
    public long LongCount(CacheKey partition, CacheReadMode cacheReadMode)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!Enum.IsDefined(cacheReadMode)) throw new ArgumentException(ErrorMessages.InvalidCacheReadMode, nameof(cacheReadMode));

        try
        {
            var result = CountInternal(partition, cacheReadMode);

            // Postconditions
            Debug.Assert(result >= 0L);
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnCountPartition, Settings.CacheName, partition);
            return 0L;
        }
    }

    /// <inheritdoc/>
    public CacheResult<TVal> Peek<TVal>(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var result = PeekInternal<TVal>(partition, key);

                // Postconditions
                Debug.Assert(Contains(partition, key) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public IList<ICacheEntry<TVal>> PeekEntries<TVal>(CacheQueryOptions queryOptions = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = PeekEntriesInternal<TVal>(default, queryOptions);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= Count());
            Debug.Assert(result.Count <= LongCount());
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadAll, Settings.CacheName);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public IList<ICacheEntry<TVal>> PeekEntries<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        try
        {
            queryOptions = CacheQueryOptions.Validate(queryOptions);
            var result = PeekEntriesInternal<TVal>(partition, queryOptions);

            // Postconditions
            Debug.Assert(result != null);
            Debug.Assert(result.Count <= queryOptions.Take);
            Debug.Assert(result.Count <= Count(partition));
            Debug.Assert(result.Count <= LongCount(partition));
            return result;
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnReadPartition, Settings.CacheName, partition);
            return Array.Empty<ICacheEntry<TVal>>();
        }
    }

    /// <inheritdoc/>
    public CacheResult<ICacheEntry<TVal>> PeekEntry<TVal>(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (!CanPeek) throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheDoesNotAllowPeeking, Settings.CacheName));

        using (StartCacheEntryRetrievalActivity(partition, key))
        {
            try
            {
                var result = PeekEntryInternal<TVal>(partition, key);

                // Postconditions
                Debug.Assert(Contains(partition, key) == result.HasValue);
                return result;
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnRead, partition, key, Settings.CacheName);
                return default;
            }
        }
    }

    /// <inheritdoc/>
    public void Remove(CacheKey partition, CacheKey key)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));

        try
        {
            RemoveInternal(partition, key);

            // Postconditions
            Debug.Assert(!Contains(partition, key));
        }
        catch (Exception ex)
        {
            LastError = ex;
            Logger.LogError(ex, ErrorMessages.InternalErrorOnWrite, partition, key, Settings.CacheName);
        }
    }

    #endregion ICache members

    #region Helpers

    /// <summary>
    ///   Gets the log used by the cache.
    /// </summary>
    protected ILogger<TCache> Logger { get; }

    private void Add<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, TimeSpan interval, IList<CacheKey> parentKeys)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (parentKeys != null && parentKeys.Count > MaxParentKeyCountPerEntry) throw new NotSupportedException(ErrorMessages.TooManyParentKeys);

        using (StartCacheEntryInsertionActivity(partition, key))
        {
            try
            {
                var stopwatch = Logger.IsEnabled(LogLevel.Trace) ? Stopwatch.StartNew() : default;
                AddInternal(partition, key, value, utcExpiry, interval, parentKeys);
                TraceNewEntry<TVal>(partition, key, utcExpiry, interval, stopwatch);
            }
            catch (Exception ex)
            {
                LastError = ex;
                Logger.LogError(ex, ErrorMessages.InternalErrorOnWrite, partition, key, Settings.CacheName);
            }
        }
    }

    private void TraceNewEntry<TVal>(CacheKey partition, CacheKey key, DateTimeOffset utcExpiry, TimeSpan interval, Stopwatch stopwatch)
    {
        if (Logger.IsEnabled(LogLevel.Trace))
        {
            Logger.LogTrace(TraceMessages.AddEntry, partition, key, Settings.CacheName, utcExpiry, interval, stopwatch?.ElapsedMilliseconds);
        }

        // Postconditions
        Debug.Assert(!Contains(partition, key) || !CanPeek || PeekEntry<TVal>(partition, key).Value.Interval == interval);
    }

    private bool TryGet<TVal>(
        CacheKey partition, CacheKey key, Func<TVal> valueGetter,
        IList<CacheKey> parentKeys, Predicate<TVal> valueFilterer, out TVal value)
    {
        // Preconditions
        if (Disposed) throw new ObjectDisposedException(Settings.CacheName, string.Format(CultureInfo.InvariantCulture, ErrorMessages.CacheHasBeenDisposed, Settings.CacheName));
        if (valueGetter == null) throw new ArgumentNullException(nameof(valueGetter), ErrorMessages.NullValueGetter);
        if (parentKeys != null && parentKeys.Count > MaxParentKeyCountPerEntry) throw new NotSupportedException(ErrorMessages.TooManyParentKeys);

        var originalPartition = partition;
        var originalKey = key;
        Interceptor.EntryGetting(ref partition, ref key);

        var cacheResult = Get<TVal>(partition, key);
        if (cacheResult.HasValue)
        {
            value = cacheResult.Value;
            return true;
        }

        // This line is reached when the cache does not contain the entry or an error has occurred.
        value = valueGetter();

        // If value filterer has been specified, then it should be invoked in order to
        // understand whether retrieved value will be filtered.
        if (valueFilterer?.Invoke(value) ?? false)
        {
            return true;
        }

        partition = originalPartition;
        key = originalKey;
        Interceptor.EntryAdding(ref partition, ref key);

        return false;
    }

    #endregion Helpers
}

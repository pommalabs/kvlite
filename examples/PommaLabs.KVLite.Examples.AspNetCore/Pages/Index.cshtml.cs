﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.Examples.AspNetCore.Pages;

public sealed class IndexModel : PageModel
{
    private readonly IClock _clock;

    public IndexModel(IClock clock)
    {
        _clock = clock;
    }

    public DateTimeOffset FirstVisit { get; set; }

    public void OnGet()
    {
        var firstVisitSessionKey = new CacheKey(nameof(FirstVisit));
        var firstVisit = HttpContext.Session.GetObject<DateTimeOffset>(firstVisitSessionKey);
        if (firstVisit.HasValue)
        {
            FirstVisit = firstVisit.Value;
            return;
        }
        FirstVisit = _clock.UtcNow;
        HttpContext.Session.SetObject(firstVisitSessionKey, FirstVisit);
    }
}

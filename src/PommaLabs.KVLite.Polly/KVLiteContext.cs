﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using Polly;

namespace PommaLabs.KVLite.Polly;

/// <summary>
///   Polly <see cref="Context"/> extended in order to be created with a <see cref="CacheKey"/>.
/// </summary>
public class KVLiteContext : Context
{
    /// <summary>
    ///   Initializes a new instance of the <see cref="KVLiteContext"/> class, with the
    ///   specified <paramref name="operationKeyParts"/>.
    /// </summary>
    /// <param name="operationKeyParts">The operation key parts.</param>
    public KVLiteContext(params string[] operationKeyParts)
        : base(new CacheKey(operationKeyParts).Value)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="KVLiteContext"/> class, with the
    ///   specified <paramref name="operationKeyParts"/>.
    /// </summary>
    /// <param name="operationKeyParts">The operation key parts.</param>
    public KVLiteContext(params object[] operationKeyParts)
        : base(new CacheKey(operationKeyParts).Value)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="KVLiteContext"/> class, with the
    ///   specified <paramref name="operationKey"/>.
    /// </summary>
    /// <param name="operationKey">The operation key.</param>
    public KVLiteContext(CacheKey operationKey)
        : base(operationKey.Value)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="KVLiteContext"/> class, with the
    ///   specified <paramref name="operationKey"/> and the supplied <paramref name="contextData"/>.
    /// </summary>
    /// <param name="operationKey">The operation key.</param>
    /// <param name="contextData">The context data.</param>
    public KVLiteContext(CacheKey operationKey, IDictionary<string, object> contextData)
        : base(operationKey.Value, contextData)
    {
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite;

/// <summary>
///   Settings for <see cref="ICache"/>.
/// </summary>
public interface ICacheSettings
{
    /// <summary>
    ///   The cache name, can be used for logging.
    /// </summary>
    /// <value>The cache name.</value>
    string CacheName { get; set; }
}

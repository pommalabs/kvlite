﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

[TestFixture, Parallelizable, Category(nameof(Core))]
internal sealed class CacheKeyTests
{
    [Test]
    public void Constructor_ByteArrayAndMemoryStream()
    {
        var byteArray = new byte[] { 1, 2, 3, 4 };

        var ms = new MemoryStream(byteArray);
        new CacheKey(byteArray).Value.ShouldBe(new CacheKey(ms).Value);
        CacheKey.FromObjectArray(byteArray).Value.ShouldBe(CacheKey.FromObjectArray(ms).Value);

        ms.ReadByte();
        new CacheKey(byteArray).Value.ShouldNotBe(new CacheKey(ms).Value);

        ms.Dispose();
        new CacheKey(byteArray).Value.ShouldNotBe(new CacheKey(ms).Value);
    }

    [Test]
    public void Constructor_Expressions()
    {
        new CacheKey(() => Math.Round(3.14))
            .Value.ShouldBe("SYSTEM.MATH:ROUND:3.14");

        new CacheKey(() => Math.Min(3.3M, 4.4M))
            .Value.ShouldBe("SYSTEM.MATH:MIN:3.3:4.4");

        var random = new Random();
        new CacheKey(() => random.Next(0, 100))
            .Value.ShouldBe("SYSTEM.RANDOM:NEXT:0:100");
    }

    [TestCase("0123456789012345678901234567890123456789", "89012345678901234567890123456789")]
    [TestCase("aaaaaaaaaabbbbbbbbbbccccccccccddddddddddee", "BBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE")]
    [TestCase("AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE", "BBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE")]
    public void Constructor_LongString(string value, string expectedPrefix)
    {
        // Act
        var parts = new CacheKey(value).Value.Split('~');

        // Assert
        parts[0].ShouldBe(expectedPrefix);
        parts[1].ShouldMatch("\\d+");
    }

    [TestCase(new object[] { "ab.c" }, "AB.C")]
    [TestCase(new object[] { 12 }, "12")]
    [TestCase(new object[] { null }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "" }, CacheKey.NoValuePart)]
    [TestCase(new object[] { " " }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "   " }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "ab.c", 12 }, "AB.C:12")]
    [TestCase(new object[] { "ab.c", null }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", "" }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", " " }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", "   " }, "AB.C:_")]
    [TestCase(new object[] { null, "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { " ", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "   ", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "   ", "ab.c", 12 }, "_:AB.C:12")]
    public void Constructor_ManyObjects(object[] valueParts, string expected)
    {
        // Act
        var cacheKey = new CacheKey(valueParts);

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase(new[] { "ab.c" }, "AB.C")]
    [TestCase(new[] { "12.3" }, "12.3")]
    [TestCase(new string[] { null }, CacheKey.NoValuePart)]
    [TestCase(new[] { "" }, CacheKey.NoValuePart)]
    [TestCase(new[] { " " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "   " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "ab.c", "12.3" }, "AB.C:12.3")]
    [TestCase(new[] { "ab.c", null }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "" }, "AB.C:_")]
    [TestCase(new[] { "ab.c", " " }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "   " }, "AB.C:_")]
    [TestCase(new[] { null, "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { " ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c", "12.3" }, "_:AB.C:12.3")]
    public void Constructor_ManyStrings(string[] valueParts, string expected)
    {
        // Act
        var cacheKey = new CacheKey(valueParts);

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase("ab.c", "AB.C")]
    [TestCase("12.3", "12.3")]
    [TestCase(null, CacheKey.NoValuePart)]
    [TestCase("", CacheKey.NoValuePart)]
    [TestCase(" ", CacheKey.NoValuePart)]
    [TestCase("   ", CacheKey.NoValuePart)]
    public void Constructor_OneString(string value, string expected)
    {
        // Act
        var cacheKey = new CacheKey(value);

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase(new object[] { "ab.c", "ab.c" }, "AB.C:AB.C")]
    [TestCase(new object[] { 12, 12 }, "12:12")]
    [TestCase(new object[] { true, true }, "1:1")]
    public void Constructor_SameObjects(object[] valueParts, string expected)
    {
        // Act
        var cacheKey = new CacheKey(valueParts);

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [Test]
    public void FromObjectArray_ByteArrayAndMemoryStream()
    {
        // Arrange
        var byteArray = new byte[] { 1, 2, 3, 4 };
        var ms = new MemoryStream(byteArray);

        // Act & Assert
        CacheKey.FromObjectArray(byteArray).Value.ShouldBe(CacheKey.FromObjectArray(ms).Value);
    }

    [TestCase("0123456789012345678901234567890123456789", "89012345678901234567890123456789")]
    [TestCase("aaaaaaaaaabbbbbbbbbbccccccccccddddddddddee", "BBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE")]
    [TestCase("AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE", "BBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEE")]
    public void FromString_LongString(string value, string expectedPrefix)
    {
        // Act
        var parts = CacheKey.FromString(value).Value.Split('~');

        // Assert
        parts[0].ShouldBe(expectedPrefix);
        parts[1].ShouldMatch("\\d+");
    }

    [TestCase(new[] { "ab.c" }, "AB.C")]
    [TestCase(new[] { "12.3" }, "12.3")]
    [TestCase(new string[] { null }, CacheKey.NoValuePart)]
    [TestCase(new[] { "" }, CacheKey.NoValuePart)]
    [TestCase(new[] { " " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "   " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "ab.c", "12.3" }, "AB.C:12.3")]
    [TestCase(new[] { "ab.c", null }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "" }, "AB.C:_")]
    [TestCase(new[] { "ab.c", " " }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "   " }, "AB.C:_")]
    [TestCase(new[] { null, "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { " ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c", "12.3" }, "_:AB.C:12.3")]
    public void FromStringArray_ManyStrings(string[] valueParts, string expected)
    {
        // Act
        var cacheKey = CacheKey.FromStringArray(valueParts);

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase(new object[] { "ab.c" }, "AB.C")]
    [TestCase(new object[] { 12 }, "12")]
    [TestCase(new object[] { null }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "" }, CacheKey.NoValuePart)]
    [TestCase(new object[] { " " }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "   " }, CacheKey.NoValuePart)]
    [TestCase(new object[] { "ab.c", 12 }, "AB.C:12")]
    [TestCase(new object[] { "ab.c", null }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", "" }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", " " }, "AB.C:_")]
    [TestCase(new object[] { "ab.c", "   " }, "AB.C:_")]
    [TestCase(new object[] { null, "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { " ", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "   ", "ab.c" }, "_:AB.C")]
    [TestCase(new object[] { "   ", "ab.c", 12 }, "_:AB.C:12")]
    public void ImplicitCast_ManyObjects(object[] valueParts, string expected)
    {
        // Act
        CacheKey cacheKey = valueParts;

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase(new[] { "ab.c" }, "AB.C")]
    [TestCase(new[] { "12.3" }, "12.3")]
    [TestCase(new string[] { null }, CacheKey.NoValuePart)]
    [TestCase(new[] { "" }, CacheKey.NoValuePart)]
    [TestCase(new[] { " " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "   " }, CacheKey.NoValuePart)]
    [TestCase(new[] { "ab.c", "12.3" }, "AB.C:12.3")]
    [TestCase(new[] { "ab.c", null }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "" }, "AB.C:_")]
    [TestCase(new[] { "ab.c", " " }, "AB.C:_")]
    [TestCase(new[] { "ab.c", "   " }, "AB.C:_")]
    [TestCase(new[] { null, "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { " ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c" }, "_:AB.C")]
    [TestCase(new[] { "   ", "ab.c", "12.3" }, "_:AB.C:12.3")]
    public void ImplicitCast_ManyStrings(string[] valueParts, string expected)
    {
        // Act
        CacheKey cacheKey = valueParts;

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [TestCase("ab.c", "AB.C")]
    [TestCase("12.3", "12.3")]
    [TestCase(null, CacheKey.NoValuePart)]
    [TestCase("", CacheKey.NoValuePart)]
    [TestCase(" ", CacheKey.NoValuePart)]
    [TestCase("   ", CacheKey.NoValuePart)]
    public void ImplicitCast_OneString(string value, string expected)
    {
        // Act
        CacheKey cacheKey = value;

        // Assert
        cacheKey.Value.ShouldBe(expected);
        cacheKey.CompareTo(cacheKey).ShouldBe(0);
        cacheKey.CompareTo(expected).ShouldBe(0);
    }

    [Test]
    public void Truncate_LongerString_ShouldBeTruncated()
    {
        // Arrange
        CacheKey cacheKey = new string('a', 20);

        // Act
        var truncated = cacheKey.Truncate(10);

        // Assert
        truncated.Value.Length.ShouldBe(10);
    }

    [Test]
    public void Truncate_LongerStringWithSeparator_ShouldBeTruncated()
    {
        // Arrange
        CacheKey cacheKey = new[] { new string('a', 9), new string('b', 10) };

        // Act
        var truncated = cacheKey.Truncate(10);

        // Assert
        truncated.Value.Length.ShouldBe(9);
    }
}

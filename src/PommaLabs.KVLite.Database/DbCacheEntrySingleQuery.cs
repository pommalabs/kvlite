﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Database;

/// <summary>
///   Used to query a single entry.
/// </summary>
public sealed class DbCacheEntrySingleQuery
{
    /// <summary>
    ///   Hash of partition and key.
    /// </summary>
    public long Hash { get; set; }

    /// <summary>
    ///   Retrieve an entry even if it has expired.
    /// </summary>
    public byte IgnoreExpiryDate { get; set; }

    /// <summary>
    ///   When the entry will expire, expressed as seconds after UNIX epoch.
    /// </summary>
    public long UtcExpiry { get; set; }
}

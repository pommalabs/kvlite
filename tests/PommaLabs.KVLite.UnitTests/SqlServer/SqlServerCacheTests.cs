﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.KVLite.SqlServer;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.SqlServer;

[TestFixture]
[Category(nameof(SqlServer))]
[NonParallelizable]
internal sealed class SqlServerCacheTests : AbstractTests
{
    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        SqlServerCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance
}

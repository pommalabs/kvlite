﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Cache connection factory specialized for <see cref="PersistentCache"/>.
/// </summary>
public sealed class PersistentCacheConnectionFactory : SQLiteCacheConnectionFactory<PersistentCacheSettings>
{
    private static readonly string[] s_mapPathStarts = ["~//", "~\\\\", "~/", "~\\", "~"];

    /// <summary>
    ///   Cache connection factory specialized for <see cref="PersistentCache"/>.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    public PersistentCacheConnectionFactory(PersistentCacheSettings settings, ILogger<PersistentCache> logger)
        : base(settings, logger, "rwc", SQLiteJournalModeEnum.Wal, nameof(settings.CacheFile))
    {
        OnSettingsPropertyChanged(null, null);
    }

    /// <summary>
    ///   Updates the connection string used to operate the SQL cache.
    /// </summary>
    protected override void UpdateConnectionString()
    {
        // Map cache path, since it may be an IIS relative path.
        var dataSource = MapPath(Settings.CacheFile);

        // If the directory which should contain the cache does not exist, then we create it.
        // SQLite will take care of creating the DB itself.
        var cacheDirectory = Path.GetDirectoryName(dataSource);
        if (cacheDirectory != null && !Directory.Exists(cacheDirectory))
        {
            Directory.CreateDirectory(cacheDirectory);
        }

        ConnectionString = InitConnectionString(dataSource);

        EnsureSchemaIsReady();
    }

    /// <summary>
    ///   Maps given path into an absolute one.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns>Given path mapped into an absolute one.</returns>
    private static string MapPath(string path)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(path);

        if (Path.IsPathRooted(path))
        {
            return path;
        }

        var basePath = AppDomain.CurrentDomain.BaseDirectory;
        var trimmedPath = path.Trim();

        foreach (var startLength in s_mapPathStarts.Where(p => trimmedPath.StartsWith(p, StringComparison.Ordinal)).Select(p => p.Length))
        {
            trimmedPath = trimmedPath[startLength..];
        }

        return Path.Combine(basePath, trimmedPath);
    }
}

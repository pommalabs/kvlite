﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.CompilerServices;
using System.Text;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.Core;

/// <summary>
///   Hashing module.
/// </summary>
internal static unsafe class Hashing

{
    #region Partition and Key Hashing

    /// <summary>
    ///   Hashes given object.
    /// </summary>
    /// <param name="obj">Object.</param>
    /// <returns>Object hash.</returns>
    public static long HashObject(object obj)
    {
        using var serializedStream = MemoryStreamManager.GetMemoryStream();
        BinarySerializer.Instance.Serialize(serializedStream, obj);
        return (long)XXHash64.Calculate(serializedStream.GetBuffer(), (int)serializedStream.Length);
    }

    /// <summary>
    ///   Hashes given partition.
    /// </summary>
    /// <param name="partition">Partition.</param>
    /// <returns>Partition hash.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long? HashPartition(CacheKey? partition) => (partition.HasValue) ? ((long)XXHash32.CalculateRaw(partition.Value.Value) << 32) : new long?();

    /// <summary>
    ///   Hashes given partition and key.
    /// </summary>
    /// <param name="partition">Partition.</param>
    /// <param name="key">Key.</param>
    /// <returns>Partition and key hash.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long HashPartitionAndKey(CacheKey partition, CacheKey key) => ((long)XXHash32.CalculateRaw(partition.Value) << 32) + XXHash32.CalculateRaw(key.Value);

    #endregion Partition and Key Hashing

    #region Bits

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static uint RotateLeft32(uint value, int count)
    {
        return (value << count) | (value >> (32 - count));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    internal static ulong RotateLeft64(ulong value, int count)
    {
        return (value << count) | (value >> (64 - count));
    }

    #endregion Bits

    #region XXHash32 and XXHash64

    internal interface ICharacterModifier
    {
        char Modify(char ch);
    }

    internal readonly struct OrdinalIgnoreCaseModifier : ICharacterModifier, IEquatable<OrdinalIgnoreCaseModifier>
    {
        public static bool operator !=(OrdinalIgnoreCaseModifier x, OrdinalIgnoreCaseModifier y) => !x.Equals(y);

        public static bool operator ==(OrdinalIgnoreCaseModifier x, OrdinalIgnoreCaseModifier y) => x.Equals(y);

        public bool Equals(OrdinalIgnoreCaseModifier _) => true;

        public override bool Equals(object obj) => obj is OrdinalIgnoreCaseModifier other && Equals(other);

        public override int GetHashCode() => 0;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public char Modify(char ch)
        {
            if (ch >= 65 && ch <= 90)
                ch = (char)(ch | 0x0020);
            return ch;
        }
    }

    internal readonly struct OrdinalModifier : ICharacterModifier, IEquatable<OrdinalModifier>
    {
        public static bool operator !=(OrdinalModifier x, OrdinalModifier y) => !x.Equals(y);

        public static bool operator ==(OrdinalModifier x, OrdinalModifier y) => x.Equals(y);

        public bool Equals(OrdinalModifier _) => true;

        public override bool Equals(object obj) => obj is OrdinalModifier other && Equals(other);

        public override int GetHashCode() => 0;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public char Modify(char ch)
        {
            return ch;
        }
    }

    internal static class JumpConsistentHash
    {
        // A Fast, Minimal Memory, Consistent Hash Algorithm by John Lamping, Eric Veach. Relevant
        // article: https://arxiv.org/abs/1406.2294
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long Calculate(ulong key, int numBuckets)
        {
            var b = 1L;
            var j = 0L;
            while (j < numBuckets)
            {
                b = j;
                key = (key * 2862933555777941757UL) + 1;
                j = (long)((b + 1) * ((1L << 31) / ((double)(key >> 33) + 1)));
            }
            return b;
        }
    }

    /// <summary>
    ///   A port of the original XXHash algorithm from Google in 32bits.
    /// </summary>
    /// <remarks>
    ///   The 32bits and 64bits hashes for the same data are different. In short those are 2
    ///   entirely different algorithms.
    /// </remarks>
    internal static class XXHash32
    {
        public static uint Calculate(byte* buffer, int len, uint seed = 0)
        {
            return CalculateInline(buffer, len, seed);
        }

        public static uint Calculate(string value, Encoding encoder, uint seed = 0)
        {
            var buf = encoder.GetBytes(value);

            fixed (byte* buffer = buf)
            {
                return CalculateInline(buffer, buf.Length, seed);
            }
        }

        public static uint Calculate(string buffer, uint seed = 0)
        {
            return CalculateInline<OrdinalModifier>(buffer, seed);
        }

        public static uint Calculate<TCharacterModifier>(string buffer, uint seed = 0) where TCharacterModifier : struct, ICharacterModifier
        {
            return CalculateInline<TCharacterModifier>(buffer, seed);
        }

        public static uint Calculate(byte[] buf, int len = -1, uint seed = 0)
        {
            if (len == -1)
                len = buf.Length;

            fixed (byte* buffer = buf)
            {
                return CalculateInline(buffer, len, seed);
            }
        }

        public static uint Calculate(int[] buf, int len = -1, uint seed = 0)
        {
            if (len == -1)
                len = buf.Length;

            fixed (int* buffer = buf)
            {
                return Calculate((byte*)buffer, len * sizeof(int), seed);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint CalculateInline(byte* buffer, int len, uint seed = 0)
        {
            unchecked
            {
                uint h32;

                var bEnd = buffer + len;

                if (len >= 16)
                {
                    var limit = bEnd - 16;

                    var v1 = seed + XXHash32Constants.PRIME32_1 + XXHash32Constants.PRIME32_2;
                    var v2 = seed + XXHash32Constants.PRIME32_2;
                    var v3 = seed + 0;
                    var v4 = seed - XXHash32Constants.PRIME32_1;

                    do
                    {
                        v1 += ((uint*)buffer)[0] * XXHash32Constants.PRIME32_2;
                        v2 += ((uint*)buffer)[1] * XXHash32Constants.PRIME32_2;
                        v3 += ((uint*)buffer)[2] * XXHash32Constants.PRIME32_2;
                        v4 += ((uint*)buffer)[3] * XXHash32Constants.PRIME32_2;

                        buffer += 4 * sizeof(uint);

                        v1 = RotateLeft32(v1, 13);
                        v2 = RotateLeft32(v2, 13);
                        v3 = RotateLeft32(v3, 13);
                        v4 = RotateLeft32(v4, 13);

                        v1 *= XXHash32Constants.PRIME32_1;
                        v2 *= XXHash32Constants.PRIME32_1;
                        v3 *= XXHash32Constants.PRIME32_1;
                        v4 *= XXHash32Constants.PRIME32_1;
                    }
                    while (buffer <= limit);

                    h32 = RotateLeft32(v1, 1) + RotateLeft32(v2, 7) + RotateLeft32(v3, 12) + RotateLeft32(v4, 18);
                }
                else
                {
                    h32 = seed + XXHash32Constants.PRIME32_5;
                }

                h32 += (uint)len;

                while (buffer + 4 <= bEnd)
                {
                    h32 += *((uint*)buffer) * XXHash32Constants.PRIME32_3;
                    h32 = RotateLeft32(h32, 17) * XXHash32Constants.PRIME32_4;
                    buffer += 4;
                }

                while (buffer < bEnd)
                {
                    h32 += (uint)(*buffer) * XXHash32Constants.PRIME32_5;
                    h32 = RotateLeft32(h32, 11) * XXHash32Constants.PRIME32_1;
                    buffer++;
                }

                h32 ^= h32 >> 15;
                h32 *= XXHash32Constants.PRIME32_2;
                h32 ^= h32 >> 13;
                h32 *= XXHash32Constants.PRIME32_3;
                h32 ^= h32 >> 16;

                return h32;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint CalculateInline<TCharacterModifier>(string buffer, uint seed = 0) where TCharacterModifier : struct, ICharacterModifier
        {
            unchecked
            {
                uint h32;

                var len = (uint)buffer.Length;

                uint position = 0;
                if (len >= 8)
                {
                    var v1 = seed + XXHash32Constants.PRIME32_1 + XXHash32Constants.PRIME32_2;
                    var v2 = seed + XXHash32Constants.PRIME32_2;
                    var v3 = seed + 0;
                    var v4 = seed - XXHash32Constants.PRIME32_1;

                    var limit = len - 8;
                    do
                    {
                        v1 += CharToUInt32<TCharacterModifier>(buffer, position) * XXHash32Constants.PRIME32_2;
                        v2 += CharToUInt32<TCharacterModifier>(buffer, position + 2) * XXHash32Constants.PRIME32_2;
                        v3 += CharToUInt32<TCharacterModifier>(buffer, position + 4) * XXHash32Constants.PRIME32_2;
                        v4 += CharToUInt32<TCharacterModifier>(buffer, position + 6) * XXHash32Constants.PRIME32_2;

                        position += 8;

                        v1 = RotateLeft32(v1, 13);
                        v2 = RotateLeft32(v2, 13);
                        v3 = RotateLeft32(v3, 13);
                        v4 = RotateLeft32(v4, 13);

                        v1 *= XXHash32Constants.PRIME32_1;
                        v2 *= XXHash32Constants.PRIME32_1;
                        v3 *= XXHash32Constants.PRIME32_1;
                        v4 *= XXHash32Constants.PRIME32_1;
                    }
                    while (position <= limit);

                    h32 = RotateLeft32(v1, 1) + RotateLeft32(v2, 7) + RotateLeft32(v3, 12) + RotateLeft32(v4, 18);
                }
                else
                {
                    h32 = seed + XXHash32Constants.PRIME32_5;
                }

                h32 += len * sizeof(char);

                while (position + 2 <= len)
                {
                    h32 += CharToUInt32<TCharacterModifier>(buffer, position) * XXHash32Constants.PRIME32_3;
                    h32 = RotateLeft32(h32, 17) * XXHash32Constants.PRIME32_4;
                    position += 2;
                }

                h32 ^= h32 >> 15;
                h32 *= XXHash32Constants.PRIME32_2;
                h32 ^= h32 >> 13;
                h32 *= XXHash32Constants.PRIME32_3;
                h32 ^= h32 >> 16;

                return h32;
            }
        }

        public static uint CalculateRaw(string buffer, uint seed = 0)
        {
            fixed (char* bufferPtr = buffer)
            {
                return CalculateInline((byte*)bufferPtr, buffer.Length * sizeof(char), seed);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static uint CharToUInt32<TCharacterModifier>(string buffer, uint position) where TCharacterModifier : struct, ICharacterModifier
        {
            TCharacterModifier modifier = default;
            return ((uint)modifier.Modify(buffer[(int)position + 1]) << 16) | modifier.Modify(buffer[(int)position]);
        }
    }

    internal static class XXHash32Constants
    {
        internal const uint PRIME32_1 = 2654435761U;
        internal const uint PRIME32_2 = 2246822519U;
        internal const uint PRIME32_3 = 3266489917U;
        internal const uint PRIME32_4 = 668265263U;
        internal const uint PRIME32_5 = 374761393U;
    }

    /// <summary>
    ///   A port of the original XXHash algorithm from Google in 64bits.
    /// </summary>
    /// <remarks>
    ///   The 32bits and 64bits hashes for the same data are different. In short those are 2
    ///   entirely different algorithms.
    /// </remarks>
    internal static class XXHash64
    {
        public static ulong Calculate(byte* buffer, ulong len, ulong seed = 0)
        {
            return CalculateInline(buffer, len, seed);
        }

        public static ulong Calculate(string value, Encoding encoder, ulong seed = 0)
        {
            var buf = encoder.GetBytes(value);

            fixed (byte* buffer = buf)
            {
                return CalculateInline(buffer, (ulong)buf.Length, seed);
            }
        }

        public static ulong Calculate(string value, UTF8Encoding encoder, ulong seed = 0)
        {
            var buf = encoder.GetBytes(value);

            fixed (byte* buffer = buf)
            {
                return CalculateInline(buffer, (ulong)buf.Length, seed);
            }
        }

        public static ulong Calculate(byte[] buf, int len = -1, ulong seed = 0)
        {
            if (len == -1)
                len = buf.Length;

            fixed (byte* buffer = buf)
            {
                return CalculateInline(buffer, (ulong)len, seed);
            }
        }

        public static ulong Calculate(int[] buf, int len = -1, ulong seed = 0)
        {
            if (len == -1)
                len = buf.Length;

            fixed (int* buffer = buf)
            {
                return CalculateInline((byte*)buffer, (ulong)(len * sizeof(int)), seed);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ulong CalculateInline(byte* buffer, ulong len, ulong seed = 0)
        {
            ulong h64;

            var bEnd = buffer + len;

            if (len >= 32)
            {
                var limit = bEnd - 32;

                var v1 = seed + XXHash64Constants.PRIME64_1 + XXHash64Constants.PRIME64_2;
                var v2 = seed + XXHash64Constants.PRIME64_2;
                var v3 = seed + 0;
                var v4 = seed - XXHash64Constants.PRIME64_1;

                do
                {
                    v1 += ((ulong*)buffer)[0] * XXHash64Constants.PRIME64_2;
                    v2 += ((ulong*)buffer)[1] * XXHash64Constants.PRIME64_2;
                    v3 += ((ulong*)buffer)[2] * XXHash64Constants.PRIME64_2;
                    v4 += ((ulong*)buffer)[3] * XXHash64Constants.PRIME64_2;

                    buffer += 4 * sizeof(ulong);

                    v1 = RotateLeft64(v1, 31);
                    v2 = RotateLeft64(v2, 31);
                    v3 = RotateLeft64(v3, 31);
                    v4 = RotateLeft64(v4, 31);

                    v1 *= XXHash64Constants.PRIME64_1;
                    v2 *= XXHash64Constants.PRIME64_1;
                    v3 *= XXHash64Constants.PRIME64_1;
                    v4 *= XXHash64Constants.PRIME64_1;
                }
                while (buffer <= limit);

                h64 = RotateLeft64(v1, 1) + RotateLeft64(v2, 7) + RotateLeft64(v3, 12) + RotateLeft64(v4, 18);

                v1 *= XXHash64Constants.PRIME64_2;
                v2 *= XXHash64Constants.PRIME64_2;
                v3 *= XXHash64Constants.PRIME64_2;
                v4 *= XXHash64Constants.PRIME64_2;

                v1 = RotateLeft64(v1, 31);
                v2 = RotateLeft64(v2, 31);
                v3 = RotateLeft64(v3, 31);
                v4 = RotateLeft64(v4, 31);

                v1 *= XXHash64Constants.PRIME64_1;
                v2 *= XXHash64Constants.PRIME64_1;
                v3 *= XXHash64Constants.PRIME64_1;
                v4 *= XXHash64Constants.PRIME64_1;

                h64 ^= v1;
                h64 = (h64 * XXHash64Constants.PRIME64_1) + XXHash64Constants.PRIME64_4;

                h64 ^= v2;
                h64 = (h64 * XXHash64Constants.PRIME64_1) + XXHash64Constants.PRIME64_4;

                h64 ^= v3;
                h64 = (h64 * XXHash64Constants.PRIME64_1) + XXHash64Constants.PRIME64_4;

                h64 ^= v4;
                h64 = (h64 * XXHash64Constants.PRIME64_1) + XXHash64Constants.PRIME64_4;
            }
            else
            {
                h64 = seed + XXHash64Constants.PRIME64_5;
            }

            h64 += len;

            while (buffer + 8 <= bEnd)
            {
                var k1 = *((ulong*)buffer);
                k1 *= XXHash64Constants.PRIME64_2;
                k1 = RotateLeft64(k1, 31);
                k1 *= XXHash64Constants.PRIME64_1;
                h64 ^= k1;
                h64 = (RotateLeft64(h64, 27) * XXHash64Constants.PRIME64_1) + XXHash64Constants.PRIME64_4;
                buffer += 8;
            }

            if (buffer + 4 <= bEnd)
            {
                h64 ^= *(uint*)buffer * XXHash64Constants.PRIME64_1;
                h64 = (RotateLeft64(h64, 23) * XXHash64Constants.PRIME64_2) + XXHash64Constants.PRIME64_3;
                buffer += 4;
            }

            while (buffer < bEnd)
            {
                h64 ^= ((ulong)*buffer) * XXHash64Constants.PRIME64_5;
                h64 = RotateLeft64(h64, 11) * XXHash64Constants.PRIME64_1;
                buffer++;
            }

            h64 ^= h64 >> 33;
            h64 *= XXHash64Constants.PRIME64_2;
            h64 ^= h64 >> 29;
            h64 *= XXHash64Constants.PRIME64_3;
            h64 ^= h64 >> 32;

            return h64;
        }

        public static ulong CalculateRaw(string buf, ulong seed = 0)
        {
            fixed (char* buffer = buf)
            {
                return CalculateInline((byte*)buffer, (ulong)(buf.Length * sizeof(char)), seed);
            }
        }
    }

    internal static class XXHash64Constants
    {
        internal const ulong PRIME64_1 = 11400714785074694791UL;
        internal const ulong PRIME64_2 = 14029467366897019727UL;
        internal const ulong PRIME64_3 = 1609587929392839161UL;
        internal const ulong PRIME64_4 = 9650029242287828579UL;
        internal const ulong PRIME64_5 = 2870177450012600261UL;
    }

    #endregion XXHash32 and XXHash64
}

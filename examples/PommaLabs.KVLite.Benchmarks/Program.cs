﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using BenchmarkDotNet.Running;
using Dasync.Collections;
using PommaLabs.KVLite.Benchmarks.Models;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Serilog;
using FSharpBinarySerializer = MBrace.FsPickler.BinarySerializer;

namespace PommaLabs.KVLite.Benchmarks;

public static class Program
{
    private const int IterationCount = 3;
    private const int LogMessagesCount = 3000;
    private const string Partition = nameof(Benchmarks);
    private const int RowCount = 100;
    private const string Spacer = "------------------------------";
    private const int TablesCount = 1000;
    private static readonly string[] s_columnNames = ["A", "B", "C", "D", "E"];
    private static readonly TimeSpan s_interval = TimeSpan.FromHours(1);
    private static LogMessage[] s_logMessages;
    private static double s_logMessagesSize;
    private static double s_tableListSize;
    private static List<DataTable> s_tables;

    public static async Task Main(string[] args)
    {
        // Configure Serilog and LibLog logging.
        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

        Log.Information("Changing connection strings of default caches...");
        Caches.MySql.Settings.ConnectionString = DatabaseHelper.GetMySqlConnectionString();
        Caches.PostgreSql.Settings.ConnectionString = DatabaseHelper.GetPostgreSqlConnectionString();
        Caches.SqlServer.Settings.ConnectionString = DatabaseHelper.GetSqlServerConnectionString();
        PersistentCache.DefaultInstance.Settings.CacheFile = Path.GetTempFileName();

        Log.Information("Running vacuum on SQLite persistent cache...");
        PersistentCache.DefaultInstance.Vacuum();

        if (args.Length >= 1)
        {
            BenchmarkSwitcher.FromTypes(
            [
                typeof(AddLogMessages),
                typeof(CreateCacheKeys),
                typeof(GetOrAddLogMessages)
            ]).Run(args);
            return;
        }

        Log.Information("Generating random data tables...");
        s_tables = GenerateRandomDataTables();
        s_tableListSize = GetObjectSizeInMB(s_tables);

        Log.Information($"Table count: {TablesCount}");
        Log.Information($"Row count: {RowCount}");
        Log.Information($"Total table size: {s_tableListSize:0.0} MB");

        Log.Information("Generating random log messages...");
        s_logMessages = LogMessage.GenerateRandomLogMessages(LogMessagesCount);
        s_logMessagesSize = GetObjectSizeInMB(s_logMessages);

        Log.Information($"Log messages count: {TablesCount}");
        Log.Information($"Total log messages size: {s_logMessagesSize:0.0} MB");

        var caches = new ICache[]
        {
            Caches.Memory,
            Caches.MySql,
            Caches.PostgreSql,
            Caches.SqlServer,
            PersistentCache.DefaultInstance,
            VolatileCache.DefaultInstance
        };

        for (var i = 0; i < IterationCount; ++i)
        {
            /*** STORE EACH LOG MESSAGE ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await StoreEachLogMessageAsync(cache, i);
            }

            /*** STORE EACH DATA TABLE ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await StoreEachDataTableAsync(cache, i);
            }

            /*** STORE AND RETRIEVE EACH DATA TABLE TWO TIMES ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await StoreAndRetrieveEachDataTableAsync(cache, i);
            }

            /*** STORE EACH DATA TABLE ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                StoreEachDataTable(cache, i);
            }

            /*** STORE EACH DATA TABLE TWO TIMES ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                StoreEachDataTableTwoTimes(cache, i);
            }

            /*** REMOVE EACH DATA TABLE ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                RemoveEachDataTable(cache, i);
            }

            /*** REMOVE EACH DATA TABLE ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await RemoveEachDataTableAsync(cache, i);
            }

            /*** PEEK EACH DATA TABLE ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                PeekEachDataTable(cache, i);
            }

            /*** RETRIEVE EACH DATA TABLE ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                RetrieveEachDataTable(cache, i);
            }

            /*** RETRIEVE EACH DATA TABLE ENTRY ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                RetrieveEachDataTableEntry(cache, i);
            }

            /*** RETRIEVE EACH DATA TABLE ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await RetrieveEachDataTableAsync(cache, i);
            }

            /*** STORE DATA TABLES LIST ***/

            FullyCleanCaches();
            foreach (var cache in caches)
            {
                StoreDataTableList(cache, i);
            }

            /*** STORE EACH DATA TABLE TWO TIMES ASYNC ***/

            await FullyCleanCachesAsync();
            foreach (var cache in caches)
            {
                await StoreEachDataTableTwoTimesAsync(cache, i);
            }
        }

        FullyCleanCaches();

        Console.WriteLine();
        Console.Write("Press any key to exit...");
        Console.Read();
    }

    private static void FullyCleanCaches()
    {
        Log.Warning("Fully cleaning all caches...");
        Caches.Memory.Clear();
        Caches.MySql.Clear(CacheReadMode.IgnoreExpiryDate);
        Caches.PostgreSql.Clear(CacheReadMode.IgnoreExpiryDate);
        Caches.SqlServer.Clear(CacheReadMode.IgnoreExpiryDate);
        PersistentCache.DefaultInstance.Clear(CacheReadMode.IgnoreExpiryDate);
        VolatileCache.DefaultInstance.Clear(CacheReadMode.IgnoreExpiryDate);
    }

    private static async Task FullyCleanCachesAsync()
    {
        Log.Warning("Fully cleaning all caches (async)...");
        await Caches.Memory.ClearAsync();
        await Caches.MySql.ClearAsync(CacheReadMode.IgnoreExpiryDate);
        await Caches.PostgreSql.ClearAsync(CacheReadMode.IgnoreExpiryDate);
        await Caches.SqlServer.ClearAsync(CacheReadMode.IgnoreExpiryDate);
        await PersistentCache.DefaultInstance.ClearAsync(CacheReadMode.IgnoreExpiryDate);
        await VolatileCache.DefaultInstance.ClearAsync(CacheReadMode.IgnoreExpiryDate);
    }

    private static List<DataTable> GenerateRandomDataTables()
    {
        var gen = new RandomDataTableGenerator(s_columnNames);
        var list = new List<DataTable>();
        for (var i = 0; i < TablesCount; ++i)
        {
            list.Add(gen.GenerateDataTable(RowCount));
        }
        return list;
    }

    private static double GetObjectSizeInMB(object obj)
    {
        var result = new FSharpBinarySerializer().ComputeSize(obj) / (1024.0 * 1024.0);
        GC.Collect();
        return result;
    }

    private static void PeekEachDataTable<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        if (!cache.CanPeek)
        {
            return;
        }

        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Peeking each data table, iteration {iteration}...");

        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            var returnedTable = cache.Peek<DataTable>(Partition, table.TableName);
            if (!returnedTable.HasValue)
            {
                throw new Exception("Wrong data table read from cache! :(");
            }
        }
        stopwatch.Stop();

        Log.Information($"[{cacheName}] Data tables peeked in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void RemoveEachDataTable<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Removing each data table, iteration {iteration}...");

        foreach (var table in s_tables)
        {
            cache.AddTimed(Partition, table.TableName, table, cache.Clock.UtcNow + TimeSpan.FromHours(1));
        }

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            cache.Remove(Partition, table.TableName);
        }
        stopwatch.Stop();

        Debug.Assert(cache.Count() == 0);
        Debug.Assert(cache.LongCount() == 0L);

        Log.Information($"[{cacheName}] Data tables removed in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task RemoveEachDataTableAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Removing each data table asynchronously, iteration {iteration}...");

        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddTimedAsync(Partition, table.TableName, table, cache.Clock.UtcNow + TimeSpan.FromHours(1));
        }, Environment.ProcessorCount * 2);

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.RemoveAsync(Partition, table.TableName);
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Debug.Assert(await cache.CountAsync() == 0);
        Debug.Assert(await cache.LongCountAsync() == 0L);

        Log.Information($"[{cacheName}] Data tables removed in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void RetrieveEachDataTable<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Retrieving each data table, iteration {iteration}...");

        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            var returnedTable = cache.Get<DataTable>(Partition, table.TableName);
            if (!returnedTable.HasValue)
            {
                throw new Exception("Wrong data table read from cache! :(");
            }
        }
        stopwatch.Stop();

        Log.Information($"[{cacheName}] Data tables retrieved in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task RetrieveEachDataTableAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Retrieving each data table asynchronously, iteration {iteration}...");

        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddSlidingAsync(Partition, table.TableName, table, s_interval);
        }, Environment.ProcessorCount * 2);

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_tables.ParallelForEachAsync(async table =>
        {
            var returnedTable = await cache.GetAsync<DataTable>(Partition, table.TableName);
            if (!returnedTable.HasValue)
            {
                throw new Exception("Wrong data table read from cache! :(");
            }
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Log.Information($"[{cacheName}] Data tables retrieved in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024L * 1024L)} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void RetrieveEachDataTableEntry<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Retrieving each data table entry, iteration {iteration}...");

        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            var returnedTable = cache.GetEntry<DataTable>(Partition, table.TableName);
            if (!returnedTable.HasValue)
            {
                throw new Exception("Wrong data table entry read from cache! :(");
            }
        }
        stopwatch.Stop();

        Log.Information($"[{cacheName}] Data table entries retrieved in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task StoreAndRetrieveEachDataTableAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing and retrieving each data table asynchronously, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddSlidingAsync(Partition, table.TableName, table, s_interval);
            var returnedTable = await cache.GetAsync<DataTable>(Partition, table.TableName);
            if (!returnedTable.HasValue)
            {
                throw new Exception("Wrong data table read from cache! :(");
            }
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Debug.Assert(await cache.CountAsync() == s_tables.Count);
        Debug.Assert(await cache.LongCountAsync() == s_tables.Count);

        Log.Information($"[{cacheName}] Data tables stored and retrieved in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024L * 1024L)} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize * 2 / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void StoreDataTableList<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing data table list, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        cache.AddSliding(Partition, "TABLE_LIST", s_tables, s_interval);
        stopwatch.Stop();

        Debug.Assert(cache.Count() == 1);
        Debug.Assert(cache.LongCount() == 1);

        Log.Information($"[{cacheName}] Data table list stored in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024L * 1024L)} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void StoreEachDataTable<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing each data table, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }
        stopwatch.Stop();

        Debug.Assert(cache.Count() == s_tables.Count);
        Debug.Assert(cache.LongCount() == s_tables.Count);

        Log.Information($"[{cacheName}] Data tables stored in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task StoreEachDataTableAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing each data table asynchronously, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddSlidingAsync(Partition, table.TableName, table, s_interval);
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Debug.Assert(await cache.CountAsync() == s_tables.Count);
        Debug.Assert(await cache.LongCountAsync() == s_tables.Count);

        Log.Information($"[{cacheName}] Data tables stored in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static void StoreEachDataTableTwoTimes<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing each data table two times, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }
        foreach (var table in s_tables)
        {
            cache.AddSliding(Partition, table.TableName, table, s_interval);
        }
        stopwatch.Stop();

        Debug.Assert(cache.Count() == s_tables.Count);
        Debug.Assert(cache.LongCount() == s_tables.Count);

        Log.Information($"[{cacheName}] Data tables stored two times in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {cache.GetCacheSizeInBytes() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task StoreEachDataTableTwoTimesAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing each data table (two times, asynchronously), iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddSlidingAsync(Partition, table.TableName, table, s_interval);
        }, Environment.ProcessorCount * 2);
        await s_tables.ParallelForEachAsync(async table =>
        {
            await cache.AddSlidingAsync(Partition, table.TableName, table, s_interval);
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Debug.Assert(await cache.CountAsync() == s_tables.Count);
        Debug.Assert(await cache.LongCountAsync() == s_tables.Count);

        Log.Information($"[{cacheName}] Data tables stored in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024L * 1024L)} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_tableListSize * 2 / stopwatch.Elapsed.TotalSeconds:0.0}");
    }

    private static async Task StoreEachLogMessageAsync<TCache>(TCache cache, int iteration)
        where TCache : ICache
    {
        var cacheName = cache.Settings.CacheName;

        Console.WriteLine(Spacer);
        Log.Information($"[{cacheName}] Storing each log message asynchronously, iteration {iteration}...");

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await s_logMessages.ParallelForEachAsync(async (logMessage, i) =>
        {
            var logMessageKey = i.ToString();
            await cache.AddSlidingAsync(Partition, logMessageKey, logMessage, s_interval);
        }, Environment.ProcessorCount * 2);
        stopwatch.Stop();

        Debug.Assert(await cache.CountAsync() == s_logMessages.Length);
        Debug.Assert(await cache.LongCountAsync() == s_logMessages.LongLength);

        Log.Information($"[{cacheName}] Log messages stored in: {stopwatch.Elapsed}");
        Log.Information($"[{cacheName}] Current cache size: {await cache.GetCacheSizeInBytesAsync() / (1024.0 * 1024.0):0.0} MB");
        Log.Information($"[{cacheName}] Approximate speed (MB/sec): {s_logMessagesSize / stopwatch.Elapsed.TotalSeconds:0.0}");
    }
}

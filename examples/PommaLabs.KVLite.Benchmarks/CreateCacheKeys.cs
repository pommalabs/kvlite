﻿using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace PommaLabs.KVLite.Benchmarks;

[MemoryDiagnoser]
public class CreateCacheKeys
{
    private const int EntryCount = 10;

    private static readonly Guid[] s_guids = Enumerable.Range(0, EntryCount).Select(x => Guid.NewGuid()).ToArray();
    private static readonly int[] s_integers = Enumerable.Range(0, EntryCount).Select(x => (int)Math.Pow(10, x)).ToArray();
    private static readonly Random s_random = new();
    private static readonly string[] s_strings = Enumerable.Range('A', EntryCount).Select(x => new string((char)x, x)).ToArray();

    [Params(100, 1000)]
    public int Count { get; set; }

    [Benchmark]
    public List<CacheKey> Guids()
    {
        var cacheKeys = new List<CacheKey>();
        for (var i = 0; i < Count; ++i)
        {
            cacheKeys.Add(new CacheKey(s_guids[s_random.Next(EntryCount)]));
        }
        return cacheKeys;
    }

    [Benchmark]
    public List<CacheKey> Integers()
    {
        var cacheKeys = new List<CacheKey>();
        for (var i = 0; i < Count; ++i)
        {
            cacheKeys.Add(new CacheKey(s_integers[s_random.Next(EntryCount)]));
        }
        return cacheKeys;
    }

    [Benchmark]
    public List<CacheKey> ManyParts()
    {
        var cacheKeys = new List<CacheKey>();
        for (var i = 0; i < Count; ++i)
        {
            cacheKeys.Add(new CacheKey(
                s_strings[s_random.Next(EntryCount)],
                s_integers[s_random.Next(EntryCount)],
                s_guids[s_random.Next(EntryCount)]));
        }
        return cacheKeys;
    }

    [Benchmark(Baseline = true)]
    public List<CacheKey> Strings()
    {
        var cacheKeys = new List<CacheKey>();
        for (var i = 0; i < Count; ++i)
        {
            cacheKeys.Add(new CacheKey(s_strings[s_random.Next(EntryCount)]));
        }
        return cacheKeys;
    }
}

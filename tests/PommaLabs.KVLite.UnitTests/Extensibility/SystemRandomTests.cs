﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.Extensibility;

[TestFixture]
[Category(nameof(Extensibility))]
[Parallelizable]
internal sealed class SystemRandomTests : AbstractTests
{
    [Test]
    public void NextRandom_ShouldNotThrow()
    {
        Should.NotThrow(() => new SystemRandom().NextDouble());
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;
using Microsoft.IO;

namespace PommaLabs.KVLite.Core;

/// <summary>
///   Memory stream manager, which relies on an <see cref="RecyclableMemoryStreamManager"/> instance.
/// </summary>
internal static class MemoryStreamManager
{
    /// <summary>
    ///   Recyclable memory stream manager instance.
    /// </summary>
    private static readonly RecyclableMemoryStreamManager s_instance = new();

    /// <summary>
    ///   Returns a new <see cref="MemoryStream"/> object with a default initial capacity.
    /// </summary>
    /// <returns>A new <see cref="MemoryStream"/> object with a default initial capacity.</returns>
    public static MemoryStream GetMemoryStream() => s_instance.GetStream();
}

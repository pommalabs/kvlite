﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Random generator based on <see cref="Random"/> BCL class. An instance of this class is
///   _not_ thread safe, since the underlying random generator is not thread safe.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[SuppressMessage("Security", "SCS0005:Weak random number generator.", Justification = "It is not used for cryptographic purposes")]
public sealed class SystemRandom : IRandom
{
    private readonly Random _random = new();

    /// <summary>
    ///   Returns a random floating-point number that is greater than or equal to 0.0, and less
    ///   than 1.0.
    /// </summary>
    /// <returns>
    ///   A double-precision floating point number that is greater than or equal to 0.0, and
    ///   less than 1.0.
    /// </returns>
    public double NextDouble() => _random.NextDouble();
}

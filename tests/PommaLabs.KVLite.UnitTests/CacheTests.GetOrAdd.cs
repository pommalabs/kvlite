﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

internal partial class CacheTests
{
    [Test]
    public void GetOrAddSliding_EntryAvailable_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.GetOrAddSliding(p, k, () => Tuple.Create(v1, v2), i);

        // Try to add again, should not work.
        var r = _cache.GetOrAddSliding(p, k, () => Tuple.Create(v2, v1), i);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void GetOrAddSliding_EntryMissing_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        var r = _cache.GetOrAddSliding(p, k, () => Tuple.Create(v1, v2), i);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void GetOrAddSliding_Expression_Filtered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.GetOrAddSliding(p, () => Tuple.Create(v1, v2), i, valueFilterer: _ => true);

        _cache.Count().ShouldBe(0);
        _cache.LongCount().ShouldBe(0L);
    }

    [Test]
    public void GetOrAddSliding_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        var r = _cache.GetOrAddSliding(p, () => Tuple.Create(v1, v2), i);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => Tuple.Create(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void GetOrAddSliding_Expression_Unfiltered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.GetOrAddSliding(p, () => Tuple.Create(v1, v2), i, valueFilterer: _ => false);

        _cache.Count().ShouldBe(1);
        _cache.LongCount().ShouldBe(1L);
    }

    [Test]
    public async Task GetOrAddSlidingAsync_Expression_Filtered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        await _cache.GetOrAddSlidingAsync(
            p, () => AsyncTupleCreate(v1, v2), i,
            valueFilterer: _ => true);

        _cache.Count().ShouldBe(0);
        _cache.LongCount().ShouldBe(0L);
    }

    [Test]
    public async Task GetOrAddSlidingAsync_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        var r = await _cache.GetOrAddSlidingAsync(p, () => AsyncTupleCreate(v1, v2), i);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => AsyncTupleCreate(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public async Task GetOrAddSlidingAsync_Expression_Unfiltered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        await _cache.GetOrAddSlidingAsync(
            p, () => AsyncTupleCreate(v1, v2), i,
            valueFilterer: _ => false);

        _cache.Count().ShouldBe(1);
        _cache.LongCount().ShouldBe(1L);
    }

    [Test]
    public void GetOrAddTimed_EntryAvailable_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.GetOrAddTimed(p, k, () => Tuple.Create(v1, v2), e);

        // Try to add again, should not work.
        var r = _cache.GetOrAddTimed(p, k, () => Tuple.Create(v2, v1), e);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void GetOrAddTimed_Expression_Filtered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.GetOrAddTimed(p, () => Tuple.Create(v1, v2), e, valueFilterer: _ => true);

        _cache.Count().ShouldBe(0);
        _cache.LongCount().ShouldBe(0L);
    }

    [Test]
    public void GetOrAddTimed_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        var r = _cache.GetOrAddTimed(p, () => Tuple.Create(v1, v2), e);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => Tuple.Create(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void GetOrAddTimed_Expression_Unfiltered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.GetOrAddTimed(p, () => Tuple.Create(v1, v2), e, valueFilterer: _ => false);

        _cache.Count().ShouldBe(1);
        _cache.LongCount().ShouldBe(1L);
    }

    [Test]
    public void GetOrAddTimed_MissingEntry_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        var r = _cache.GetOrAddTimed(p, k, () => Tuple.Create(v1, v2), e);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void GetOrAddTimed_WithDuration_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var l = TenMinutes;

        var r = _cache.GetOrAddTimed(p, () => Tuple.Create(v1, v2), l);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => Tuple.Create(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        var e = _cache.Clock.UtcNow.Add(l);
        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void GetOrAddTimed_WithDuration_MissingEntry_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var l = TenMinutes;

        var r = _cache.GetOrAddTimed(p, k, () => Tuple.Create(v1, v2), l);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        var e = _cache.Clock.UtcNow.Add(l);
        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public async Task GetOrAddTimedAsync_Expression_Filtered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        await _cache.GetOrAddTimedAsync(
            p, () => AsyncTupleCreate(v1, v2), e,
            valueFilterer: _ => true);

        _cache.Count().ShouldBe(0);
        _cache.LongCount().ShouldBe(0L);
    }

    [Test]
    public async Task GetOrAddTimedAsync_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        var r = await _cache.GetOrAddTimedAsync(p, () => AsyncTupleCreate(v1, v2), e);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => AsyncTupleCreate(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public async Task GetOrAddTimedAsync_Expression_Unfiltered()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        await _cache.GetOrAddTimedAsync(
            p, () => AsyncTupleCreate(v1, v2), e,
            valueFilterer: _ => false);

        _cache.Count().ShouldBe(1);
        _cache.LongCount().ShouldBe(1L);
    }

    [Test]
    public async Task GetOrAddTimedAsync_WithDuration_Expression_RightInfo()
    {
        var p = CacheKeys[0];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var l = TenMinutes;

        var r = await _cache.GetOrAddTimedAsync(p, () => AsyncTupleCreate(v1, v2), l);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var k = new CacheKey(() => AsyncTupleCreate(v1, v2));

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        var e = _cache.Clock.UtcNow.Add(l);
        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public async Task GetOrAddTimedAsync_WithDuration_MissingEntry_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var l = TenMinutes;

        var r = await _cache.GetOrAddTimedAsync(p, k, () => Task.FromResult(Tuple.Create(v1, v2)), l);
        r.Item1.ShouldBe(v1);
        r.Item2.ShouldBe(v2);

        var info = (await _cache.GetEntryAsync<Tuple<StringValue, StringValue>>(p, k)).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        var e = _cache.Clock.UtcNow.Add(l);
        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    private static Task<Tuple<T1, T2>> AsyncTupleCreate<T1, T2>(T1 s1, T2 s2) => Task.FromResult(Tuple.Create(s1, s2));
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

[TestFixture, Parallelizable, Category(nameof(Core))]
internal sealed class CacheResultTests
{
    [Test]
    public void Deconstruct_ClassValueType_NoValue()
    {
        var (hasValue, value) = default(CacheResult<string>);
        hasValue.ShouldBeFalse();
        value.ShouldBe(default);
    }

    [Test]
    public void Deconstruct_ClassValueType_WithValue()
    {
        const string Hello = "Hello!";
        var (hasValue, value) = (CacheResult<string>)Hello;
        hasValue.ShouldBeTrue();
        value.ShouldBe(Hello);
    }

    [Test]
    public void Deconstruct_StructValueType_NoValue()
    {
        var (hasValue, value) = default(CacheResult<int>);
        hasValue.ShouldBeFalse();
        value.ShouldBe(default);
    }

    [Test]
    public void Deconstruct_StructValueType_WithValue()
    {
        const int Ten = 10;
        var (hasValue, value) = (CacheResult<int>)Ten;
        hasValue.ShouldBeTrue();
        value.ShouldBe(Ten);
    }
}

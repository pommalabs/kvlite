﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.Extensions.Logging;
using Npgsql;
using PommaLabs.KVLite.Database;
using PommaLabs.KVLite.Extensibility;

namespace PommaLabs.KVLite.PostgreSql;

/// <summary>
///   Cache backed by PostgreSQL.
/// </summary>
public sealed class PostgreSqlCache : DbCache<PostgreSqlCache, PostgreSqlCacheSettings, PostgreSqlCacheConnectionFactory, NpgsqlConnection, NpgsqlTransaction, NpgsqlCommand>
{
    #region Default instance

    /// <summary>
    ///   Gets the default instance for this cache kind. Default instance is configured using
    ///   default cache settings.
    /// </summary>
    public static PostgreSqlCache DefaultInstance { get; } = new(new PostgreSqlCacheSettings());

    #endregion Default instance

    /// <summary>
    ///   Initializes a new instance of the <see cref="PostgreSqlCache"/> class with given settings.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public PostgreSqlCache(PostgreSqlCacheSettings settings, ISerializer serializer = null, IClock clock = null,
        ILogger<PostgreSqlCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : this(settings, new PostgreSqlCacheConnectionFactory(settings, logger), serializer, clock, logger, interceptor, random)
    {
    }

    /// <summary>
    ///   Initializes a new instance of the <see cref="PostgreSqlCache"/> class with given
    ///   settings and specified connection factory.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="connectionFactory">Cache connection factory.</param>
    /// <param name="serializer">The serializer.</param>
    /// <param name="clock">The clock.</param>
    /// <param name="logger">The logger.</param>
    /// <param name="interceptor">The interceptor.</param>
    /// <param name="random">The random number generator.</param>
    public PostgreSqlCache(
        PostgreSqlCacheSettings settings, PostgreSqlCacheConnectionFactory connectionFactory, ISerializer serializer = null,
        IClock clock = null, ILogger<PostgreSqlCache> logger = null, IInterceptor interceptor = null, IRandom random = null)
        : base(settings, connectionFactory, serializer, clock, logger, interceptor, random)
    {
    }
}

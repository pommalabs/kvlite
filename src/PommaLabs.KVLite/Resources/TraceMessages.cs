﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Resources;

internal static class TraceMessages
{
    public const string AddEntry = "Added entry '{Partition}/{Key}' to {Cache} with UTC expiry '{UtcExpiry}' and interval '{Interval}' in {ElapsedMilliseconds} ms";
    public const string AutoSchemaCreationDisabled = "Automatic schema creation has been disabled, skipping schema check";
    public const string GetEntry = "Read entry '{Partition}/{Key}' from {Cache} in {ElapsedMilliseconds} ms";
    public const string InternalErrorOnCreateSchema = "Could not create cache schema, please check SQL user permissions: {CacheSchema}";
}

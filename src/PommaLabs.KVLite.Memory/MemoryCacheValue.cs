﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.KVLite.Memory;

internal sealed class MemoryCacheValue
{
    public static HashSet<MemoryCacheKey> NoParentKeys { get; } = new();

    public MemoryCacheKey CacheKey { get; set; }

    public bool Compressed { get; set; }

    public TimeSpan Interval { get; set; }

    public HashSet<MemoryCacheKey> ParentKeys { get; set; } = NoParentKeys;

    public DateTimeOffset UtcCreation { get; set; }

    public DateTimeOffset UtcExpiry { get; set; }

    public byte[] Value { get; set; }
}

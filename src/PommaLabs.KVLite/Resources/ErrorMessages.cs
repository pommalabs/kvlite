﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Resources;

internal static class ErrorMessages
{
    public const string CacheDoesNotAllowPeeking = "{0} does not allow peeking entries, therefore this method is not implemented";
    public const string CacheDoesNotAllowSlidingAndAbsolute = "KVLite caching interfaces do not allow setting a sliding entry with absolute expiration";
    public const string CacheHasBeenDisposed = "{0} instance has been disposed, therefore no more operations are allowed on it";
    public const string EmptyCacheResult = "Cache result has no value";
    public const string FullCache = "Cache is full! Current size {0}, maximum size {1}";
    public const string InternalErrorOnClearAll = "An error occurred while clearing all {Cache} partitions";
    public const string InternalErrorOnClearPartition = "An error occurred while clearing {Cache} partition '{Partition}'";
    public const string InternalErrorOnCountAll = "An error occurred while counting entries in all {Cache} partitions";
    public const string InternalErrorOnCountPartition = "An error occurred while counting entries in {Cache} partition '{Partition}'";
    public const string InternalErrorOnDeserialization = "Entry '{Partition}/{Key}' from {Cache} could not be deserialized";
    public const string InternalErrorOnRead = "An error occurred while reading entry '{Partition}/{Key}' from {Cache}";
    public const string InternalErrorOnReadAll = "An error occurred while reading entries in all {Cache} partitions";
    public const string InternalErrorOnReadPartition = "An error occurred while reading entries in {Cache} partition '{Partition}'";
    public const string InternalErrorOnSerialization = "Value '{Value}' could not be serialized";
    public const string InternalErrorOnVacuum = "An error occurred while applying VACUUM on the SQLite cache";
    public const string InternalErrorOnWrite = "An error occurred while writing entry '{Partition}/{Key}' into {Cache}";
    public const string InvalidCacheEntriesTableName = "Specified name for SQL entries table is not valid";
    public const string InvalidCacheName = "In-memory cache name can only contain alphanumeric characters, dots and underscores";
    public const string InvalidCacheReadMode = "An invalid enumeration value was given for cache read mode";
    public const string InvalidCacheSchemaName = "Specified SQL schema name is not valid";
    public const string InvalidColumnName = "Specified name for SQL column is not valid";
    public const string InvalidExpression = "Only method call expression are supported";
    public const string NotSerializableValue = "Only serializable objects can be stored into the cache";
    public const string NullCache = "Cache cannot be null, please specify one valid cache or use either PersistentCache or VolatileCache default instances";
    public const string NullCacheResolver = "Cache resolver function cannot be null, please specify one non-null function";
    public const string NullOrEmptyCacheFile = "Cache file cannot be null or empty";
    public const string NullOrEmptyCacheName = "Cache name cannot be null or empty";
    public const string NullOrEmptyConnectionString = "Connection string cannot be null or empty";
    public const string NullSettings = "Settings cannot be null, please specify valid settings or use the default instance";
    public const string NullValueGetter = "Value getter function cannot be null, please specify one non-null function";
    public const string TooManyParentKeys = "Too many parent keys have been specified for this entry";
}

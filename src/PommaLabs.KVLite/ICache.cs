﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace PommaLabs.KVLite;

/// <summary>
///   <para>
///     This interface represents a partition based key-value store. Each (partition, key,
///     value) triple has attached either an expiry time or a refresh interval, because values
///     should not be stored forever inside a cache.
///   </para>
///   <para>
///     In fact, a cache is, almost by definition, a transient store, used to temporaly store
///     the results of time consuming operations. This kind of cache should, therefore, store
///     any kind of object for a predetermined amount of time, trying to be extremely efficient
///     while handling entries. The cache does its best in order to be a reliable store, but it
///     should not be treated like a database: long story short, your code needs to be aware
///     that values in this cache may disappear as time passes.
///   </para>
/// </summary>
public partial interface ICache : IDisposable
{
    /// <summary>
    ///   Gets the value with the specified partition and key.
    /// </summary>
    /// <value>The value with the specified partition and key.</value>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>The value with the specified partition and key.</returns>
    /// <remarks>
    ///   This method, differently from other readers (like
    ///   <see cref="Get{TVal}(CacheKey,CacheKey)"/> or
    ///   <see cref="Peek{TVal}(CacheKey,CacheKey)"/>), does not have a typed return object,
    ///   because indexers cannot be generic. Therefore, we have to return a simple <see cref="object"/>.
    /// </remarks>
    CacheResult<object> this[CacheKey partition, CacheKey key] { get; }

    /// <summary>
    ///   Computes cache size in bytes. This value might be an estimate of real cache size and,
    ///   therefore, it does not need to be extremely accurate.
    /// </summary>
    /// <returns>An estimate of cache size in bytes.</returns>
    long GetCacheSizeInBytes();

    #region Add

    /// <summary>
    ///   Adds a "sliding" value with given partition and key. Value will last as much as
    ///   specified in given interval and, if accessed before expiry, its lifetime will be
    ///   extended by the interval itself.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="interval">The interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    void AddSliding<TVal>(CacheKey partition, CacheKey key, TVal value, TimeSpan interval, IList<CacheKey> parentKeys = default);

    /// <summary>
    ///   Adds a "timed" value with given partition and key. Value will last until the specified
    ///   time and, if accessed before expiry, its lifetime will _not_ be extended.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="utcExpiry">The UTC expiry.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    void AddTimed<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, IList<CacheKey> parentKeys = default);

    #endregion Add

    #region Clear

    /// <summary>
    ///   Clears this instance, that is, it removes all stored entries.
    /// </summary>
    /// <returns>The number of entries that have been removed.</returns>
    long Clear();

    /// <summary>
    ///   Clears given partition, that is, it removes all its entries.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <returns>The number of entries that have been removed.</returns>
    long Clear(CacheKey partition);

    #endregion Clear

    #region Count

    /// <summary>
    ///   The number of entries stored in the cache.
    /// </summary>
    /// <returns>The number of entries stored in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    int Count();

    /// <summary>
    ///   The number of entries stored in given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <returns>The number of entries stored in given partition.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    int Count(CacheKey partition);

    /// <summary>
    ///   The number of entries stored in the cache.
    /// </summary>
    /// <returns>The number of entries stored in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    long LongCount();

    /// <summary>
    ///   The number of entries stored in given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <returns>The number of entries stored in given partition.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    long LongCount(CacheKey partition);

    #endregion Count

    #region Contains

    /// <summary>
    ///   Determines whether this cache contains the specified partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <returns>Whether this cache contains the specified partition and key.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    bool Contains(CacheKey partition, CacheKey key);

    #endregion Contains

    #region Get

    /// <summary>
    ///   Gets the value with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by the corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <returns>The value with specified partition and key.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    [SuppressMessage("Naming", "CA1716:Identifiers should not match keywords", Justification = "Cannot be changed in order not to break clients")]
    CacheResult<TVal> Get<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Gets all cache entries. If an entry is a "sliding" value, its lifetime will be
    ///   increased by corresponding interval.
    /// </summary>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    IList<ICacheEntry<TVal>> GetEntries<TVal>(CacheQueryOptions queryOptions = default);

    /// <summary>
    ///   Gets all cache entries in given partition. If an entry is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries in given partition.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    IList<ICacheEntry<TVal>> GetEntries<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default);

    /// <summary>
    ///   Gets the cache entry with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <returns>The cache entry with specified partition and key.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    CacheResult<ICacheEntry<TVal>> GetEntry<TVal>(CacheKey partition, CacheKey key);

    #endregion Get

    #region GetOrAdd

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "sliding" value with given partition and
    ///     key. Value will last as much as specified in given interval and, if accessed before
    ///     expiry, its lifetime will be extended by the interval itself.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="valueGetter">
    ///   The function that is called in order to get the value when it was not found inside the cache.
    /// </param>
    /// <param name="interval">The interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by <paramref name="valueGetter"/>, in
    ///   case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="valueGetter"/> is null.</exception>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    TVal GetOrAddSliding<TVal>(
        CacheKey partition, CacheKey key, Func<TVal> valueGetter, TimeSpan interval,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default);

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "timed" value with given partition and
    ///     key. Value will last until the specified time and, if accessed before expiry, its
    ///     lifetime will _not_ be extended.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="valueGetter">
    ///   The function that is called in order to get the value when it was not found inside the cache.
    /// </param>
    /// <param name="utcExpiry">The UTC expiry.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <returns>
    ///   The value found in the cache or the one returned by <paramref name="valueGetter"/>, in
    ///   case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="valueGetter"/> is null.</exception>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    TVal GetOrAddTimed<TVal>(
        CacheKey partition, CacheKey key, Func<TVal> valueGetter, DateTimeOffset utcExpiry,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default);

    #endregion GetOrAdd

    #region Peek

    /// <summary>
    ///   Gets the value corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>
    ///   The value corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    CacheResult<TVal> Peek<TVal>(CacheKey partition, CacheKey key);

    /// <summary>
    ///   Gets the all values, without updating expiry dates.
    /// </summary>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All values, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    IList<ICacheEntry<TVal>> PeekEntries<TVal>(CacheQueryOptions queryOptions = default);

    /// <summary>
    ///   Gets the all entries in given partition, without updating expiry dates.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All entries in given partition, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    IList<ICacheEntry<TVal>> PeekEntries<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default);

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    CacheResult<ICacheEntry<TVal>> PeekEntry<TVal>(CacheKey partition, CacheKey key);

    #endregion Peek

    #region Remove

    /// <summary>
    ///   Removes the entry with given partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    void Remove(CacheKey partition, CacheKey key);

    #endregion Remove
}

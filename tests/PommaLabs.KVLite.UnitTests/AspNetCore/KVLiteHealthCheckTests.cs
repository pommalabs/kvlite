﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NUnit.Framework;
using PommaLabs.KVLite.AspNetCore;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.UnitTests.Infrastructure;

namespace PommaLabs.KVLite.UnitTests.AspNetCore;

[TestFixture]
[Category(nameof(AspNetCore))]
[Parallelizable(ParallelScope.Fixtures)]
internal sealed class KVLiteHealthCheckTests : AbstractTests
{
    private const string CacheKey = nameof(CacheKey);

    private static readonly TimeSpan s_expirationDelay = TimeSpan.FromMinutes(1);

    #region Setup/Teardown

    [SetUp]
    public void SetUp()
    {
        KVLiteHealthCheck.ClearCachedResults();
    }

    [TearDown]
    public void TearDown()
    {
        KVLiteHealthCheck.ClearCachedResults();
    }

    #endregion Setup/Teardown

    [Test]
    public async Task ShouldReuseCachedResultWhenHealthCheckFailsAndResultIsStillValid()
    {
        // Arrange
        var cache = A.Fake<ICache>();
        A.CallTo(() => cache.GetAsync<Guid>(A<CacheKey>._, A<CacheKey>._, A<CancellationToken>._))
            .Returns(default(CacheResult<Guid>));

        var clock = new FakeClock(SystemClock.Instance.UtcNow);
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(IClock))).Returns(clock);

        var healthCheck = new KVLiteHealthCheck(cache, serviceProvider);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => cache.AddTimedAsync(A<CacheKey>._, A<CacheKey>._, A<Guid>._, A<DateTimeOffset>._, An<IList<CacheKey>>._, A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResultWhenHealthCheckSucceedsAndResultIsStillValid()
    {
        // Arrange
        var cache = MemoryCache.DefaultInstance;

        var clock = new FakeClock(SystemClock.Instance.UtcNow);
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(IClock))).Returns(clock);

        var healthCheck = new KVLiteHealthCheck(cache, serviceProvider);

        var context = new HealthCheckContext();
        var firstResult = await healthCheck.CheckHealthAsync(context);

        // Act
        var secondResult = await healthCheck.CheckHealthAsync(context);

        // Assert
        Assert.That(secondResult.Data[CacheKey], Is.EqualTo(firstResult.Data[CacheKey]));
    }

    [Test]
    public async Task ShouldNotReuseCachedResultWhenHealthCheckFailsAndResultIsNotValid()
    {
        // Arrange
        var cache = A.Fake<ICache>();
        A.CallTo(() => cache.GetAsync<Guid>(A<CacheKey>._, A<CacheKey>._, A<CancellationToken>._))
            .Returns(default(CacheResult<Guid>));

        var clock = new FakeClock(SystemClock.Instance.UtcNow);
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(IClock))).Returns(clock);

        var healthCheck = new KVLiteHealthCheck(cache, serviceProvider);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        clock.Advance(s_expirationDelay);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => cache.AddTimedAsync(A<CacheKey>._, A<CacheKey>._, A<Guid>._, A<DateTimeOffset>._, An<IList<CacheKey>>._, A<CancellationToken>._))
            .MustHaveHappenedTwiceExactly();
    }

    [Test]
    public async Task ShouldNotReuseCachedResultWhenHealthCheckSucceedsAndResultIsNotValid()
    {
        // Arrange
        var cache = MemoryCache.DefaultInstance;

        var clock = new FakeClock(SystemClock.Instance.UtcNow);
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(IClock))).Returns(clock);

        var healthCheck = new KVLiteHealthCheck(cache, serviceProvider);

        var context = new HealthCheckContext();
        var firstResult = await healthCheck.CheckHealthAsync(context);

        clock.Advance(s_expirationDelay);

        // Act
        var secondResult = await healthCheck.CheckHealthAsync(context);

        // Assert
        Assert.That(secondResult.Data[CacheKey], Is.Not.EqualTo(firstResult.Data[CacheKey]));
    }

    [Test]
    public async Task ShouldNotReuseCachedResultAcrossDifferentCaches()
    {
        // Arrange
        var clock = new FakeClock(SystemClock.Instance.UtcNow);
        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(IClock))).Returns(clock);

        var firstCache = A.Fake<ICache>();
        var firstHealthCheck = new KVLiteHealthCheck(firstCache, serviceProvider);
        var firstContext = new HealthCheckContext();

        var secondCache = A.Fake<ICache>();
        var secondHealthCheck = new KVLiteHealthCheck(secondCache, serviceProvider);
        var secondContext = new HealthCheckContext();

        // Act
        var firstResult = await firstHealthCheck.CheckHealthAsync(firstContext);
        var secondResult = await secondHealthCheck.CheckHealthAsync(secondContext);

        // Assert
        Assert.That(secondResult.Data[CacheKey], Is.Not.EqualTo(firstResult.Data[CacheKey]));
    }
}

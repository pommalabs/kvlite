﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Linq;
using Bogus;

namespace PommaLabs.KVLite.Benchmarks.Models;

[Serializable]
public enum LogLevel
{
    Trace,
    Debug,
    Info,
    Warn,
    Error,
    Fatal
}

[Serializable]
public sealed class LogMessage
{
    private static readonly Faker s_faker = new();
    private static readonly LogLevel[] s_logLevels = [LogLevel.Debug, LogLevel.Error, LogLevel.Fatal, LogLevel.Info, LogLevel.Trace, LogLevel.Warn];
    private static readonly Random s_random = new();

    public LogLevel Level { get; set; }

    public string LongMessage { get; set; }

    public string ShortMessage { get; set; }

    public static LogMessage[] GenerateRandomLogMessages(int count) => Enumerable.Range(0, count).Select(_ => new LogMessage
    {
        Level = s_logLevels[s_random.Next(0, s_logLevels.Length)],
        ShortMessage = s_faker.Lorem.Sentence(),
        LongMessage = s_faker.Lorem.Paragraphs(s_random.Next(5, 10))
    }).ToArray();
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   A special interceptor which does simply nothing. It is used as default interceptor.
/// </summary>
public sealed class NoOpInterceptor : IInterceptor
{
    /// <summary>
    ///   Thread safe singleton.
    /// </summary>
    public static NoOpInterceptor Instance { get; } = new();

    /// <inheritdoc/>
    public void EntryAdding(ref CacheKey partition, ref CacheKey key)
    {
        // This interceptor does nothing when an entry is added.
    }

    /// <inheritdoc/>
    public void EntryGetting(ref CacheKey partition, ref CacheKey key)
    {
        // This interceptor does nothing when an entry is being retrieved.
    }
}

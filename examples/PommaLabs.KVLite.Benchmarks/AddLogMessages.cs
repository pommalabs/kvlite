﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using BenchmarkDotNet.Attributes;
using PommaLabs.KVLite.Benchmarks.Models;

namespace PommaLabs.KVLite.Benchmarks;

[MemoryDiagnoser]
public class AddLogMessages
{
    [Params(10, 100)]
    public int Count { get; set; }

    [GlobalSetup]
    public static void ClearCaches() => Caches.Clear();

    [Benchmark]
    public void MemoryCache() => AddLogMsg(Caches.Memory);

    [Benchmark(Baseline = true)]
    public void MySqlCache() => AddLogMsg(Caches.MySql);

    [Benchmark]
    public void PostgreSqlCache() => AddLogMsg(Caches.PostgreSql);

    [Benchmark]
    public void SqlServerCache() => AddLogMsg(Caches.SqlServer);

    private void AddLogMsg<TCache>(TCache cache)
        where TCache : ICache
    {
        var k = Guid.NewGuid().ToString();
        cache.AddSliding(k, k, LogMessage.GenerateRandomLogMessages(Count), TimeSpan.FromHours(1));
    }
}

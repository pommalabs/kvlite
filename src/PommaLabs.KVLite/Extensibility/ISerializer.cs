﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Represents an object that can serialize and deserialize other objects.
/// </summary>
public interface ISerializer
{
    /// <summary>
    ///   Deserializes the object contained into specified stream.
    /// </summary>
    /// <typeparam name="TObj">The type of the object.</typeparam>
    /// <param name="inputStream">The input stream.</param>
    /// <param name="compressed">Whether input stream was serialized with compression.</param>
    /// <returns>The deserialized object.</returns>
    TObj Deserialize<TObj>(Stream inputStream, bool compressed);

    /// <summary>
    ///   Serializes given object into specified stream.
    /// </summary>
    /// <typeparam name="TObj">The type of the object.</typeparam>
    /// <param name="outputStream">The output stream.</param>
    /// <param name="obj">The object.</param>
    /// <returns>True if compression was applied, false otherwise.</returns>
    bool Serialize<TObj>(Stream outputStream, TObj obj);
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.ComponentModel;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Abstracts the system clock to facilitate testing.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
public interface IClock
{
    /// <summary>
    ///   Retrieves the current system time in UTC.
    /// </summary>
    DateTimeOffset UtcNow { get; }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Represents an object that can intercept cache actions before and after they are executed.
/// </summary>
public interface IInterceptor
{
    /// <summary>
    ///   Invoked before an entry is added.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    public void EntryAdding(ref CacheKey partition, ref CacheKey key);

    /// <summary>
    ///   Invoked before an entry is gotten.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    public void EntryGetting(ref CacheKey partition, ref CacheKey key);
}

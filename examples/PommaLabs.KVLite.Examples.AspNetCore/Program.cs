﻿using PommaLabs.KVLite.AspNetCore;
using PommaLabs.KVLite.Extensibility;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

// Add services to the container.
services.AddRazorPages();

// System clock.
services.AddSingleton<IClock>(SystemClock.Instance);

// KVLite caching services.
services.AddKVLitePersistentSQLiteCache(s =>
{
    s.CacheFile = "AspNetCoreCache.sqlite";
    s.DefaultDistributedCacheAbsoluteExpiration = TimeSpan.FromSeconds(30);
});

// Session service, which will rely on KVLite distributed cache.
services.AddSession();

// Add custom health check.
services.AddHealthChecks()
    .AddCheck<KVLiteHealthCheck>(nameof(KVLiteHealthCheck));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseSession();

app.MapHealthChecks("/health");
app.MapRazorPages();

await app.RunAsync();

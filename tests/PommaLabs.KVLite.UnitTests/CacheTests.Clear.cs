﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;

namespace PommaLabs.KVLite.UnitTests;

internal partial class CacheTests
{
    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Clear_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(_cache.Clear(), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(), Is.EqualTo(0));
        Assert.That(_cache.LongCount(), Is.EqualTo(0L));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Clear_SinglePartition_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        var p = CacheKeys[0] + CacheKeys[1];

        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(p, CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }

        Assert.That(_cache.Clear(p), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(p), Is.EqualTo(0));
        Assert.That(_cache.LongCount(p), Is.EqualTo(0L));
        Assert.That(_cache.Count(), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(), Is.EqualTo(entryCount));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Clear_SinglePartitionWithConsiderExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        var p = CacheKeys[0] + CacheKeys[1];

        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(p, CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }

        Assert.That(_cache.Clear(p, CacheReadMode.ConsiderExpiryDate), Is.EqualTo(0));
        Assert.That(_cache.Count(p, CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(p, CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount * 2));
        Assert.That(_cache.LongCount(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount * 2));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Clear_WithConsiderExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(_cache.Clear(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(0));
        Assert.That(_cache.Count(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(entryCount));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Clear_WithIgnoreExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(_cache.Clear(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(), Is.EqualTo(0));
        Assert.That(_cache.LongCount(), Is.EqualTo(0L));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task ClearAsync_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(await _cache.ClearAsync(), Is.EqualTo(entryCount));
        Assert.That(await _cache.CountAsync(), Is.EqualTo(0));
        Assert.That(await _cache.LongCountAsync(), Is.EqualTo(0L));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task ClearAsync_SinglePartition_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        var p = CacheKeys[0] + CacheKeys[1];

        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(p, CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }

        Assert.That(await _cache.ClearAsync(p), Is.EqualTo(entryCount));
        Assert.That(await _cache.CountAsync(p), Is.EqualTo(0));
        Assert.That(await _cache.LongCountAsync(p), Is.EqualTo(0L));
        Assert.That(await _cache.CountAsync(), Is.EqualTo(entryCount));
        Assert.That(await _cache.LongCountAsync(), Is.EqualTo(entryCount));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task ClearAsync_SinglePartitionWithConsiderExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        var p = CacheKeys[0] + CacheKeys[1];

        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(p, CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }

        Assert.That(await _cache.ClearAsync(p, CacheReadMode.ConsiderExpiryDate), Is.EqualTo(0));
        Assert.That(await _cache.CountAsync(p), Is.EqualTo(entryCount));
        Assert.That(await _cache.LongCountAsync(p), Is.EqualTo(entryCount));
        Assert.That(await _cache.CountAsync(), Is.EqualTo(entryCount * 2));
        Assert.That(await _cache.LongCountAsync(), Is.EqualTo(entryCount * 2));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task ClearAsync_WithConsiderExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(await _cache.ClearAsync(CacheReadMode.ConsiderExpiryDate), Is.EqualTo(0));
        Assert.That(await _cache.CountAsync(), Is.EqualTo(entryCount));
        Assert.That(await _cache.LongCountAsync(), Is.EqualTo(entryCount));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task ClearAsync_WithIgnoreExpiryDateReadMode_ReturnsTheNumberOfEntriesRemoved(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        Assert.That(await _cache.ClearAsync(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(entryCount));
        Assert.That(await _cache.CountAsync(), Is.EqualTo(0));
        Assert.That(await _cache.LongCountAsync(), Is.EqualTo(0L));
    }
}

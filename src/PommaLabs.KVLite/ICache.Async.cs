﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PommaLabs.KVLite;

public partial interface ICache
{
    /// <summary>
    ///   Computes cache size in bytes. This value might be an estimate of real cache size and,
    ///   therefore, it does not need to be extremely accurate.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>An estimate of cache size in bytes.</returns>
    Task<long> GetCacheSizeInBytesAsync(CancellationToken cancellationToken = default);

    #region Add

    /// <summary>
    ///   Adds a "sliding" value with given partition and key. Value will last as much as
    ///   specified in given interval and, if accessed before expiry, its lifetime will be
    ///   extended by the interval itself.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="interval">The interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    Task AddSlidingAsync<TVal>(CacheKey partition, CacheKey key, TVal value, TimeSpan interval, IList<CacheKey> parentKeys = default, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Adds a "timed" value with given partition and key. Value will last until the specified
    ///   time and, if accessed before expiry, its lifetime will _not_ be extended.
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="utcExpiry">The UTC expiry.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    Task AddTimedAsync<TVal>(CacheKey partition, CacheKey key, TVal value, DateTimeOffset utcExpiry, IList<CacheKey> parentKeys = default, CancellationToken cancellationToken = default);

    #endregion Add

    #region Clear

    /// <summary>
    ///   Clears this instance, that is, it removes all stored entries.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries that have been removed.</returns>
    Task<long> ClearAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   Clears given partition, that is, it removes all its entries.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries that have been removed.</returns>
    Task<long> ClearAsync(CacheKey partition, CancellationToken cancellationToken = default);

    #endregion Clear

    #region Count

    /// <summary>
    ///   The number of entries stored in the cache.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries stored in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    Task<int> CountAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   The number of entries stored in given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries stored in given partition.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    Task<int> CountAsync(CacheKey partition, CancellationToken cancellationToken = default);

    /// <summary>
    ///   The number of entries stored in the cache.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries stored in the cache.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    Task<long> LongCountAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   The number of entries stored in given partition.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>The number of entries stored in given partition.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    Task<long> LongCountAsync(CacheKey partition, CancellationToken cancellationToken = default);

    #endregion Count

    #region Contains

    /// <summary>
    ///   Determines whether this cache contains the specified partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>Whether this cache contains the specified partition and key.</returns>
    /// <remarks>Calling this method does not extend sliding entries lifetime.</remarks>
    Task<bool> ContainsAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    #endregion Contains

    #region Get

    /// <summary>
    ///   Gets the value with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by the corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <returns>The value with specified partition and key.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    Task<CacheResult<TVal>> GetAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets all cache entries. If an entry is a "sliding" value, its lifetime will be
    ///   increased by corresponding interval.
    /// </summary>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    Task<IList<ICacheEntry<TVal>>> GetEntriesAsync<TVal>(CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets all cache entries in given partition. If an entry is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All cache entries in given partition.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    Task<IList<ICacheEntry<TVal>>> GetEntriesAsync<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets the cache entry with specified partition and key. If it is a "sliding" value, its
    ///   lifetime will be increased by corresponding interval.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected value.</typeparam>
    /// <returns>The cache entry with specified partition and key.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    Task<CacheResult<ICacheEntry<TVal>>> GetEntryAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    #endregion Get

    #region GetOrAdd

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "sliding" value with given partition and
    ///     key. Value will last as much as specified in given interval and, if accessed before
    ///     expiry, its lifetime will be extended by the interval itself.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="valueGetter">
    ///   The function that is called in order to get the value when it was not found inside the cache.
    /// </param>
    /// <param name="interval">The interval.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>
    ///   The value found in the cache or the one returned by <paramref name="valueGetter"/>, in
    ///   case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="valueGetter"/> is null.</exception>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    Task<TVal> GetOrAddSlidingAsync<TVal>(
        CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, TimeSpan interval,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   <para>
    ///     At first, it tries to get the cache entry with specified partition and key. If it is
    ///     a "sliding" value, its lifetime will be increased by corresponding interval.
    ///   </para>
    ///   <para>
    ///     If the value is not found, then it adds a "timed" value with given partition and
    ///     key. Value will last until the specified time and, if accessed before expiry, its
    ///     lifetime will _not_ be extended.
    ///   </para>
    /// </summary>
    /// <typeparam name="TVal">The type of the value.</typeparam>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="valueGetter">
    ///   The function that is called in order to get the value when it was not found inside the cache.
    /// </param>
    /// <param name="utcExpiry">The UTC expiry.</param>
    /// <param name="parentKeys">
    ///   Keys, belonging to current partition, on which the new entry will depend.
    /// </param>
    /// <param name="valueFilterer">
    ///   An optional predicate which can be specified in order to exclude some values from the
    ///   cache. For example, if null values should not be cached, then a predicate which
    ///   returns true for null values should be specified.
    /// </param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <returns>
    ///   The value found in the cache or the one returned by <paramref name="valueGetter"/>, in
    ///   case a new value has been added to the cache.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="valueGetter"/> is null.</exception>
    /// <exception cref="NotSupportedException">
    ///   Too many parent keys have been specified for this entry. Please have a look at the
    ///   <see cref="MaxParentKeyCountPerEntry"/> to understand how many parent keys each entry
    ///   may have.
    /// </exception>
    Task<TVal> GetOrAddTimedAsync<TVal>(
        CacheKey partition, CacheKey key, Func<Task<TVal>> valueGetter, DateTimeOffset utcExpiry,
        IList<CacheKey> parentKeys = default, Predicate<TVal> valueFilterer = default,
        CancellationToken cancellationToken = default);

    #endregion GetOrAdd

    #region Peek

    /// <summary>
    ///   Gets the value corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>
    ///   The value corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    Task<CacheResult<TVal>> PeekAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets the all values, without updating expiry dates.
    /// </summary>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All values, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    Task<IList<ICacheEntry<TVal>>> PeekEntriesAsync<TVal>(CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets the all entries in given partition, without updating expiry dates.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="queryOptions">Cache query options.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>All entries in given partition, without updating expiry dates.</returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    Task<IList<ICacheEntry<TVal>>> PeekEntriesAsync<TVal>(CacheKey partition, CacheQueryOptions queryOptions = default, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Gets the entry corresponding to given partition and key, without updating expiry date.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <typeparam name="TVal">The type of the expected values.</typeparam>
    /// <returns>
    ///   The entry corresponding to given partition and key, without updating expiry date.
    /// </returns>
    /// <remarks>
    ///   If you are uncertain of which type the value should have, you can always pass
    ///   <see cref="object"/> as type parameter; that will work whether the required value is a
    ///   class or not.
    /// </remarks>
    /// <exception cref="NotSupportedException">
    ///   Cache does not support peeking (please have a look at the <see cref="CanPeek"/> property).
    /// </exception>
    Task<CacheResult<ICacheEntry<TVal>>> PeekEntryAsync<TVal>(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    #endregion Peek

    #region Remove

    /// <summary>
    ///   Removes the entry with given partition and key.
    /// </summary>
    /// <param name="partition">The partition.</param>
    /// <param name="key">The key.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    Task RemoveAsync(CacheKey partition, CacheKey key, CancellationToken cancellationToken = default);

    #endregion Remove
}

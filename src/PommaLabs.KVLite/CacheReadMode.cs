﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite;

/// <summary>
///   Determines whether expired entries should be included in lists or in queries.
/// </summary>
public enum CacheReadMode
{
    /// <summary>
    ///   Considers expiry policy while retrieving entries.
    /// </summary>
    ConsiderExpiryDate = 0,

    /// <summary>
    ///   Ignores expiry policy while retrieving entries.
    /// </summary>
    IgnoreExpiryDate = 1
}

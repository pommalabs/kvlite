﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.Memory;

[TestFixture]
[Category(nameof(Memory))]
[NonParallelizable]
internal sealed class MemoryCacheTests : AbstractTests
{
    #region Default instance

    [Test]
    public void DefaultInstance_IsNotNull()
    {
        MemoryCache.DefaultInstance.ShouldNotBeNull();
    }

    #endregion Default instance
}

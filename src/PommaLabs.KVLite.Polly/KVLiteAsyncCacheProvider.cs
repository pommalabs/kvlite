﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading;
using System.Threading.Tasks;
using Polly.Caching;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.Polly;

/// <summary>
///   KVLite adapter for <see cref="IAsyncCacheProvider"/>.
/// </summary>
public sealed class KVLiteAsyncCacheProvider : IAsyncCacheProvider
{
    /// <summary>
    ///   Initializes cache provider.
    /// </summary>
    /// <param name="backingCache">Backing KVLite cache.</param>
    /// <param name="options">KVLite cache options.</param>
    public KVLiteAsyncCacheProvider(ICache backingCache, KVLiteCacheProviderOptions options)
    {
        BackingCache = backingCache ?? throw new ArgumentNullException(nameof(backingCache), ErrorMessages.NullCache);
        Options = options ?? throw new ArgumentNullException(nameof(options), ErrorMessages.NullSettings);
    }

    /// <summary>
    ///   Backing KVLite cache.
    /// </summary>
    public ICache BackingCache { get; }

    /// <summary>
    ///   KVLite cache provider options.
    /// </summary>
    public KVLiteCacheProviderOptions Options { get; }

    /// <summary>
    ///   Puts the specified value in the cache asynchronously.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="value">The value to put into the cache.</param>
    /// <param name="ttl">The time-to-live for the cache entry.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="continueOnCapturedContext">
    ///   Whether async calls should continue on a captured synchronization context.
    ///   <para>
    ///     <remarks>Note: if the underlying cache's async API does not support controlling
    ///     whether to continue on a captured context, async Policy executions with
    ///     <paramref name="continueOnCapturedContext"/> == true cannot be guaranteed to remain
    ///     on the captured context.</remarks>
    ///   </para>
    /// </param>
    /// <returns>A <see cref="Task"/> which completes when the value has been cached.</returns>
    public async Task PutAsync(string key, object value, Ttl ttl, CancellationToken cancellationToken, bool continueOnCapturedContext)
    {
        if (ttl.SlidingExpiration)
        {
            await BackingCache.AddSlidingAsync(Options.Partition, key, value, ttl.Timespan, null, cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }
        else
        {
            await BackingCache.AddTimedAsync(Options.Partition, key, value, ttl.Timespan, null, cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }
    }

    /// <summary>
    ///   Tries to get a value from the cache asynchronously.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="continueOnCapturedContext">
    ///   Whether async calls should continue on a captured synchronization context.
    ///   <para>
    ///     <remarks>Note: if the underlying cache's async API does not support controlling
    ///     whether to continue on a captured context, async Policy executions with
    ///     <paramref name="continueOnCapturedContext"/> == true cannot be guaranteed to remain
    ///     on the captured context.</remarks>
    ///   </para>
    /// </param>
    /// <returns>
    ///   A task promising as Result the value from cache, or null, if none was found.
    /// </returns>
    public async Task<(bool, object)> TryGetAsync(string key, CancellationToken cancellationToken, bool continueOnCapturedContext)
    {
        var cacheResult = await BackingCache
            .GetAsync<object>(Options.Partition, key, cancellationToken)
            .ConfigureAwait(continueOnCapturedContext);

        return (cacheResult.HasValue, cacheResult.ValueOrDefault());
    }
}

/// <summary>
///   KVLite adapter for <see cref="IAsyncCacheProvider{TResult}"/>.
/// </summary>
/// <typeparam name="TResult">The result type.</typeparam>
public sealed class KVLiteAsyncCacheProvider<TResult> : IAsyncCacheProvider<TResult>
{
    /// <summary>
    ///   Initializes cache provider.
    /// </summary>
    /// <param name="backingCache">Backing KVLite cache.</param>
    /// <param name="options">KVLite cache options.</param>
    public KVLiteAsyncCacheProvider(ICache backingCache, KVLiteCacheProviderOptions options)
    {
        BackingCache = backingCache ?? throw new ArgumentNullException(nameof(backingCache), ErrorMessages.NullCache);
        Options = options ?? throw new ArgumentNullException(nameof(options), ErrorMessages.NullSettings);
    }

    /// <summary>
    ///   Backing KVLite cache.
    /// </summary>
    public ICache BackingCache { get; }

    /// <summary>
    ///   KVLite cache provider options.
    /// </summary>
    public KVLiteCacheProviderOptions Options { get; }

    /// <summary>
    ///   Puts the specified value in the cache asynchronously.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="value">The value to put into the cache.</param>
    /// <param name="ttl">The time-to-live for the cache entry.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="continueOnCapturedContext">
    ///   Whether async calls should continue on a captured synchronization context.
    ///   <para>
    ///     <remarks>Note: if the underlying cache's async API does not support controlling
    ///     whether to continue on a captured context, async Policy executions with
    ///     <paramref name="continueOnCapturedContext"/> == true cannot be guaranteed to remain
    ///     on the captured context.</remarks>
    ///   </para>
    /// </param>
    /// <returns>A <see cref="Task"/> which completes when the value has been cached.</returns>
    public async Task PutAsync(string key, TResult value, Ttl ttl, CancellationToken cancellationToken, bool continueOnCapturedContext)
    {
        if (ttl.SlidingExpiration)
        {
            await BackingCache.AddSlidingAsync(Options.Partition, key, value, ttl.Timespan, null, cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }
        else
        {
            await BackingCache.AddTimedAsync(Options.Partition, key, value, ttl.Timespan, null, cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }
    }

    /// <summary>
    ///   Tries to get a value from the cache asynchronously.
    /// </summary>
    /// <param name="key">The cache key.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="continueOnCapturedContext">
    ///   Whether async calls should continue on a captured synchronization context.
    ///   <para>
    ///     <remarks>Note: if the underlying cache's async API does not support controlling
    ///     whether to continue on a captured context, async Policy executions with
    ///     <paramref name="continueOnCapturedContext"/> == true cannot be guaranteed to remain
    ///     on the captured context.</remarks>
    ///   </para>
    /// </param>
    /// <returns>
    ///   A task promising as Result the value from cache, or null, if none was found.
    /// </returns>
    public async Task<(bool, TResult)> TryGetAsync(string key, CancellationToken cancellationToken, bool continueOnCapturedContext)
    {
        var cacheResult = await BackingCache
            .GetAsync<TResult>(Options.Partition, key, cancellationToken)
            .ConfigureAwait(continueOnCapturedContext);

        return (cacheResult.HasValue, cacheResult.ValueOrDefault());
    }
}

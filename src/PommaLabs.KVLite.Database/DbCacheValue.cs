﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;

namespace PommaLabs.KVLite.Database;

/// <summary>
///   Represents a flat value stored inside the cache.
/// </summary>
public class DbCacheValue
{
    /// <summary>
    ///   Database agnostic "false".
    /// </summary>
    public const byte False = 0;

    /// <summary>
    ///   Database agnostic "true".
    /// </summary>
    public const byte True = 1;

    /// <summary>
    ///   Whether the entry content was compressed or not.
    /// </summary>
    public byte Compressed { get; set; }

    /// <summary>
    ///   Hash of partition and key.
    /// </summary>
    public long Hash { get; set; }

    /// <summary>
    ///   How many seconds should be used to extend expiry time when the entry is retrieved.
    /// </summary>
    public long Interval { get; set; }

    /// <summary>
    ///   When the entry will expire, expressed as seconds after UNIX epoch.
    /// </summary>
    public long UtcExpiry { get; set; }

    /// <summary>
    ///   Serialized and optionally compressed content of this entry.
    /// </summary>
    public MemoryStream Value { get; set; }
}

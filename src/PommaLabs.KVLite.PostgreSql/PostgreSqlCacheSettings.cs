﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.Serialization;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.PostgreSql;

/// <summary>
///   Settings used by <see cref="PostgreSqlCache"/>.
/// </summary>
[Serializable, DataContract]
public sealed class PostgreSqlCacheSettings : DbCacheSettings<PostgreSqlCacheSettings>;

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Ninject;
using NUnit.Framework;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Goodies;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.SqlServer;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

[TestFixture(typeof(MemoryCache))]
[TestFixture(typeof(MySqlCache))]
[TestFixture(typeof(PersistentCache))]
[TestFixture(typeof(PostgreSqlCache))]
[TestFixture(typeof(SqlServerCache))]
[TestFixture(typeof(VolatileCache))]
[Parallelizable(ParallelScope.Fixtures)]
[SuppressMessage("Structure", "NUnit1032:An IDisposable field/property should be Disposed in a TearDown method", Justification = "Cache is not disposed because it is reused between tests")]
internal sealed partial class CacheTests(Type cacheType) : AbstractTests
{
    #region Setup/Teardown

    private ICacheInternal _cache;

    [SetUp]
    public void SetUp()
    {
        _cache = Kernel.Get(cacheType) as ICacheInternal;
        _cache.Clear();
    }

    [TearDown]
    public void TearDown()
    {
        if (_cache?.Disposed == false)
        {
            _cache.Clear();
        }
        _cache = null;
    }

    #endregion Setup/Teardown

    #region Caching enumerable

    [TestCase(0)]
    [TestCase(1)]
    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void CachingEnumerable_ResultShouldBeTheSameAsTheInput(int entryCount)
    {
        var input = StringValues.Take(entryCount);
        var result = new CachingEnumerable<StringValue>(_cache, input);

        result.SequenceEqual(input).ShouldBeTrue();
    }

    #endregion Caching enumerable

    #region Tuple serialization

    [Test]
    public void AddSliding_TupleType()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var v3 = StringValues[2];
        var i = TenMinutes;

        _cache.AddSliding(p, k, Tuple.Create(v1, v2, v3), i);
        var info = _cache.GetEntry<Tuple<StringValue, StringValue, StringValue>>(p, k).Value;

        info.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.Value.Item3.ShouldBe(v3);
    }

    #endregion Tuple serialization

    #region Parent keys management

    [Test]
    public void AddSliding_TooManyParentKeys()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v = StringValues[0];
        var t = new CacheKey[_cache.MaxParentKeyCountPerEntry + 1];
        Should.Throw<NotSupportedException>(() => { _cache.AddSliding(p, k, v, TimeSpan.FromDays(1), t); });
    }

    [Test]
    public void AddTimed_TooManyParentKeys()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v = StringValues[0];
        var t = new CacheKey[_cache.MaxParentKeyCountPerEntry + 1];
        Should.Throw<NotSupportedException>(() => { _cache.AddTimed(p, k, v, TimeSpan.FromDays(1), t); });
    }

    [Test]
    public void GetOrAddSliding_TooManyParentKeys()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v = StringValues[0];
        var t = new CacheKey[_cache.MaxParentKeyCountPerEntry + 1];
        Should.Throw<NotSupportedException>(() => { _cache.GetOrAddSliding(p, k, () => v, TimeSpan.FromDays(1), t); });
    }

    [Test]
    public void GetOrAddTimed_TooManyParentKeys()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v = StringValues[0];
        var t = new CacheKey[_cache.MaxParentKeyCountPerEntry + 1];
        Should.Throw<NotSupportedException>(() => { _cache.GetOrAddTimed(p, k, () => v, TimeSpan.FromDays(1), t); });
    }

    #endregion Parent keys management

    [Test]
    public void AddSliding_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.AddSliding(p, k, Tuple.Create(v1, v2), i);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void AddSliding_RightInfo_WithParentKey()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var t = CacheKeys[2];
        var i = TenMinutes;

        _cache.AddSliding(p, t, v1, i);
        _cache.AddSliding(p, k, Tuple.Create(v1, v2), i, [t]);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
        info.ParentKeys.Count().ShouldBe(1);
        info.ParentKeys.ShouldContain(t);
    }

    [Test]
    public void AddSliding_TwoTimes_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.AddSliding(p, k, Tuple.Create(v1, v2), i);
        _cache.AddSliding(p, k, Tuple.Create(v1, v2), i);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void AddSliding_TwoTimes_RightInfo_DifferentValue()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var i = TenMinutes;

        _cache.AddSliding(p, k, Tuple.Create(v1, v2), i);
        _cache.AddSliding(p, k, Tuple.Create(v2, v1), i);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.Item1.ShouldBe(v2);
        info.Value.Item2.ShouldBe(v1);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(i);
    }

    [Test]
    public void AddTimed_HugeValue()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v = new byte[20000];

        _cache.AddTimed(p, k, v, _cache.Clock.UtcNow + TenMinutes);

        var info = _cache.GetEntry<byte[]>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldBe(v);
        info.UtcExpiry.ShouldNotBe(default);
        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void AddTimed_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.AddTimed(p, k, Tuple.Create(v1, v2), e);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void AddTimed_RightInfo_WithParentKey()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var t = CacheKeys[2];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.AddTimed(p, t, v1, e);
        _cache.AddTimed(p, k, Tuple.Create(v1, v2), e, [t]);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);

        info.ParentKeys.Count().ShouldBe(1);
        info.ParentKeys.ShouldContain(t);
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void AddTimed_TwoTimes(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Add(TenMinutes));
            Assert.That(_cache.Contains(CacheKeys[i], CacheKeys[i]), Is.True);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Add(TenMinutes));
            Assert.That(_cache.Contains(CacheKeys[i], CacheKeys[i]), Is.True);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void AddTimed_TwoTimes_CheckEntries(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Add(TenMinutes));
            Assert.That(_cache.Contains(CacheKeys[i], CacheKeys[i]), Is.True);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Add(TenMinutes));
            Assert.That(_cache.Contains(CacheKeys[i], CacheKeys[i]), Is.True);
        }
        var entries = _cache.GetEntries<StringValue>();
        for (var i = 0; i < entryCount; ++i)
        {
            var k = CacheKeys[i];
            var s = StringValues[i];
            Assert.That(entries.Count(x => x.Key == k && x.Value.Equals(s)), Is.EqualTo(1));
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void AddTimed_TwoTimes_Parallel(int entryCount)
    {
        var tasks = new List<Task>();
        for (var i = 0; i < entryCount; ++i)
        {
            var l = i;
            var task = Task.Factory.StartNew(() =>
            {
                _cache.AddTimed(CacheKeys[l], CacheKeys[l], StringValues[l], _cache.Clock.UtcNow.Subtract(TenMinutes));
                _cache.Contains(CacheKeys[l], CacheKeys[l]).ShouldBeFalse();
            });
            tasks.Add(task);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            var l = i;
            var task = Task.Factory.StartNew(() =>
            {
                _cache.AddTimed(CacheKeys[l], CacheKeys[l], StringValues[l], _cache.Clock.UtcNow.Subtract(TenMinutes));
                _cache.Contains(CacheKeys[l], CacheKeys[l]).ShouldBeFalse();
            });
            tasks.Add(task);
        }
        foreach (var task in tasks)
        {
            task.Wait();
        }
    }

    [Test]
    public void AddTimed_TwoTimes_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var e = _cache.Clock.UtcNow + TenMinutes;

        _cache.AddTimed(p, k, Tuple.Create(v1, v2), e);
        _cache.AddTimed(p, k, Tuple.Create(v1, v2), e);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void AddTimed_TwoValues_SameKey()
    {
        _cache.AddTimed(CacheKeys[0], CacheKeys[1], StringValues[0], _cache.Clock.UtcNow + TenMinutes);
        Assert.That(_cache.Get<StringValue>(CacheKeys[0], CacheKeys[1]).Value, Is.EqualTo(StringValues[0]));
        _cache.AddTimed(CacheKeys[0], CacheKeys[1], StringValues[1], _cache.Clock.UtcNow + TenMinutes);
        Assert.That(_cache.Get<StringValue>(CacheKeys[0], CacheKeys[1]).Value, Is.EqualTo(StringValues[1]));
    }

    [Test]
    public void AddTimed_WithDuration_RightInfo()
    {
        var p = CacheKeys[0];
        var k = CacheKeys[1];
        var v1 = StringValues[0];
        var v2 = StringValues[1];
        var l = TenMinutes;

        _cache.AddTimed(p, k, Tuple.Create(v1, v2), l);

        var info = _cache.GetEntry<Tuple<StringValue, StringValue>>(p, k).Value;
        info.ShouldNotBeNull();
        info.Partition.ShouldBe(p);
        info.Key.ShouldBe(k);
        info.Value.ShouldNotBeNull();
        info.Value.Item1.ShouldBe(v1);
        info.Value.Item2.ShouldBe(v2);

        var e = _cache.Clock.UtcNow.Add(l);
        info.UtcExpiry.ShouldNotBe(default);
        info.UtcExpiry.Date.ShouldBe(e.Date);
        info.UtcExpiry.Hour.ShouldBe(e.Hour);
        info.UtcExpiry.Minute.ShouldBe(e.Minute);
        info.UtcExpiry.Second.ShouldBe(e.Second);

        info.Interval.ShouldBe(TimeSpan.Zero);
    }

    [Test]
    public void Clean_AfterFixedNumberOfInserts_InvalidValues()
    {
        const int FixedValue = LargeEntryCount;
        for (var i = 0; i < FixedValue; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Subtract(TenMinutes));
        }
        Assert.That(_cache.Count(), Is.EqualTo(0));
    }

    [Test]
    public void Clean_AfterFixedNumberOfInserts_InvalidValues_Async()
    {
        const int FixedValue = LargeEntryCount;
        Parallel.For(0, FixedValue, new ParallelOptions { MaxDegreeOfParallelism = 2 }, i =>
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Subtract(TenMinutes));
        });
        Assert.That(_cache.Count(), Is.EqualTo(0));
    }

    [Test]
    public void Clean_AfterFixedNumberOfInserts_ValidValues()
    {
        const int FixedValue = LargeEntryCount;
        for (var i = 0; i < FixedValue - 1; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        }
        Assert.That(_cache.Count(), Is.EqualTo(FixedValue - 1));
        Assert.That(_cache.Count(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(FixedValue - 1));

        // Advance the clock, in order to make entries not valid.
        (_cache.Clock as FakeClock).Advance(TimeSpan.FromMinutes(15));

        // Add a new entry, and then trigger a soft cleanup.
        _cache.AddTimed(CacheKeys[0], CacheKeys[0], StringValues[0], _cache.Clock.UtcNow + TenMinutes);
        _cache.Clear(CacheReadMode.ConsiderExpiryDate);

        Assert.That(_cache.Count(), Is.EqualTo(1));
        Assert.That(_cache.Count(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(1));
    }

    [Test]
    public void Clean_AfterFixedNumberOfInserts_ValidValues_Async()
    {
        const int FixedValue = LargeEntryCount;
        Parallel.For(0, FixedValue - 1, new ParallelOptions { MaxDegreeOfParallelism = 2 }, i =>
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow + TenMinutes);
        });
        Assert.That(_cache.Count(), Is.EqualTo(FixedValue - 1));
        Assert.That(_cache.Count(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(FixedValue - 1));

        // Advance the clock, in order to make entries not valid.
        (_cache.Clock as FakeClock).Advance(TimeSpan.FromMinutes(15));

        // Add a new entry, and then trigger a soft cleanup.
        _cache.AddTimed(CacheKeys[0], CacheKeys[0], StringValues[0], _cache.Clock.UtcNow + TenMinutes);
        _cache.Clear(CacheReadMode.ConsiderExpiryDate);

        Assert.That(_cache.Count(), Is.EqualTo(1));
        Assert.That(_cache.Count(CacheReadMode.IgnoreExpiryDate), Is.EqualTo(1));
    }

    [Test]
    public void Count_EmptyCache()
    {
        Assert.That(_cache.Count(), Is.EqualTo(0));
        Assert.That(_cache.Count(CacheKeys[0]), Is.EqualTo(0));
        Assert.That(_cache.LongCount(), Is.EqualTo(0));
        Assert.That(_cache.LongCount(CacheKeys[0]), Is.EqualTo(0));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Count_FullCache_OnePartition(int entryCount)
    {
        var partition = CacheKeys[0];
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(partition, CacheKeys[i], StringValues[i], TenMinutes);
        }
        Assert.That(_cache.Count(), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(partition), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(partition), Is.EqualTo(entryCount));
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Count_FullCache_TwoPartitions(int entryCount)
    {
        int i;
        var partition1 = CacheKeys[0];
        var p1Count = entryCount / 3;
        for (i = 0; i < entryCount / 3; ++i)
        {
            _cache.AddTimed(partition1, CacheKeys[i], StringValues[i], TenMinutes);
        }
        var partition2 = CacheKeys[1];
        for (; i < entryCount; ++i)
        {
            _cache.AddTimed(partition2, CacheKeys[i], StringValues[i], TenMinutes);
        }
        Assert.That(_cache.Count(), Is.EqualTo(entryCount));
        Assert.That(_cache.Count(partition1), Is.EqualTo(p1Count));
        Assert.That(_cache.Count(partition2), Is.EqualTo(entryCount - p1Count));
        Assert.That(_cache.LongCount(), Is.EqualTo(entryCount));
        Assert.That(_cache.LongCount(partition1), Is.EqualTo(p1Count));
        Assert.That(_cache.LongCount(partition2), Is.EqualTo(entryCount - p1Count));
    }

    #region BCL Collections

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Collections_Dictionary(int entryCount)
    {
        var p = CacheKeys[0];
        var l = Enumerable.Range(1, entryCount).ToDictionary(i => i, i => i.ToString(CultureInfo.InvariantCulture));
        _cache.AddSliding(p, "dict", l, TenMinutes);
        var lc = _cache.Get<Dictionary<int, string>>(p, "dict").Value;
        l.ShouldBe(lc);
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Collections_HashSet(int entryCount)
    {
        var p = CacheKeys[0];
        var l = new HashSet<int>(Enumerable.Range(1, entryCount));
        _cache.AddSliding(p, "set", l, TenMinutes);
        var lc = _cache.Get<HashSet<int>>(p, "set").Value;
        l.ShouldBe(lc);
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Collections_List(int entryCount)
    {
        var p = CacheKeys[0];
        var l = new List<int>(Enumerable.Range(1, entryCount));
        _cache.AddSliding(p, "list", l, TenMinutes);
        var lc = _cache.Get<List<int>>(p, "list").Value;
        l.ShouldBe(lc);
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Collections_SortedList(int entryCount)
    {
        var p = CacheKeys[0];
        var l = new SortedList<int, string>(Enumerable.Range(1, entryCount).ToDictionary(i => i, i => i.ToString(CultureInfo.InvariantCulture)));
        _cache.AddSliding(p, "list", l, TenMinutes);
        var lc = _cache.Get<SortedList<int, string>>(p, "list").Value;
        l.ShouldBe(lc);
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Collections_SortedSet(int entryCount)
    {
        var p = CacheKeys[0];
        var l = new SortedSet<int>(Enumerable.Range(1, entryCount));
        _cache.AddSliding(p, "set", l, TenMinutes);
        var lc = _cache.Get<SortedSet<int>>(p, "set").Value;
        l.ShouldBe(lc);
    }

    #endregion BCL Collections

    #region Private Methods

    private static void AddSliding(ICache instance, int entryCount, TimeSpan interval)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            instance.AddSliding(CacheKeys[i], CacheKeys[i], StringValues[i], interval);
        }
    }

    private static void AddTimed(ICache instance, int entryCount, DateTimeOffset utcTime)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            instance.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], utcTime);
        }
    }

    #endregion Private Methods
}

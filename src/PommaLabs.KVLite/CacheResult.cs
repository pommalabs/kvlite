﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

/// <summary>
///   The result of a cache read operation.
/// </summary>
/// <typeparam name="T">The requested type.</typeparam>
public readonly struct CacheResult<T> : IEquatable<CacheResult<T>>
{
    /// <summary>
    ///   Backing field for <see cref="Value"/>.
    /// </summary>
    private readonly T _value;

    /// <summary>
    ///   Builds a cache result.
    /// </summary>
    /// <param name="value">Result value.</param>
    private CacheResult(T value)
    {
        HasValue = true;
        _value = value;
    }

    /// <summary>
    ///   True if result has a value, false otherwise.
    /// </summary>
    public bool HasValue { get; }

    /// <summary>
    ///   Result value.
    /// </summary>
    /// <exception cref="InvalidOperationException">Result has no value.</exception>
    public T Value
    {
        get
        {
            // Preconditions
            if (!HasValue) throw new InvalidOperationException(ErrorMessages.EmptyCacheResult);

            return _value;
        }
    }

    /// <summary>
    ///   Creates a result from given value.
    /// </summary>
    /// <param name="value">The value.</param>
    public static implicit operator CacheResult<T>(T value) => new(value);

    /// <summary>
    ///   Compares two objects.
    /// </summary>
    /// <param name="x">Left.</param>
    /// <param name="y">Right.</param>
    /// <returns>True if they are not equal, false otherwise.</returns>
    public static bool operator !=(CacheResult<T> x, CacheResult<T> y) => !x.Equals(y);

    /// <summary>
    ///   Compares two objects.
    /// </summary>
    /// <param name="x">Left.</param>
    /// <param name="y">Right.</param>
    /// <returns>True if they are equal, false otherwise.</returns>
    public static bool operator ==(CacheResult<T> x, CacheResult<T> y) => x.Equals(y);

    /// <summary>
    ///   Returns a tuple with <see cref="HasValue"/> and <see cref="Value"/> properties. This
    ///   method does not throw any exception if <see cref="HasValue"/> is false.
    /// </summary>
    /// <param name="hasValue">True if result has a value, false otherwise.</param>
    /// <param name="value">Result value.</param>
    public void Deconstruct(out bool hasValue, out T value)
    {
        hasValue = HasValue;
        value = _value;
    }

    /// <summary>
    ///   Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>
    ///   true if the current object is equal to the <paramref name="other">other</paramref>
    ///   parameter; otherwise, false.
    /// </returns>
    public bool Equals(CacheResult<T> other) => EqualityComparer<T>.Default.Equals(_value, other._value) && EqualityComparer<bool>.Default.Equals(HasValue, other.HasValue);

    /// <summary>
    ///   Indicates whether this instance and a specified object are equal.
    /// </summary>
    /// <param name="obj">The object to compare with the current instance.</param>
    /// <returns>
    ///   true if <paramref name="obj">obj</paramref> and this instance are the same type and
    ///   represent the same value; otherwise, false.
    /// </returns>
    public override bool Equals(object obj) => (obj is CacheResult<T> result) && Equals(result);

    /// <summary>
    ///   Returns the hash code for this instance.
    /// </summary>
    /// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
    public override int GetHashCode()
    {
        unchecked
        {
            const int Prime = -1521134295;
            var hash = 12345701;
            hash = (hash * Prime) + EqualityComparer<T>.Default.GetHashCode(_value);
            hash = (hash * Prime) + EqualityComparer<bool>.Default.GetHashCode(HasValue);
            return hash;
        }
    }

    /// <summary>
    ///   Result value if it exists, otherwise it returns default value for given type.
    /// </summary>
    /// <returns>
    ///   Result value if it exists, otherwise it returns default value for given type.
    /// </returns>
    public T ValueOrDefault() => HasValue ? _value : default;
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Database;

/// <summary>
///   Represents a flat entry stored inside the cache.
/// </summary>
public sealed partial class DbCacheEntry : DbCacheValue
{
    /// <summary>
    ///   A key uniquely identifies an entry inside a partition.
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    ///   Optional parent entry hash, used to link entries in a hierarchical way.
    /// </summary>
    public long? ParentHash0 { get; set; }

    /// <summary>
    ///   Optional parent entry hash, used to link entries in a hierarchical way.
    /// </summary>
    public long? ParentHash1 { get; set; }

    /// <summary>
    ///   Optional parent entry hash, used to link entries in a hierarchical way.
    /// </summary>
    public long? ParentHash2 { get; set; }

    /// <summary>
    ///   Optional parent entry key, used to link entries in a hierarchical way.
    /// </summary>
    public string ParentKey0 { get; set; }

    /// <summary>
    ///   Optional parent entry key, used to link entries in a hierarchical way.
    /// </summary>
    public string ParentKey1 { get; set; }

    /// <summary>
    ///   Optional parent entry key, used to link entries in a hierarchical way.
    /// </summary>
    public string ParentKey2 { get; set; }

    /// <summary>
    ///   A partition holds a group of related keys.
    /// </summary>
    public string Partition { get; set; }

    /// <summary>
    ///   When the entry was created, expressed as seconds after UNIX epoch.
    /// </summary>
    public long UtcCreation { get; set; }
}

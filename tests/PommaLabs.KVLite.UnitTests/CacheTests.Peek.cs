﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests;

internal partial class CacheTests
{
    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Peek_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Peek<object>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Peek_EmptyCache_Parallel(int entryCount)
    {
        var tasks = new List<Task<CacheResult<StringValue>>>();
        for (var i = 0; i < entryCount; ++i)
        {
            var l = i;
            var task = Task.Run(() => _cache.Peek<StringValue>(CacheKeys[l], CacheKeys[l]));
            tasks.Add(task);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(tasks[i].Result.HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Peek_FullCache_ExpiryNotChanged(int entryCount)
    {
        var expiryDate = _cache.Clock.UtcNow + TenMinutes;
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], expiryDate);
        }
        for (var i = 0; i < entryCount; ++i)
        {
            var value = _cache.Peek<StringValue>(CacheKeys[i], CacheKeys[i]);
            Assert.That(value.Value, Is.EqualTo(StringValues[i]));
            var entry = _cache.PeekEntry<StringValue>(CacheKeys[i], CacheKeys[i]).Value;
            Assert.That(entry.UtcExpiry.Date, Is.EqualTo(expiryDate.Date));
            Assert.That(entry.UtcExpiry.Hour, Is.EqualTo(expiryDate.Hour));
            Assert.That(entry.UtcExpiry.Minute, Is.EqualTo(expiryDate.Minute));
            Assert.That(entry.UtcExpiry.Second, Is.EqualTo(expiryDate.Second));
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Peek_FullCache_Outdated(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], _cache.Clock.UtcNow.Subtract(TenMinutes));
        }
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Peek<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void Peek_Typed_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.Peek<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void PeekEntries_Pagination_NoPartition(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = _cache.PeekEntries<StringValue>(new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = _cache.PeekEntries<StringValue>(new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void PeekEntries_Pagination_WithPartition(int entryCount)
    {
        var p = CacheKeys[0];

        for (var i = 0; i < entryCount; ++i)
        {
            _cache.AddTimed(p, CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = _cache.PeekEntries<StringValue>(p, new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = _cache.PeekEntries<StringValue>(p, new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task PeekEntriesAsync_Pagination_NoPartition(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(CacheKeys[i], CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = await _cache.PeekEntriesAsync<StringValue>(new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = await _cache.PeekEntriesAsync<StringValue>(new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public async Task PeekEntriesAsync_Pagination_WithPartition(int entryCount)
    {
        var p = CacheKeys[0];

        for (var i = 0; i < entryCount; ++i)
        {
            await _cache.AddTimedAsync(p, CacheKeys[i], StringValues[i], TenMinutes);
        }

        var halfEntryCount = entryCount / 2;

        var firstHalf = await _cache.PeekEntriesAsync<StringValue>(p, new CacheQueryOptions
        {
            Skip = 0,
            Take = halfEntryCount
        });

        firstHalf.Count.ShouldBe(halfEntryCount);

        var secondHalf = await _cache.PeekEntriesAsync<StringValue>(p, new CacheQueryOptions
        {
            Skip = halfEntryCount,
            Take = halfEntryCount
        });

        secondHalf.Count.ShouldBe(halfEntryCount);

        firstHalf.Intersect(secondHalf).ShouldBeEmpty();
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void PeekEntry_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.PeekEntry<object>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }

    [TestCase(SmallEntryCount)]
    [TestCase(MediumEntryCount)]
    [TestCase(LargeEntryCount)]
    public void PeekEntry_Typed_EmptyCache(int entryCount)
    {
        for (var i = 0; i < entryCount; ++i)
        {
            Assert.That(_cache.PeekEntry<StringValue>(CacheKeys[i], CacheKeys[i]).HasValue, Is.False);
        }
    }
}

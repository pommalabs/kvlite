﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Microsoft.Extensions.Caching.Distributed;
using PommaLabs.KVLite;
using PommaLabs.KVLite.SqlServer;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for SQL Server KVLite services.
/// </summary>
public static class SqlServerKVLiteServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="SqlServerCache"/> as singleton implementation for <see
    ///   cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteSqlServerCache(this IServiceCollection services) => services.AddKVLiteSqlServerCache(null);

    /// <summary>
    ///   Registers <see cref="SqlServerCache"/> as singleton implementation for <see
    ///   cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeSettings">Can be used to customize settings.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteSqlServerCache(this IServiceCollection services, Action<SqlServerCacheSettings> changeSettings)
    {
        var settings = new SqlServerCacheSettings();
        changeSettings?.Invoke(settings);

        return services.AddKVLiteCache<SqlServerCache, SqlServerCacheSettings>(
            (serializer, clock, logger, interceptor, random) =>
            new SqlServerCache(settings, serializer, clock, logger, interceptor, random));
    }
}

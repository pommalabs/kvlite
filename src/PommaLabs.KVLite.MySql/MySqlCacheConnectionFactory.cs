﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.MySql;

/// <summary>
///   Cache connection factory specialized for MySQL.
/// </summary>
public sealed class MySqlCacheConnectionFactory : DbCacheConnectionFactory<MySqlCacheSettings, MySqlConnection, MySqlTransaction, MySqlCommand>
{
    /// <summary>
    ///   Cache connection factory specialized for MySQL.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    public MySqlCacheConnectionFactory(MySqlCacheSettings settings, ILogger<MySqlCache> logger)
        : base(settings, logger)
    {
        OnSettingsPropertyChanged(null, null);
    }

    /// <summary>
    ///   The symbol used to enclose an identifier (left side).
    /// </summary>
    protected override string LeftIdentifierEncloser { get; } = "`";

    /// <summary>
    ///   The symbol used to enclose an identifier (right side).
    /// </summary>
    protected override string RightIdentifierEncloser { get; } = "`";

    /// <summary>
    ///   Ensures cache schema is ready.
    /// </summary>
    protected override void EnsureSchemaIsReady()
    {
        using var db = OpenConnection();
        using var cm = db.CreateCommand();
        cm.CommandText = CreateCacheSchemaCommand;
        cm.ExecuteNonQuery();
    }

    /// <summary>
    ///   This method is called when either the cache schema name or the cache entries table
    ///   name have been changed by the user.
    /// </summary>
    protected override void UpdateCommandsAndQueries()
    {
        base.UpdateCommandsAndQueries();

        var p = ParameterPrefix;
        var s = SqlSchemaWithDot;

        #region Commands

        InsertOrUpdateCacheEntryCommand = MinifyQuery($@"
                replace into {s}{Settings.CacheEntriesTableName} (
                    {Settings.HashColumnName},
                    {Settings.UtcExpiryColumnName},
                    {Settings.IntervalColumnName},
                    {Settings.ValueColumnName},
                    {Settings.CompressedColumnName},
                    {Settings.PartitionColumnName},
                    {Settings.KeyColumnName},
                    {Settings.UtcCreationColumnName},
                    {Settings.ParentHash0ColumnName},
                    {Settings.ParentKey0ColumnName},
                    {Settings.ParentHash1ColumnName},
                    {Settings.ParentKey1ColumnName},
                    {Settings.ParentHash2ColumnName},
                    {Settings.ParentKey2ColumnName}
                )
                values (
                    {p}{nameof(DbCacheValue.Hash)},
                    {p}{nameof(DbCacheValue.UtcExpiry)},
                    {p}{nameof(DbCacheValue.Interval)},
                    {p}{nameof(DbCacheValue.Value)},
                    {p}{nameof(DbCacheValue.Compressed)},
                    {p}{nameof(DbCacheEntry.Partition)},
                    {p}{nameof(DbCacheEntry.Key)},
                    {p}{nameof(DbCacheEntry.UtcCreation)},
                    {p}{nameof(DbCacheEntry.ParentHash0)},
                    {p}{nameof(DbCacheEntry.ParentKey0)},
                    {p}{nameof(DbCacheEntry.ParentHash1)},
                    {p}{nameof(DbCacheEntry.ParentKey1)},
                    {p}{nameof(DbCacheEntry.ParentHash2)},
                    {p}{nameof(DbCacheEntry.ParentKey2)}
                );
            ");

        CreateCacheSchemaCommand = MinifyQuery($@"
                CREATE TABLE IF NOT EXISTS {s}{Settings.CacheEntriesTableName} (
                    {Settings.IdColumnName} BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Automatically generated ID.',
                    {Settings.HashColumnName} BIGINT(20) SIGNED NOT NULL COMMENT 'Hash of partition and key.',
                    {Settings.UtcExpiryColumnName} BIGINT(20) UNSIGNED NOT NULL COMMENT 'When the entry will expire, expressed as seconds after UNIX epoch.',
                    {Settings.IntervalColumnName} BIGINT(20) UNSIGNED NOT NULL COMMENT 'How many seconds should be used to extend expiry time when the entry is retrieved.',
                    {Settings.ValueColumnName} MEDIUMBLOB NOT NULL COMMENT 'Serialized and optionally compressed content of this entry.',
                    {Settings.CompressedColumnName} TINYINT(1) UNSIGNED NOT NULL COMMENT 'Whether the entry content was compressed or not.',
                    {Settings.PartitionColumnName} VARCHAR(2000) NOT NULL COMMENT 'A partition holds a group of related keys.',
                    {Settings.KeyColumnName} VARCHAR(2000) NOT NULL COMMENT 'A key uniquely identifies an entry inside a partition.',
                    {Settings.UtcCreationColumnName} BIGINT(20) UNSIGNED NOT NULL COMMENT 'When the entry was created, expressed as seconds after UNIX epoch.',
                    {Settings.ParentHash0ColumnName} BIGINT(20) SIGNED DEFAULT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
                    {Settings.ParentKey0ColumnName} VARCHAR(2000) NULL DEFAULT NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.',
                    {Settings.ParentHash1ColumnName} BIGINT(20) SIGNED DEFAULT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
                    {Settings.ParentKey1ColumnName} VARCHAR(2000) NULL DEFAULT NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.',
                    {Settings.ParentHash2ColumnName} BIGINT(20) SIGNED DEFAULT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
                    {Settings.ParentKey2ColumnName} VARCHAR(2000) NULL DEFAULT NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.',
                    PRIMARY KEY ({Settings.IdColumnName}),
                    UNIQUE uk_{Settings.CacheEntriesTableName} ({Settings.HashColumnName})
                )
                COLLATE='utf8mb4_unicode_ci'
                ENGINE=InnoDB
                ROW_FORMAT=DYNAMIC
                ;
            ");

        #endregion Commands

        #region Queries

        PeekCacheEntriesQuery = MinifyQuery($@"
                {PeekCacheEntriesQuery}
                limit {p}{nameof(DbCacheEntryGroupQuery.Skip)}, {p}{nameof(DbCacheEntryGroupQuery.Take)}
            ");

        #endregion Queries
    }

    #region DB Connection

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public override DbParameter AddParameter(MySqlCommand command, string name, MemoryStream value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);
        ArgumentNullException.ThrowIfNull(value);

        return command.Parameters.Add(new MySqlParameter(name, value.ToArray()) { MySqlDbType = MySqlDbType.MediumBlob });
    }

    /// <summary>
    ///   Starts a database transaction.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <returns>An object representing the new transaction.</returns>
    public override MySqlTransaction BeginTransaction(MySqlConnection connection)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(connection);

        return connection.BeginTransaction(IsolationLevel.ReadCommitted);
    }

    /// <summary>
    ///   Creates a database command.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <param name="transaction">A started transaction.</param>
    /// <param name="text">Command text.</param>
    /// <returns>An object representing a database command.</returns>
    public override MySqlCommand CreateCommand(MySqlConnection connection, MySqlTransaction transaction, string text)
    {
        return new MySqlCommand(text, connection, transaction) { CommandTimeout = (int)Settings.CommandTimeout.TotalSeconds };
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <returns>An open connection.</returns>
    public override MySqlConnection OpenConnection()
    {
        var connection = new MySqlConnection(ConnectionString);
        connection.Open();
        return connection;
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <param name="cancellationToken">The cancellation instruction.</param>
    /// <returns>An open connection.</returns>
    public override async Task<MySqlConnection> OpenConnectionAsync(CancellationToken cancellationToken)
    {
        var connection = new MySqlConnection(ConnectionString);
        await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
        return connection;
    }

    #endregion DB Connection
}

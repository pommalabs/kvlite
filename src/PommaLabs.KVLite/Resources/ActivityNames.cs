﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Resources;

internal static class ActivityNames
{
    public const string CacheCleanup = "cache_cleanup";
    public const string CacheEntryInsertion = "cache_entry_insertion";
    public const string CacheEntryRetrieval = "cache_entry_retrieval";
}

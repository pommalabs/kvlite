﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.Serialization;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.SQLite;

/// <summary>
///   Settings used by SQLite caches.
/// </summary>
[Serializable, DataContract]
public abstract class SQLiteCacheSettings<TSettings> : DbCacheSettings<TSettings>
    where TSettings : SQLiteCacheSettings<TSettings>;

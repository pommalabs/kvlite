﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.KVLite;

/// <summary>
///   An entry that is stored in the cache.
/// </summary>
/// <typeparam name="TVal">The type of the value.</typeparam>
public interface ICacheEntry<TVal>
{
    /// <summary>
    ///   The refresh interval, used if the entry is sliding; if it is not, it equals to <see cref="TimeSpan.Zero"/>.
    /// </summary>
    TimeSpan Interval { get; }

    /// <summary>
    ///   The key corresponding to this entry.
    /// </summary>
    CacheKey Key { get; }

    /// <summary>
    ///   The parent keys of this entry. If not specified, the array will be empty, but not null.
    /// </summary>
    IEnumerable<CacheKey> ParentKeys { get; }

    /// <summary>
    ///   The partition corresponding to this entry.
    /// </summary>
    CacheKey Partition { get; }

    /// <summary>
    ///   When the cache entry was created.
    /// </summary>
    DateTimeOffset UtcCreation { get; }

    /// <summary>
    ///   When the cache entry will expire.
    /// </summary>
    DateTimeOffset UtcExpiry { get; }

    /// <summary>
    ///   The typed value.
    /// </summary>
    TVal Value { get; }
}

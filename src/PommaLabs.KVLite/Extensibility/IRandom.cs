﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.ComponentModel;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Generates random numbers.
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
public interface IRandom
{
    /// <summary>
    ///   Returns a random floating-point number that is greater than or equal to 0.0, and less
    ///   than 1.0.
    /// </summary>
    /// <returns>
    ///   A double-precision floating point number that is greater than or equal to 0.0, and
    ///   less than 1.0.
    /// </returns>
    double NextDouble();
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite;

public abstract partial class AbstractCache
{
    /// <summary>
    ///   Partition reserved for Microsoft Distributed Cache.
    /// </summary>
    protected static readonly CacheKey DistributedCachePartition = "distributed";
}

public abstract partial class AbstractCache<TCache, TSettings> : AbstractCache, IDistributedCache
{
    /// <inheritdoc/>
    public byte[] Get(string key) => Get<byte[]>(DistributedCachePartition, key).ValueOrDefault();

    /// <inheritdoc/>
    public async Task<byte[]> GetAsync(string key, CancellationToken token = default) => (await GetAsync<byte[]>(DistributedCachePartition, key, token).ConfigureAwait(false)).ValueOrDefault();

    /// <inheritdoc/>
    public void Refresh(string key) => Get<byte[]>(DistributedCachePartition, key);

    /// <inheritdoc/>
    public async Task RefreshAsync(string key, CancellationToken token = default) => await GetAsync<byte[]>(DistributedCachePartition, key, token).ConfigureAwait(false);

    /// <inheritdoc/>
    public void Remove(string key) => Remove(DistributedCachePartition, key);

    /// <inheritdoc/>
    public async Task RemoveAsync(string key, CancellationToken token = default) => await RemoveAsync(DistributedCachePartition, key, token).ConfigureAwait(false);

    /// <inheritdoc/>
    public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(options);
        if (options.SlidingExpiration.HasValue && (options.AbsoluteExpiration.HasValue || options.AbsoluteExpirationRelativeToNow.HasValue))
        {
            throw new InvalidOperationException(ErrorMessages.CacheDoesNotAllowSlidingAndAbsolute);
        }

        if (options.SlidingExpiration.HasValue)
        {
            AddSliding(DistributedCachePartition, key, value, options.SlidingExpiration.Value);
        }
        else if (options.AbsoluteExpiration.HasValue)
        {
            AddTimed(DistributedCachePartition, key, value, options.AbsoluteExpiration.Value);
        }
        else if (options.AbsoluteExpirationRelativeToNow.HasValue)
        {
            this.AddTimed(DistributedCachePartition, key, value, options.AbsoluteExpirationRelativeToNow.Value);
        }
        else
        {
            this.AddTimed(DistributedCachePartition, key, value, Settings.DefaultDistributedCacheAbsoluteExpiration);
        }
    }

    /// <inheritdoc/>
    public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(options);
        if (options.SlidingExpiration.HasValue && (options.AbsoluteExpiration.HasValue || options.AbsoluteExpirationRelativeToNow.HasValue))
        {
            throw new InvalidOperationException(ErrorMessages.CacheDoesNotAllowSlidingAndAbsolute);
        }

        return AsyncMethod(key, value, options, token);

        async Task AsyncMethod(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token)
        {
            if (options.SlidingExpiration.HasValue)
            {
                await AddSlidingAsync(DistributedCachePartition, key, value, options.SlidingExpiration.Value, null, token).ConfigureAwait(false);
            }
            else if (options.AbsoluteExpiration.HasValue)
            {
                await AddTimedAsync(DistributedCachePartition, key, value, options.AbsoluteExpiration.Value, null, token).ConfigureAwait(false);
            }
            else if (options.AbsoluteExpirationRelativeToNow.HasValue)
            {
                await this.AddTimedAsync(DistributedCachePartition, key, value, options.AbsoluteExpirationRelativeToNow.Value, null, token).ConfigureAwait(false);
            }
            else
            {
                await this.AddTimedAsync(DistributedCachePartition, key, value, Settings.DefaultDistributedCacheAbsoluteExpiration, null, token).ConfigureAwait(false);
            }
        }
    }
}

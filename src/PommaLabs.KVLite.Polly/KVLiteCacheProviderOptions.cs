﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.KVLite.Polly;

/// <summary>
///   Options for <see cref="KVLiteSyncCacheProvider{TResult}"/> and <see cref="KVLiteAsyncCacheProvider{TResult}"/>.
/// </summary>
public sealed class KVLiteCacheProviderOptions
{
    /// <summary>
    ///   The partition used by the cache provider. Defaults to "polly".
    /// </summary>
    public CacheKey Partition { get; set; } = nameof(Polly);
}

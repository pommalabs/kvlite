﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Polly.Caching;
using PommaLabs.KVLite.Polly;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for Polly KVLite services.
/// </summary>
public static class KVLitePollyServiceCollectionExtensions
{
    /// <summary>
    ///   Registers:
    ///   * <see cref="KVLiteSyncCacheProvider"/> as singleton for <see cref="ISyncCacheProvider"/>
    ///   * <see cref="KVLiteSyncCacheProvider{TResult}"/> as singleton for <see cref="ISyncCacheProvider{TResult}"/>
    ///   * <see cref="KVLiteAsyncCacheProvider"/> as singleton for <see cref="IAsyncCacheProvider"/>
    ///   * <see cref="KVLiteAsyncCacheProvider{TResult}"/> as singleton for <see cref="IAsyncCacheProvider{TResult}"/>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePollyCacheProvider(this IServiceCollection services) => services.AddKVLitePollyCacheProvider(null);

    /// <summary>
    ///   Registers:
    ///   * <see cref="KVLiteSyncCacheProvider"/> as singleton for <see cref="ISyncCacheProvider"/>
    ///   * <see cref="KVLiteSyncCacheProvider{TResult}"/> as singleton for <see cref="ISyncCacheProvider{TResult}"/>
    ///   * <see cref="KVLiteAsyncCacheProvider"/> as singleton for <see cref="IAsyncCacheProvider"/>
    ///   * <see cref="KVLiteAsyncCacheProvider{TResult}"/> as singleton for <see cref="IAsyncCacheProvider{TResult}"/>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeOptions">Can be used to customize options.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePollyCacheProvider(this IServiceCollection services, Action<KVLiteCacheProviderOptions> changeOptions)
    {
        var options = new KVLiteCacheProviderOptions();
        changeOptions?.Invoke(options);

        return services
            .AddSingleton(options)
            .AddSingleton<ISyncCacheProvider, KVLiteSyncCacheProvider>()
            .AddSingleton(typeof(ISyncCacheProvider<>), typeof(KVLiteSyncCacheProvider<>))
            .AddSingleton<IAsyncCacheProvider, KVLiteAsyncCacheProvider>()
            .AddSingleton(typeof(IAsyncCacheProvider<>), typeof(KVLiteAsyncCacheProvider<>));
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.SqlServer;

/// <summary>
///   Cache connection factory specialized for SQL Server.
/// </summary>
public sealed class SqlServerCacheConnectionFactory : DbCacheConnectionFactory<SqlServerCacheSettings, SqlConnection, SqlTransaction, SqlCommand>
{
    /// <summary>
    ///   Cache connection factory specialized for SQL Server.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    public SqlServerCacheConnectionFactory(SqlServerCacheSettings settings, ILogger<SqlServerCache> logger)
        : base(settings, logger)
    {
        OnSettingsPropertyChanged(null, null);
    }

    /// <summary>
    ///   The symbol used to enclose an identifier (left side).
    /// </summary>
    protected override string LeftIdentifierEncloser { get; } = "[";

    /// <summary>
    ///   Function used to estimate cache size.
    /// </summary>
    protected override string LengthSqlFunction { get; } = "len";

    /// <summary>
    ///   The symbol used to enclose an identifier (right side).
    /// </summary>
    protected override string RightIdentifierEncloser { get; } = "]";

    /// <summary>
    ///   This method is called when either the cache schema name or the cache entries table
    ///   name have been changed by the user.
    /// </summary>
    protected override void UpdateCommandsAndQueries()
    {
        base.UpdateCommandsAndQueries();

        var p = ParameterPrefix;
        var l = LeftIdentifierEncloser;
        var r = RightIdentifierEncloser;
        var s = SqlSchemaWithDot;

        #region Commands

        InsertOrUpdateCacheEntryCommand = MinifyQuery($@"
                update {s}{l}{Settings.CacheEntriesTableName}{r}
                   set {l}{Settings.UtcExpiryColumnName}{r} = {p}{nameof(DbCacheValue.UtcExpiry)},
                       {l}{Settings.IntervalColumnName}{r} = {p}{nameof(DbCacheValue.Interval)},
                       {l}{Settings.ValueColumnName}{r} = {p}{nameof(DbCacheValue.Value)},
                       {l}{Settings.CompressedColumnName}{r} = {p}{nameof(DbCacheValue.Compressed)},
                       {l}{Settings.UtcCreationColumnName}{r} = {p}{nameof(DbCacheEntry.UtcCreation)},
                       {l}{Settings.ParentHash0ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentHash0)},
                       {l}{Settings.ParentKey0ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentKey0)},
                       {l}{Settings.ParentHash1ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentHash1)},
                       {l}{Settings.ParentKey1ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentKey1)},
                       {l}{Settings.ParentHash2ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentHash2)},
                       {l}{Settings.ParentKey2ColumnName}{r} = {p}{nameof(DbCacheEntry.ParentKey2)}
                 where {l}{Settings.HashColumnName}{r} = {p}{nameof(DbCacheValue.Hash)};

                if @@rowcount = 0
                begin
                    insert into {s}{l}{Settings.CacheEntriesTableName}{r} (
                        {l}{Settings.HashColumnName}{r},
                        {l}{Settings.UtcExpiryColumnName}{r},
                        {l}{Settings.IntervalColumnName}{r},
                        {l}{Settings.ValueColumnName}{r},
                        {l}{Settings.CompressedColumnName}{r},
                        {l}{Settings.PartitionColumnName}{r},
                        {l}{Settings.KeyColumnName}{r},
                        {l}{Settings.UtcCreationColumnName}{r},
                        {l}{Settings.ParentHash0ColumnName}{r},
                        {l}{Settings.ParentKey0ColumnName}{r},
                        {l}{Settings.ParentHash1ColumnName}{r},
                        {l}{Settings.ParentKey1ColumnName}{r},
                        {l}{Settings.ParentHash2ColumnName}{r},
                        {l}{Settings.ParentKey2ColumnName}{r}
                    )
                    values (
                        {p}{nameof(DbCacheValue.Hash)},
                        {p}{nameof(DbCacheValue.UtcExpiry)},
                        {p}{nameof(DbCacheValue.Interval)},
                        {p}{nameof(DbCacheValue.Value)},
                        {p}{nameof(DbCacheValue.Compressed)},
                        {p}{nameof(DbCacheEntry.Partition)},
                        {p}{nameof(DbCacheEntry.Key)},
                        {p}{nameof(DbCacheEntry.UtcCreation)},
                        {p}{nameof(DbCacheEntry.ParentHash0)},
                        {p}{nameof(DbCacheEntry.ParentKey0)},
                        {p}{nameof(DbCacheEntry.ParentHash1)},
                        {p}{nameof(DbCacheEntry.ParentKey1)},
                        {p}{nameof(DbCacheEntry.ParentHash2)},
                        {p}{nameof(DbCacheEntry.ParentKey2)}
                    );
                end
            ");

        CreateCacheSchemaCommand = MinifyQuery($@"
                IF SCHEMA_ID(N'{Settings.CacheSchemaName}') IS NULL
                BEGIN
                    EXEC('CREATE SCHEMA {l}{Settings.CacheSchemaName}{r}');
                END
                IF OBJECT_ID(N'{s}{Settings.CacheEntriesTableName}', N'U') IS NULL
                BEGIN
                    CREATE TABLE {s}{l}{Settings.CacheEntriesTableName}{r} (
                        {l}{Settings.IdColumnName}{r} BIGINT IDENTITY(1, 1) NOT NULL,
                        {l}{Settings.HashColumnName}{r} BIGINT NOT NULL,
                        {l}{Settings.UtcExpiryColumnName}{r} BIGINT NOT NULL,
                        {l}{Settings.IntervalColumnName}{r} BIGINT NOT NULL,
                        {l}{Settings.ValueColumnName}{r} VARBINARY (MAX) NOT NULL,
                        {l}{Settings.CompressedColumnName}{r} BIT NOT NULL,
                        {l}{Settings.PartitionColumnName}{r} NVARCHAR (2000) NOT NULL,
                        {l}{Settings.KeyColumnName}{r} NVARCHAR (2000) NOT NULL,
                        {l}{Settings.UtcCreationColumnName}{r} BIGINT NOT NULL,
                        {l}{Settings.ParentHash0ColumnName}{r} BIGINT NULL,
                        {l}{Settings.ParentKey0ColumnName}{r} NVARCHAR (2000) NULL,
                        {l}{Settings.ParentHash1ColumnName}{r} BIGINT NULL,
                        {l}{Settings.ParentKey1ColumnName}{r} NVARCHAR (2000) NULL,
                        {l}{Settings.ParentHash2ColumnName}{r} BIGINT NULL,
                        {l}{Settings.ParentKey2ColumnName}{r} NVARCHAR (2000) NULL,
                        CONSTRAINT {l}pk_{Settings.CacheEntriesTableName}{r} PRIMARY KEY CLUSTERED ({l}{Settings.IdColumnName}{r}),
                        CONSTRAINT {l}uk_{Settings.CacheEntriesTableName}{r} UNIQUE ({l}{Settings.HashColumnName}{r})
                    );

                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Automatically generated ID.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.IdColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Hash of partition and key.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.HashColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'When the entry will expire, expressed as seconds after UNIX epoch.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.UtcExpiryColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'How many seconds should be used to extend expiry time when the entry is retrieved.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.IntervalColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Serialized and optionally compressed content of this entry.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ValueColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Whether the entry content was compressed or not.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.CompressedColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'A partition holds a group of related keys.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.PartitionColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'A key uniquely identifies an entry inside a partition.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.KeyColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'When the entry was created, expressed as seconds after UNIX epoch.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.UtcCreationColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry hash, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentHash0ColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry key, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentKey0ColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry hash, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentHash1ColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry key, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentKey1ColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry hash, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentHash2ColumnName}';
                    EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Optional parent entry key, used to link entries in a hierarchical way.', @level0type = N'SCHEMA', @level0name = N'{Settings.CacheSchemaName}', @level1type = N'TABLE', @level1name = N'{Settings.CacheEntriesTableName}', @level2type = N'COLUMN', @level2name = N'{Settings.ParentKey2ColumnName}';
                END
            ");

        #endregion Commands

        #region Queries

        PeekCacheEntriesQuery = MinifyQuery($@"
                {PeekCacheEntriesQuery}
                offset {p}{nameof(DbCacheEntryGroupQuery.Skip)} rows fetch next {p}{nameof(DbCacheEntryGroupQuery.Take)} rows only
            ");

        #endregion Queries
    }

    #region DB Connection

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public override DbParameter AddParameter(SqlCommand command, string name, MemoryStream value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);
        ArgumentNullException.ThrowIfNull(value);

        value.Position = 0L;
        return command.Parameters.AddWithValue(name, value);
    }

    /// <summary>
    ///   Starts a database transaction.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <returns>An object representing the new transaction.</returns>
    public override SqlTransaction BeginTransaction(SqlConnection connection)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(connection);

        return connection.BeginTransaction(IsolationLevel.ReadCommitted);
    }

    /// <summary>
    ///   Creates a database command.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <param name="transaction">A started transaction.</param>
    /// <param name="text">Command text.</param>
    /// <returns>An object representing a database command.</returns>
    public override SqlCommand CreateCommand(SqlConnection connection, SqlTransaction transaction, string text)
    {
        return new SqlCommand(text, connection, transaction) { CommandTimeout = (int)Settings.CommandTimeout.TotalSeconds };
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <returns>An open connection.</returns>
    public override SqlConnection OpenConnection()
    {
        var connection = new SqlConnection(ConnectionString);
        connection.Open();
        return connection;
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <param name="cancellationToken">The cancellation instruction.</param>
    /// <returns>An open connection.</returns>
    public override async Task<SqlConnection> OpenConnectionAsync(CancellationToken cancellationToken)
    {
        var connection = new SqlConnection(ConnectionString);
        await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
        return connection;
    }

    #endregion DB Connection
}

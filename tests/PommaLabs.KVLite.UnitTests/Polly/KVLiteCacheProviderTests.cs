﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Ninject;
using NUnit.Framework;
using Polly.Caching;
using PommaLabs.KVLite.Memory;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.Polly;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SQLite;
using PommaLabs.KVLite.SqlServer;
using PommaLabs.KVLite.UnitTests.Infrastructure;
using Shouldly;

namespace PommaLabs.KVLite.UnitTests.Polly;

[TestFixture(typeof(MemoryCache))]
[TestFixture(typeof(MySqlCache))]
[TestFixture(typeof(PersistentCache))]
[TestFixture(typeof(PostgreSqlCache))]
[TestFixture(typeof(SqlServerCache))]
[TestFixture(typeof(VolatileCache))]
[Category(nameof(Polly))]
[NonParallelizable]
internal sealed class KVLiteCacheProviderTests(Type backingCacheType) : AbstractTests
{
    #region Setup/Teardown

    private KVLiteAsyncCacheProvider<string> _typedAsyncCacheProvider;
    private KVLiteSyncCacheProvider<string> _typedSyncCacheProvider;
    private KVLiteAsyncCacheProvider _untypedAsyncCacheProvider;
    private KVLiteSyncCacheProvider _untypedSyncCacheProvider;

    [SetUp]
    public async Task SetUpAsync()
    {
        var cache = Kernel.Get(backingCacheType) as ICache;

        _untypedSyncCacheProvider = new KVLiteSyncCacheProvider(cache, new KVLiteCacheProviderOptions());
        _typedSyncCacheProvider = new KVLiteSyncCacheProvider<string>(cache, new KVLiteCacheProviderOptions());
        _untypedAsyncCacheProvider = new KVLiteAsyncCacheProvider(cache, new KVLiteCacheProviderOptions());
        _typedAsyncCacheProvider = new KVLiteAsyncCacheProvider<string>(cache, new KVLiteCacheProviderOptions());

        await _typedAsyncCacheProvider.BackingCache.ClearAsync();
    }

    [TearDown]
    public async Task TearDownAsync()
    {
        try
        {
            await _typedAsyncCacheProvider?.BackingCache?.ClearAsync();
        }
        finally
        {
            _typedSyncCacheProvider = null;
            _typedAsyncCacheProvider = null;
        }
    }

    #endregion Setup/Teardown

    [Test]
    public async Task TypedAsyncCacheProviderShouldReturnAnEntryWhenItExists()
    {
        const string ExistingKey = nameof(ExistingKey);
        const string ExistingValue = nameof(ExistingValue);
        await _typedAsyncCacheProvider.PutAsync(ExistingKey, ExistingValue, new Ttl(TenMinutes), default, false);

        var (hasValue, value) = await _typedAsyncCacheProvider.TryGetAsync(ExistingKey, default, false);

        hasValue.ShouldBeTrue();
        value.ShouldBe(ExistingValue);
    }

    [Test]
    public async Task TypedAsyncCacheProviderShouldReturnNullWhenRequestedEntryDoesNotExist()
    {
        const string NotExistingKey = nameof(NotExistingKey);

        var (hasValue, value) = await _typedAsyncCacheProvider.TryGetAsync(NotExistingKey, default, false);

        hasValue.ShouldBeFalse();
        value.ShouldBeNull();
    }

    [Test]
    public void TypedSyncCacheProviderShouldReturnAnEntryWhenItExists()
    {
        const string ExistingKey = nameof(ExistingKey);
        const string ExistingValue = nameof(ExistingValue);
        _typedSyncCacheProvider.Put(ExistingKey, ExistingValue, new Ttl(TenMinutes));

        var (hasValue, value) = _typedSyncCacheProvider.TryGet(ExistingKey);

        hasValue.ShouldBeTrue();
        value.ShouldBe(ExistingValue);
    }

    [Test]
    public void TypedSyncCacheProviderShouldReturnNullWhenRequestedEntryDoesNotExist()
    {
        const string NotExistingKey = nameof(NotExistingKey);

        var (hasValue, value) = _typedSyncCacheProvider.TryGet(NotExistingKey);

        hasValue.ShouldBeFalse();
        value.ShouldBeNull();
    }

    [Test]
    public async Task UntypedAsyncCacheProviderShouldReturnAnEntryWhenItExists()
    {
        const string ExistingKey = nameof(ExistingKey);
        const string ExistingValue = nameof(ExistingValue);
        await _untypedAsyncCacheProvider.PutAsync(ExistingKey, ExistingValue, new Ttl(TenMinutes), default, false);

        var (hasValue, value) = await _untypedAsyncCacheProvider.TryGetAsync(ExistingKey, default, false);

        hasValue.ShouldBeTrue();
        value.ShouldBe(ExistingValue);
    }

    [Test]
    public async Task UntypedAsyncCacheProviderShouldReturnNullWhenRequestedEntryDoesNotExist()
    {
        const string NotExistingKey = nameof(NotExistingKey);

        var (hasValue, value) = await _untypedAsyncCacheProvider.TryGetAsync(NotExistingKey, default, false);

        hasValue.ShouldBeFalse();
        value.ShouldBeNull();
    }

    [Test]
    public void UntypedSyncCacheProviderShouldReturnAnEntryWhenItExists()
    {
        const string ExistingKey = nameof(ExistingKey);
        const string ExistingValue = nameof(ExistingValue);
        _untypedSyncCacheProvider.Put(ExistingKey, ExistingValue, new Ttl(TenMinutes));

        var (hasValue, value) = _untypedSyncCacheProvider.TryGet(ExistingKey);

        hasValue.ShouldBeTrue();
        value.ShouldBe(ExistingValue);
    }

    [Test]
    public void UntypedSyncCacheProviderShouldReturnNullWhenRequestedEntryDoesNotExist()
    {
        const string NotExistingKey = nameof(NotExistingKey);

        var (hasValue, value) = _untypedSyncCacheProvider.TryGet(NotExistingKey);

        hasValue.ShouldBeFalse();
        value.ShouldBeNull();
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PommaLabs.KVLite;

/// <summary>
///   An entry that is stored into the cache.
/// </summary>
[Serializable, DataContract]
public abstract class CacheEntry
{
    #region Constants

    /// <summary>
    ///   Used when the entry has no parent keys.
    /// </summary>
    public static readonly IEnumerable<CacheKey> NoParentKeys = Array.Empty<CacheKey>();

    #endregion Constants
}

/// <summary>
///   An entry that is stored into the cache.
/// </summary>
/// <typeparam name="TVal">The type of the cached value.</typeparam>
[Serializable, DataContract]
public sealed class CacheEntry<TVal> : CacheEntry, ICacheEntry<TVal>, IEquatable<CacheEntry<TVal>>, IEquatable<ICacheEntry<TVal>>
{
    private IEnumerable<CacheKey> _parentKeys;

    /// <inheritdoc/>
    [DataMember(Order = 5)]
    public TimeSpan Interval { get; set; }

    /// <inheritdoc/>
    [DataMember(Order = 1)]
    public CacheKey Key { get; set; }

    /// <inheritdoc/>
    [DataMember(Order = 6)]
    public IEnumerable<CacheKey> ParentKeys { get => _parentKeys ?? NoParentKeys; set => _parentKeys = value; }

    /// <inheritdoc/>
    [DataMember(Order = 0)]
    public CacheKey Partition { get; set; }

    /// <inheritdoc/>
    [DataMember(Order = 3)]
    public DateTimeOffset UtcCreation { get; set; }

    /// <inheritdoc/>
    [DataMember(Order = 4)]
    public DateTimeOffset UtcExpiry { get; set; }

    /// <inheritdoc/>
    [DataMember(Order = 2)]
    public TVal Value { get; set; }

    #region Equality and serialization members

    /// <inheritdoc/>
    public override bool Equals(object obj)
    {
        if (obj is null) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj is CacheEntry<TVal> info && Equals(info);
    }

    /// <inheritdoc/>
    public bool Equals(CacheEntry<TVal> other)
    {
        if (other is null) return false;
        if (ReferenceEquals(this, other)) return true;
        return Partition == other.Partition && Key == other.Key;
    }

    /// <inheritdoc/>
    public bool Equals(ICacheEntry<TVal> other)
    {
        if (other is null) return false;
        if (ReferenceEquals(this, other)) return true;
        return Partition == other.Partition && Key == other.Key;
    }

    /// <inheritdoc/>
    public override int GetHashCode()
    {
        var result = 17;
        unchecked
        {
            result = (31 * result) + Partition.GetHashCode();
            result = (31 * result) + Key.GetHashCode();
        }
        return result;
    }

    /// <inheritdoc/>
    public override string ToString() => $"{nameof(Partition)}: {Partition}, {nameof(Key)}: {Key}, {nameof(UtcExpiry)}: {UtcExpiry}";

    #endregion Equality and serialization members
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.Serialization;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.SqlServer;

/// <summary>
///   Settings used by <see cref="SqlServerCache"/>.
/// </summary>
[Serializable, DataContract]
public sealed class SqlServerCacheSettings : DbCacheSettings<SqlServerCacheSettings>
{
    /// <summary>
    ///   Default constructor.
    /// </summary>
    public SqlServerCacheSettings()
    {
        // Custom names for SQL driver which look like ones created by EF and Identity.
        _cacheSchemaName = nameof(KVLite);
        _cacheEntriesTableName = "CacheEntries";
    }
}

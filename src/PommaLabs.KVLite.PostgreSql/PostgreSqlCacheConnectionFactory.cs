﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Npgsql;
using NpgsqlTypes;
using PommaLabs.KVLite.Database;

namespace PommaLabs.KVLite.PostgreSql;

/// <summary>
///   Cache connection factory specialized for PostgreSQL.
/// </summary>
public sealed class PostgreSqlCacheConnectionFactory : DbCacheConnectionFactory<PostgreSqlCacheSettings, NpgsqlConnection, NpgsqlTransaction, NpgsqlCommand>
{
    /// <summary>
    ///   Cache connection factory specialized for PostgreSQL.
    /// </summary>
    /// <param name="settings">Cache settings.</param>
    /// <param name="logger">Logger.</param>
    public PostgreSqlCacheConnectionFactory(PostgreSqlCacheSettings settings, ILogger<PostgreSqlCache> logger = null)
        : base(settings, logger)
    {
        OnSettingsPropertyChanged(null, null);
    }

    /// <summary>
    ///   The symbol used to enclose an identifier (left side).
    /// </summary>
    protected override string LeftIdentifierEncloser { get; } = "\"";

    /// <summary>
    ///   The symbol used to enclose an identifier (right side).
    /// </summary>
    protected override string RightIdentifierEncloser { get; } = "\"";

    /// <summary>
    ///   This method is called when either the cache schema name or the cache entries table
    ///   name have been changed by the user.
    /// </summary>
    protected override void UpdateCommandsAndQueries()
    {
        base.UpdateCommandsAndQueries();

        var p = ParameterPrefix;
        var l = LeftIdentifierEncloser;
        var r = RightIdentifierEncloser;
        var s = SqlSchemaWithDot;

        #region Commands

        InsertOrUpdateCacheEntryCommand = MinifyQuery($@"
                insert into {s}{l}{Settings.CacheEntriesTableName}{r} (
                    {Settings.HashColumnName},
                    {Settings.UtcExpiryColumnName},
                    {Settings.IntervalColumnName},
                    {Settings.ValueColumnName},
                    {Settings.CompressedColumnName},
                    {Settings.PartitionColumnName},
                    {Settings.KeyColumnName},
                    {Settings.UtcCreationColumnName},
                    {Settings.ParentHash0ColumnName},
                    {Settings.ParentKey0ColumnName},
                    {Settings.ParentHash1ColumnName},
                    {Settings.ParentKey1ColumnName},
                    {Settings.ParentHash2ColumnName},
                    {Settings.ParentKey2ColumnName}
                )
                values (
                    {p}{nameof(DbCacheValue.Hash)},
                    {p}{nameof(DbCacheValue.UtcExpiry)},
                    {p}{nameof(DbCacheValue.Interval)},
                    {p}{nameof(DbCacheValue.Value)},
                    {p}{nameof(DbCacheValue.Compressed)},
                    {p}{nameof(DbCacheEntry.Partition)},
                    {p}{nameof(DbCacheEntry.Key)},
                    {p}{nameof(DbCacheEntry.UtcCreation)},
                    {p}{nameof(DbCacheEntry.ParentHash0)},
                    {p}{nameof(DbCacheEntry.ParentKey0)},
                    {p}{nameof(DbCacheEntry.ParentHash1)},
                    {p}{nameof(DbCacheEntry.ParentKey1)},
                    {p}{nameof(DbCacheEntry.ParentHash2)},
                    {p}{nameof(DbCacheEntry.ParentKey2)}
                )
                on conflict ({Settings.HashColumnName}) do update set
                    {Settings.UtcExpiryColumnName} = {p}{nameof(DbCacheValue.UtcExpiry)},
                    {Settings.IntervalColumnName} = {p}{nameof(DbCacheValue.Interval)},
                    {Settings.ValueColumnName} = {p}{nameof(DbCacheValue.Value)},
                    {Settings.CompressedColumnName} = {p}{nameof(DbCacheValue.Compressed)},
                    {Settings.UtcCreationColumnName} = {p}{nameof(DbCacheEntry.UtcCreation)},
                    {Settings.ParentHash0ColumnName} = {p}{nameof(DbCacheEntry.ParentHash0)},
                    {Settings.ParentKey0ColumnName} = {p}{nameof(DbCacheEntry.ParentKey0)},
                    {Settings.ParentHash1ColumnName} = {p}{nameof(DbCacheEntry.ParentHash1)},
                    {Settings.ParentKey1ColumnName} = {p}{nameof(DbCacheEntry.ParentKey1)},
                    {Settings.ParentHash2ColumnName} = {p}{nameof(DbCacheEntry.ParentHash2)},
                    {Settings.ParentKey2ColumnName} = {p}{nameof(DbCacheEntry.ParentKey2)};
            ");

        CreateCacheSchemaCommand = MinifyQuery($@"
                CREATE UNLOGGED TABLE IF NOT EXISTS {s}{l}{Settings.CacheEntriesTableName}{r} (
                    {Settings.IdColumnName} bigserial NOT NULL,
                    {Settings.HashColumnName} bigint NOT NULL,
                    {Settings.UtcExpiryColumnName} bigint NOT NULL,
                    {Settings.IntervalColumnName} bigint NOT NULL,
                    {Settings.ValueColumnName} bytea NOT NULL,
                    {Settings.CompressedColumnName} smallint NOT NULL,
                    {Settings.PartitionColumnName} character varying(2000) NOT NULL,
                    {Settings.KeyColumnName} character varying(2000) NOT NULL,
                    {Settings.UtcCreationColumnName} bigint NOT NULL,
                    {Settings.ParentHash0ColumnName} bigint,
                    {Settings.ParentKey0ColumnName} character varying(2000),
                    {Settings.ParentHash1ColumnName} bigint,
                    {Settings.ParentKey1ColumnName} character varying(2000),
                    {Settings.ParentHash2ColumnName} bigint,
                    {Settings.ParentKey2ColumnName} character varying(2000),
                    CONSTRAINT pk_{Settings.CacheEntriesTableName} PRIMARY KEY ({Settings.IdColumnName}),
                    CONSTRAINT uk_{Settings.CacheEntriesTableName} UNIQUE ({Settings.HashColumnName})
                )
                WITH (
                    OIDS = FALSE
                );

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.IdColumnName}
                    IS 'Automatically generated ID.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.HashColumnName}
                    IS 'Hash of partition and key.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.UtcExpiryColumnName}
                    IS 'When the entry will expire, expressed as seconds after UNIX epoch.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.IntervalColumnName}
                    IS 'How many seconds should be used to extend expiry time when the entry is retrieved.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ValueColumnName}
                    IS 'Serialized and optionally compressed content of this entry.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.CompressedColumnName}
                    IS 'Whether the entry content was compressed or not.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.PartitionColumnName}
                    IS 'A partition holds a group of related keys.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.KeyColumnName}
                    IS 'A key uniquely identifies an entry inside a partition.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.UtcCreationColumnName}
                    IS 'When the entry was created, expressed as seconds after UNIX epoch.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentHash0ColumnName}
                    IS 'Optional parent entry hash, used to link entries in a hierarchical way.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentKey0ColumnName}
                    IS 'Optional parent entry key, used to link entries in a hierarchical way.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentHash1ColumnName}
                    IS 'Optional parent entry hash, used to link entries in a hierarchical way.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentKey1ColumnName}
                    IS 'Optional parent entry key, used to link entries in a hierarchical way.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentHash2ColumnName}
                    IS 'Optional parent entry hash, used to link entries in a hierarchical way.';

                COMMENT ON COLUMN {s}{l}{Settings.CacheEntriesTableName}{r}.{Settings.ParentKey2ColumnName}
                    IS 'Optional parent entry key, used to link entries in a hierarchical way.';
            ");

        #endregion Commands

        #region Queries

        PeekCacheEntriesQuery = MinifyQuery($@"
                {PeekCacheEntriesQuery}
                limit {p}{nameof(DbCacheEntryGroupQuery.Take)} offset {p}{nameof(DbCacheEntryGroupQuery.Skip)}
            ");

        #endregion Queries
    }

    #region DB Connection

    /// <summary>
    ///   Adds a database parameter to specified command.
    /// </summary>
    /// <param name="command">SQL command.</param>
    /// <param name="name">Name.</param>
    /// <param name="value">Value.</param>
    /// <returns>An object representing a database parameter.</returns>
    public override DbParameter AddParameter(NpgsqlCommand command, string name, MemoryStream value)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(command);
        ArgumentNullException.ThrowIfNull(value);

        return command.Parameters.AddWithValue(name, NpgsqlDbType.Bytea, (int)value.Length, value.GetBuffer());
    }

    /// <summary>
    ///   Starts a database transaction.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <returns>An object representing the new transaction.</returns>
    public override NpgsqlTransaction BeginTransaction(NpgsqlConnection connection)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(connection);

        return connection.BeginTransaction(IsolationLevel.ReadCommitted);
    }

    /// <summary>
    ///   Creates a database command.
    /// </summary>
    /// <param name="connection">An open connection.</param>
    /// <param name="transaction">A started transaction.</param>
    /// <param name="text">Command text.</param>
    /// <returns>An object representing a database command.</returns>
    public override NpgsqlCommand CreateCommand(NpgsqlConnection connection, NpgsqlTransaction transaction, string text)
    {
        return new NpgsqlCommand(text, connection, transaction) { CommandTimeout = (int)Settings.CommandTimeout.TotalSeconds };
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <returns>An open connection.</returns>
    public override NpgsqlConnection OpenConnection()
    {
        var connection = new NpgsqlConnection(ConnectionString);
        connection.Open();
        return connection;
    }

    /// <summary>
    ///   Opens a new connection to the specified data provider.
    /// </summary>
    /// <param name="cancellationToken">The cancellation instruction.</param>
    /// <returns>An open connection.</returns>
    public override async Task<NpgsqlConnection> OpenConnectionAsync(CancellationToken cancellationToken)
    {
        var connection = new NpgsqlConnection(ConnectionString);
        await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
        return connection;
    }

    #endregion DB Connection
}

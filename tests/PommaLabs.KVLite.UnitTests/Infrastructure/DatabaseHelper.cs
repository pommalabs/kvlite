﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using MySqlConnector;
using Npgsql;
using Polly;
using Polly.Retry;
using PommaLabs.KVLite.MySql;
using PommaLabs.KVLite.PostgreSql;
using PommaLabs.KVLite.SqlServer;

namespace PommaLabs.KVLite.UnitTests.Infrastructure;

public static class DatabaseHelper
{
    private static readonly ResiliencePipeline s_databaseProbePipeline = new ResiliencePipelineBuilder()
        .AddRetry(new RetryStrategyOptions
        {
            UseJitter = true, // Adds a random factor to the delay.
            MaxRetryAttempts = 5,
            Delay = TimeSpan.FromSeconds(5),
        })
        .Build();

    public static string GetMySqlConnectionString()
    {
        var host = Environment.GetEnvironmentVariable("MYSQL_HOST") ?? "mysql";
        var port = Environment.GetEnvironmentVariable("MYSQL_PORT") ?? "3306";
        var database = Environment.GetEnvironmentVariable("MYSQL_DATABASE") ?? "kvlite";
        var user = Environment.GetEnvironmentVariable("MYSQL_USER") ?? "kvlite";
        var password = Environment.GetEnvironmentVariable("MYSQL_PASSWORD") ?? "kvlite";
        return $"Server={host};Port={port};Database={database};Uid={user};Pwd={password};CharSet=utf8;Pooling=true;MinimumPoolSize=1;SslMode=None;AllowPublicKeyRetrieval=true;";
    }

    public static string GetPostgreSqlConnectionString()
    {
        var host = Environment.GetEnvironmentVariable("POSTGRES_HOST") ?? "postgres";
        var port = Environment.GetEnvironmentVariable("POSTGRES_PORT") ?? "5432";
        var database = Environment.GetEnvironmentVariable("POSTGRES_DB") ?? "kvlite";
        var user = Environment.GetEnvironmentVariable("POSTGRES_USER") ?? "kvlite";
        var password = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD") ?? "kvlite";
        return $"Server={host};Port={port};Database={database};UserId={user};Password={password};MaxAutoPrepare=10;Pooling=true;MinPoolSize=1;";
    }

    public static string GetSqlServerConnectionString()
    {
        var host = Environment.GetEnvironmentVariable("MSSQL_HOST") ?? "mssql";
        var port = Environment.GetEnvironmentVariable("MSSQL_PORT") ?? "1433";
        var database = Environment.GetEnvironmentVariable("MSSQL_DB") ?? "kvlite";
        var user = Environment.GetEnvironmentVariable("MSSQL_USER") ?? "sa";
        var password = Environment.GetEnvironmentVariable("MSSQL_SA_PASSWORD") ?? "P@ssw0rd";
        return $"Server={host},{port};Database={database};User Id={user};Password={password};Pooling=true;Min Pool Size=1;Encrypt=false;";
    }

    public static void EnsureMySqlIsAvailable(MySqlCacheSettings settings)
    {
        using var connection = new MySqlConnection(settings.ConnectionString);
        EnsureDatabaseIsAvailable(connection, "select 1");
    }

    public static void EnsurePostgreSqlIsAvailable(PostgreSqlCacheSettings settings)
    {
        using var connection = new NpgsqlConnection(settings.ConnectionString);
        EnsureDatabaseIsAvailable(connection, "select 1");
    }

    public static void EnsureSqlServerIsAvailable(SqlServerCacheSettings settings)
    {
        var connectionStringBuilder = new SqlConnectionStringBuilder(settings.ConnectionString);
        var databaseName = connectionStringBuilder.InitialCatalog;
        connectionStringBuilder.InitialCatalog = "master";
        var connectionString = connectionStringBuilder.ToString();

        using var connection = new SqlConnection(connectionString);
        EnsureDatabaseIsAvailable(connection, "select 1");

        using var command = connection.CreateCommand();
        command.CommandText = $@"
            IF NOT EXISTS (SELECT * FROM [sys].[databases] WHERE [name] = '{databaseName}')
            BEGIN
                CREATE DATABASE [{databaseName}];
            END
        ";

        TryOpenConnection(connection);
        command.ExecuteNonQuery();
    }

    private static void EnsureDatabaseIsAvailable(DbConnection connection, string probeStatement)
    {
        using var command = connection.CreateCommand();
        command.CommandText = probeStatement;

        s_databaseProbePipeline.Execute(_ =>
        {
            TryOpenConnection(connection);
            command.ExecuteNonQuery();
        });
    }

    private static void TryOpenConnection(DbConnection connection)
    {
        if (connection.State != ConnectionState.Open)
        {
            connection.Open();
        }
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.KVLite.Core;

internal static class Constants
{
    public static TimeSpan RegexMatchTimeout { get; } = TimeSpan.FromSeconds(1);
}

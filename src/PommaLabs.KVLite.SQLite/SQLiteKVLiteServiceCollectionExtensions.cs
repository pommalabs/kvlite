﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using Microsoft.Extensions.Caching.Distributed;
using PommaLabs.KVLite;
using PommaLabs.KVLite.SQLite;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for SQLite KVLite services.
/// </summary>
public static class SQLiteKVLiteServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="PersistentCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePersistentSQLiteCache(this IServiceCollection services) => services.AddKVLitePersistentSQLiteCache(null);

    /// <summary>
    ///   Registers <see cref="PersistentCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeSettings">Can be used to customize settings.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLitePersistentSQLiteCache(this IServiceCollection services, Action<PersistentCacheSettings> changeSettings)
    {
        var settings = new PersistentCacheSettings();
        changeSettings?.Invoke(settings);

        return services.AddKVLiteCache<PersistentCache, PersistentCacheSettings>((serializer, clock, logger, interceptor, random) =>
        {
            var cache = new PersistentCache(settings, serializer, clock, logger, interceptor, random);

            // Persistent SQLite cache requires a periodic VACUUM operation. Dependency
            // instantiation step seems to be the right moment to perform that operation
            // (because this will be a singleton).
            cache.Vacuum();

            return cache;
        });
    }

    /// <summary>
    ///   Registers <see cref="VolatileCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteVolatileSQLiteCache(this IServiceCollection services) => services.AddKVLiteVolatileSQLiteCache(null);

    /// <summary>
    ///   Registers <see cref="VolatileCache"/> as singleton implementation for
    ///   <see cref="ICache"/>, <see cref="ICache{TSettings}"/> and <see cref="IDistributedCache"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="changeSettings">Can be used to customize settings.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddKVLiteVolatileSQLiteCache(this IServiceCollection services, Action<VolatileCacheSettings> changeSettings)
    {
        var settings = new VolatileCacheSettings();
        changeSettings?.Invoke(settings);

        return services.AddKVLiteCache<VolatileCache, VolatileCacheSettings>(
            (serializer, clock, logger, interceptor, random) =>
            new VolatileCache(settings, serializer, clock, logger, interceptor, random));
    }
}

﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.KVLite.Extensibility;
using PommaLabs.KVLite.Resources;

namespace PommaLabs.KVLite.AspNetCore;

/// <summary>
///   Health check for KVLite cache. This check verifies that cache is healthy with following operations:
///   1) A stub random entry is added.
///   2) Entry is read and checked.
///   3) Entry is removed.
///   4) Entry is read again and it should not be found.
/// </summary>
public sealed class KVLiteHealthCheck : IHealthCheck
{
    private static readonly CacheKey s_cachePartition = "diagnostics";
    private static readonly ConditionalWeakTable<ICache, CachedResult> s_cachedResults = new();

    private readonly ICache _cache;
    private readonly IClock _clock;

    /// <summary>
    ///   Builds the health check for KVLite cache.
    /// </summary>
    /// <param name="cache">The cache.</param>
    /// <param name="serviceProvider">The service provider.</param>
    public KVLiteHealthCheck(ICache cache, IServiceProvider serviceProvider)
    {
        _cache = cache ?? throw new ArgumentNullException(nameof(cache), ErrorMessages.NullCache);
        _clock = (serviceProvider.GetService(typeof(IClock)) as IClock) ?? SystemClock.Instance;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (s_cachedResults.TryGetValue(_cache, out var cachedResult) && _clock.UtcNow < cachedResult.ExpiresAt)
        {
            return cachedResult.Result;
        }

        var rnd = Guid.NewGuid();
        var key = new CacheKey(rnd);

        var data = new Dictionary<string, object>
        {
            ["CachePartition"] = s_cachePartition,
            ["CacheKey"] = key,
        };

        await _cache.AddTimedAsync(s_cachePartition, key, rnd, TimeSpan.FromMinutes(1), cancellationToken: cancellationToken).ConfigureAwait(false);
        var check = await _cache.GetAsync<Guid>(s_cachePartition, key, cancellationToken).ConfigureAwait(false);

        if (!check.HasValue || check.Value != rnd)
        {
            return AddResultToCache(HealthCheckResult.Unhealthy("Added entry did not match or does not exist in cache", _cache.LastError, data));
        }

        await _cache.RemoveAsync(s_cachePartition, key, cancellationToken).ConfigureAwait(false);
        check = await _cache.GetAsync<Guid>(s_cachePartition, key, cancellationToken).ConfigureAwait(false);

        if (check.HasValue)
        {
            return AddResultToCache(HealthCheckResult.Unhealthy("Removed entry still exists in cache", _cache.LastError, data));
        }

        return AddResultToCache(HealthCheckResult.Healthy(data: data));
    }

    internal static void ClearCachedResults()
    {
        s_cachedResults.Clear();
    }

    private HealthCheckResult AddResultToCache(HealthCheckResult result)
    {
        var cacheLifetime = TimeSpan.FromSeconds(result.Status == HealthStatus.Healthy ? 27 : 9);
        s_cachedResults.AddOrUpdate(_cache, new CachedResult(result, _clock.UtcNow + cacheLifetime));
        return result;
    }

    private sealed record CachedResult(HealthCheckResult Result, DateTimeOffset ExpiresAt);
}

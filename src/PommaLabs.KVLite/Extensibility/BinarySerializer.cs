﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.IO.Compression;
using MBrace.FsPickler;
using Microsoft.FSharp.Core;
using FSharpBinarySerializer = MBrace.FsPickler.BinarySerializer;

namespace PommaLabs.KVLite.Extensibility;

/// <summary>
///   Binary serializer based on the <see cref="FSharpBinarySerializer"/> class.
/// </summary>
public sealed class BinarySerializer : ISerializer
{
    /// <summary>
    ///   Wrapped FsPickler binary serializer.
    /// </summary>
    private readonly FSharpBinarySerializer _binarySerializer;

    /// <summary>
    ///   Option with true value.
    /// </summary>
    private readonly FSharpOption<bool> _true = new(true);

    /// <summary>
    ///   Constructor.
    /// </summary>
    public BinarySerializer()
    {
        var registry = new CustomPicklerRegistry();
        registry.DeclareSerializable(FuncConvert.FromFunc<Type, bool>(_ => true));

        var resolver = new FSharpOption<IPicklerResolver>(PicklerCache.FromCustomPicklerRegistry(registry));

        _binarySerializer = FsPickler.CreateBinarySerializer(picklerResolver: resolver);
    }

    /// <summary>
    ///   Thread safe singleton.
    /// </summary>
    public static BinarySerializer Instance { get; } = new();

    /// <summary>
    ///   Deserializes the object contained into specified stream.
    /// </summary>
    /// <typeparam name="TObj">The type of the object.</typeparam>
    /// <param name="inputStream">The input stream.</param>
    /// <param name="compressed">Whether input stream was serialized with compression.</param>
    /// <returns>The deserialized object.</returns>
    public TObj Deserialize<TObj>(Stream inputStream, bool compressed)
    {
        if (!compressed)
        {
            return (TObj)_binarySerializer.Deserialize<Boxer>(inputStream, leaveOpen: _true).Boxed;
        }
        using var compressionStream = new DeflateStream(inputStream, CompressionMode.Decompress, true);
        return (TObj)_binarySerializer.Deserialize<Boxer>(compressionStream, leaveOpen: _true).Boxed;
    }

    /// <summary>
    ///   Serializes given object into specified stream.
    /// </summary>
    /// <typeparam name="TObj">The type of the object.</typeparam>
    /// <param name="outputStream">The output stream.</param>
    /// <param name="obj">The object.</param>
    /// <returns>True if compression was applied, false otherwise.</returns>
    public bool Serialize<TObj>(Stream outputStream, TObj obj)
    {
        using var compressionStream = new DeflateStream(outputStream, CompressionLevel.Fastest, true);
        _binarySerializer.Serialize(compressionStream, new Boxer { Boxed = obj }, leaveOpen: _true);
        return true;
    }

    /// <summary>
    ///   Used to deserialize almost everything without knowing true object type.
    /// </summary>
    private sealed class Boxer
    {
        /// <summary>
        ///   Boxed value.
        /// </summary>
        public object Boxed { get; set; }
    }
}

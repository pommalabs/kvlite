﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.KVLite.Memory;

internal sealed class MemoryCacheKey : IEquatable<MemoryCacheKey>
{
    public MemoryCacheKey(CacheKey partition, CacheKey key)
    {
        Partition = partition;
        Key = key;
    }

    public CacheKey Key { get; }

    public CacheKey Partition { get; }

    public static bool operator !=(MemoryCacheKey key1, MemoryCacheKey key2)
    {
        return !(key1 == key2);
    }

    public static bool operator ==(MemoryCacheKey key1, MemoryCacheKey key2)
    {
        return EqualityComparer<MemoryCacheKey>.Default.Equals(key1, key2);
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as MemoryCacheKey);
    }

    public bool Equals(MemoryCacheKey other)
    {
        return other != null &&
               Partition == other.Partition &&
               Key == other.Key;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Partition, Key);
    }
}
